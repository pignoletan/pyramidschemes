<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:import url="/WEB-INF/jsp/common/header.jsp">
	<c:param name="pageTitle" value="Product List View" />
</c:import>

<c:choose>
	<c:when test="${empty param.minPrice}">
		<c:set var="minPrice" value="0"/>
	</c:when>
	<c:otherwise>
		<c:set var="minPrice" value="${param.minPrice}"/>
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${empty param.maxPrice}">
		<c:set var="maxPrice" value="1000000"/>
	</c:when>
	<c:otherwise>
		<c:set var="maxPrice" value="${param.maxPrice}"/>
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${empty param.minRating}">
		<c:set var="minRating" value="0"/>
	</c:when>
	<c:otherwise>
		<c:set var="minRating" value="${param.minRating}"/>
	</c:otherwise>
</c:choose>
<%-- <c:choose>
	<c:when test="${empty param.category}">
		<c:set var="category" value="any"/>
	</c:when>
	<c:otherwise>
		<c:set var="category" value="${param.category}"/>
	</c:otherwise>
</c:choose> --%>

<div id="card-page">
	<c:import url="/WEB-INF/jsp/common/filters.jsp">
		<%-- Modify the baseRoute to apply filters to the current route. --%>
		<c:param name="baseRoute" value="#" />
	</c:import>

	<div class="main-content">
		<!-- Container for Sorting Choices
         
         Each link should take the user to this current page and use any combination of the following
         querystring parameters to sort the page:
            - sortOrder (string) - PriceLowToHigh,PriceHighToLow,RatingHighToLow
    	-->
		<div id="sorting-options">
			<h3>Sort By</h3>
			<ul>
				<li><a href="#">Price - Low to High</a></li>
				<li><a href="#">Price - High to Low</a></li>
				<li><a href="#">Rating - High to Low</a></li>
			</ul>
		</div>

		<!-- Container for all of the Products -->
		<!-- The list of products is available using the `products` variable -->
		<div id="grid">

			<c:forEach var="item" items="${products }">
				<c:if test="${item.price >= minPrice
				&&item.price <= maxPrice
				&&item.averageRating >= minRating}">
				<c:set var="classes" value="tile" />
				<c:if test="${item.remainingStock == 0}">
					<c:set var="classes" value="tile sold-out" />
				</c:if>
				<div class="${classes}">
					<c:if test="${item.remainingStock == 0}">
						<span class="banner">Sold Out</span>
					</c:if>
					<c:if test="${item.topSeller }">
						<span class="banner top-seller">Top Seller!</span>
					</c:if>

					<c:url var="linkPath" value="/products/detail">
						<c:param name="id">${item.id}</c:param>
					</c:url>
					
					<a class="product-image" href="${linkPath}">
						<img src="<c:url value="/images/product-images/${item.imageName}" />" />
					</a>
					<div class="details">
						<p class="name">${item.name}</p>

						<div class="rating">
							<c:set var="index" value="1" />
							<c:forEach begin="1" end="5">
								<c:choose>
									<c:when test="${index <= item.averageRating }">
										<span class="filled">&#9734;</span>
									</c:when>
									<c:otherwise>
										<span>&#9734;</span>
									</c:otherwise>
								</c:choose>
								<c:set var="index" value="${index + 1}" />
							</c:forEach>
						</div>
						<c:if test="${item.remainingStock > 0&&item.remainingStock <= 5}">
							<span class="product-alert">Only ${item.remainingStock }
								left!</span>
						</c:if>
						<p class="price"><fmt:formatNumber value="${item.price}" type="currency" /></p>
					</div>
				</div>
				</c:if>
			</c:forEach>

			<!-- 
			The following HTML shows different examples of what HTML could be rendered based on different rules. 
			For purposes of demonstration we've written it out so you can see it when you load the page up. 
			-->

			<!-- Standard Product -->
			<%-- <div class="tile ">
				<!-- Link to the Detail page using the product id (e.g. products/detail?id=1) -->
				<a class="product-image" href="#"> <img
					src="<c:url value="/images/product-images/grey-sofa.jpg" />" />
				</a>
				<div class="details">
					<p class="name">Grey Sofa</p>

					<!-- .filled will make the star solid -->
					<div class="rating">
						<span class="filled">&#9734;</span> <span class="filled">&#9734;</span>
						<span>&#9734;</span> <span>&#9734;</span> <span>&#9734;</span>
					</div>

					<p class="price">$939.00</p>
				</div>
			</div>

			<div class="tile ">
				<!-- Include this if the product is considered a Top Seller -->
				<span class="banner top-seller">Top Seller!</span>

				<!-- Link to the Detail page using the product id (e.g. products/detail?id=1) -->
				<a class="product-image" href="#"> <img
					src="<c:url value="/images/product-images/grey-sofa.jpg" />" />
				</a>
				<div class="details">
					<p class="name">Grey Sofa</p>

					<!-- .filled will make the star solid -->
					<div class="rating">
						<span class="filled">&#9734;</span> <span class="filled">&#9734;</span>
						<span class="filled">&#9734;</span> <span class="filled">&#9734;</span>
						<span>&#9734;</span>
					</div>

					<!-- Include this if the remaining quantity is greater than 0, but less than or equal to 5 -->
					<span class="product-alert">Only 4 left!</span>
					<p class="price">$939.00</p>
				</div>
			</div>

			<!-- Add the .sold-out class if the remaining quantity is 0 -->
			<div class="tile sold-out">
				<!-- Include this if the remaining quantity is 0 -->
				<span class="banner">Sold Out</span>

				<!-- Link to the Detail page using the product id (e.g. products/detail?id=1) -->
				<a class="product-image" href="#"> <img
					src="<c:url value="/images/product-images/grey-sofa.jpg" />" />
				</a>
				<div class="details">
					<p class="name">Grey Sofa</p>

					<!-- .filled will make the star solid -->
					<div class="rating">
						<span class="filled">&#9734;</span> <span>&#9734;</span> <span>&#9734;</span>
						<span>&#9734;</span> <span>&#9734;</span>
					</div>

					<p class="price">$939.00</p>
				</div>
			</div>
		</div> --%>
		</div>
	</div>
</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />