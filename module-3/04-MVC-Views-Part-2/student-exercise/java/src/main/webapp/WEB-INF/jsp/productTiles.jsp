<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:import url="/WEB-INF/jsp/common/header.jsp">
	<c:param name="pageTitle" value="Product Tile View" />
</c:import>

<c:choose>
	<c:when test="${empty param.minPrice}">
		<c:set var="minPrice" value="0" />
	</c:when>
	<c:otherwise>
		<c:set var="minPrice" value="${param.minPrice}" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${empty param.maxPrice}">
		<c:set var="maxPrice" value="1000000" />
	</c:when>
	<c:otherwise>
		<c:set var="maxPrice" value="${param.maxPrice}" />
	</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${empty param.minRating}">
		<c:set var="minRating" value="0" />
	</c:when>
	<c:otherwise>
		<c:set var="minRating" value="${param.minRating}" />
	</c:otherwise>
</c:choose>

<div id="masonry-page">
	<c:import url="/WEB-INF/jsp/common/filters.jsp">
		<%-- Modify the baseRoute to apply filters to the current route. --%>
		<c:param name="baseRoute" value="#" />
	</c:import>

	<div class="main-content">

		<c:forEach var="item" items="${products}">
			<c:if
				test="${item.price >= minPrice
				&&item.price <= maxPrice
				&&item.averageRating >= minRating}">

				<c:set var="divClass" value="tile" />
				<c:if test="${item.topSeller}">
					<c:set var="divClass" value="tile top-seller" />
				</c:if>
				<c:if test="${item.remainingStock == 0}">
					<c:set var="divClass" value="tile sold-out" />
				</c:if>
			</c:if>
			<div class="${divClass}">

				<c:url var="linkPath" value="/products/detail">
					<c:param name="id">${item.id}</c:param>
				</c:url>
				<a class="product-image" href="${linkPath}"> <img
					src="<c:url value="/images/product-images/${item.imageName}" />" />
				</a>

				<div class="details">

					<p class="name">
						<a href="${linkPath}">${item.name}</a>
					</p>

					<div class="rating">
						<c:set var="index" value="1" />
						<c:forEach begin="1" end="5">
							<c:choose>
								<c:when test="${index <= item.averageRating }">
									<span class="filled">&#9734;</span>
								</c:when>
								<c:otherwise>
									<span>&#9734;</span>
								</c:otherwise>
							</c:choose>
							<c:set var="index" value="${index + 1}" />
						</c:forEach>
					</div>

					<c:if test="${item.topSeller }">
						<span class="banner top-seller">Top Seller!</span>
					</c:if>

					<c:if test="${item.remainingStock > 0&&item.remainingStock <= 5}">
						<span class="product-alert">Only ${item.remainingStock }
							left!</span>
					</c:if>

					<p class="price"><fmt:formatNumber value="${item.price}" type="currency" /></p>

				</div>

			</div>
		</c:forEach>

		<%-- <a class="product-image" href="#"> 
				<img src="<c:url value="/images/product-images/grey-sofa.jpg" />" />
			</a>
			<div class="details">
				<p class="name">
					<a href="#">Grey Sofa</a>
				</p>

				<!-- .filled will make the star solid -->
				<div class="rating">
					<span class="filled">&#9734;</span> 
					<span>&#9734;</span> 
					<span>&#9734;</span>
					<span>&#9734;</span> 
					<span>&#9734;</span>
				</div>

				<p class="price">$939.00</p>
			</div>
		</div>

		<!-- Add the .top-seller class if the product is considered a Top Seller -->
		<div class="tile top-seller ">
			<!-- Link to the Detail page using the product id (e.g. products/detail?id=1) -->
			<a class="product-image" href="#"> 
				<img src="<c:url value="/images/product-images/grey-sofa.jpg" />" />
			</a>
			<div class="details">
				<p class="name">
					<a href="#">Grey Sofa</a>
				</p>

				<!-- .filled will make the star solid -->
				<div class="rating">
					<span class="filled">&#9734;</span> 
					<span class="filled">&#9734;</span>
					<span class="filled">&#9734;</span> 
					<span class="filled">&#9734;</span>
					<span>&#9734;</span>
				</div>

				<!-- Add the Top Seller <br/> and product alert if the product is considered a Top Seller -->
				<br />
				<p class="product-alert">Top Seller</p>
				<!-- Add the X remaining product alert if the remaining quantity is greater than 0, but less than or equal to 5 -->
				<p class="product-alert">4 remaining!</p>
				<p class="price">$930.00</p>
			</div>
		</div>

		<!-- Add the .sold-out class if the Remaining Stock is 0 -->
		<div class="tile  sold-out">
			<!-- Add the Sold Out banner if the Remaining Stock is 0 -->
			<span class="banner">Sold Out</span>

			<!-- Link to the Detail page using the product id (e.g. products/detail?id=1) -->
			<a class="product-image" href="#"> 
				<img src="<c:url value="/images/product-images/grey-sofa.jpg" />" />
			</a>
			<div class="details">
				<p class="name">
					<a href="#">Grey Sofa</a>
				</p>

				<!-- .filled will make the star solid -->
				<div class="rating">
					<span class="filled">&#9734;</span> 
					<span class="filled">&#9734;</span>
					<span class="filled">&#9734;</span> 
					<span class="filled">&#9734;</span>
					<span>&#9734;</span>
				</div>

				<p class="price">$939.00</p>
			</div> --%>
	</div>
</div>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />