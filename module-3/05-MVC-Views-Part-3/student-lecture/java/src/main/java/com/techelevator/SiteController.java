package com.techelevator;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SiteController {

	private ForumDao forumDao;

	public SiteController() {
		forumDao = new ForumDao();
	}

	@RequestMapping("/")  //here's the controller code for this URL path
	public String showHomePage() {
		return "home";  //Controller returns the view name as a String
	}

	@RequestMapping("/aboutUs")
	public String showAboutUs() {
		return "aboutUs";
	}

	@RequestMapping("/forum")
	public String showForumList(HttpServletRequest request) {  //receive the request as the param
		request.setAttribute("topicList", forumDao.readAllTopics());  //Load the model data into the request map
		return "forum/forumList";  //We still return the name as a String
	}

	@RequestMapping("/forum/forumDetail")
	public String showForumDetails(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("forumId"));
		request.setAttribute("topic", forumDao.getForumTopicById(id));
		return "forum/forumDetail";
	}
}
