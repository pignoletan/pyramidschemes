package com.techelevator;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller  // This is a SpringMVC Controller
public class SiteController {

	private ForumDao forumDao;

	public SiteController() {
		forumDao = new ForumDao();
	}

	@RequestMapping("/")   // Here's the Controller code for this URL path
	public String showHomePage() {
		return "home";     // Controller returns the view name as a String
	}

	@RequestMapping("/aboutUs")
	public String showAboutUs() {
		return "aboutUs";   // Controller returns the view name as a String
	}

// Typically a Controller will get data from Model
// Load it into a place the View can find it - request Map
// return the view name
//
// There is no application logic in the controller
// Controller processing is usually very straight forward
	
	@RequestMapping("/forum")
	public String showForumList(HttpServletRequest request) { // receive the request as parameter for access to request map
		request.setAttribute("topicList", forumDao.readAllTopics());  // Loading model data into the request Map
		return "forum/forumList";                   // Controller returns the view name as a String
	}

	@RequestMapping("/forum/forumDetail")
	public String showForumDetails(HttpServletRequest request) {
		Long id = Long.parseLong(request.getParameter("forumId"));  // get the value in the parameter "forumId"

		request.setAttribute("topic", forumDao.getForumTopicById(id)); // pass the id to the DAO to get the item with the id
		                                                               // add the topic retrieved to the request
		return "forum/forumDetail";   // Controller returns the view name as a String
	}
	
	
	
}
