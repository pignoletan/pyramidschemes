package com.techelevator.dao;

import com.techelevator.dao.model.Customer;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

/**
 * JDBCCustomerDao
 */
@Component
public class JDBCCustomerDao implements CustomerDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCCustomerDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public List<Customer> searchAndSortCustomers(String search, String sort) {
		List<Customer> results = new ArrayList<Customer>();
		String customerSearchSql = "select * from customer where last_name ilike ? or first_name ilike ? order by ?";
		SqlRowSet rowsReturned = jdbcTemplate.queryForRowSet(customerSearchSql,"%" + search + "%","%" + search + "%",sort);
		while (rowsReturned.next()) {
			results.add(mapRowToCustomer(rowsReturned));
		}
		return results;
	}

	private Customer mapRowToCustomer(SqlRowSet rowReturned) {
		Customer aCustomer = new Customer();
		aCustomer.setFirstName(rowReturned.getString("first_name"));
		aCustomer.setLastName(rowReturned.getString("last_name"));
		aCustomer.setEmail(rowReturned.getString("email"));
		aCustomer.setActive(rowReturned.getBoolean("activebool"));
		return aCustomer;  //the tables have turned; now customers are being returned instead of dvds
	}
	
}