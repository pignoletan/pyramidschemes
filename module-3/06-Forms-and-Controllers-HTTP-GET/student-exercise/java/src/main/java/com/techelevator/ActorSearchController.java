package com.techelevator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.techelevator.dao.ActorDao;
import com.techelevator.dao.model.Actor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ActorSearchController {

	@Autowired
	private ActorDao actorDao;

	@RequestMapping ("/actorList")
	public String showSearchActorForm(@RequestParam(required=false) String actorLastName, ModelMap modelMap) {
		List<Actor> actors = new ArrayList<Actor>();
		if (actorLastName != null) {
			actorLastName = actorLastName.toUpperCase();
			actors = actorDao.getMatchingActors(actorLastName);
		}
		modelMap.put("actors",actors);
		return "actorList";
	}

}
