package com.techelevator;

import com.techelevator.dao.CustomerDao;
import com.techelevator.dao.model.Customer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CustomerSearchController {

    @Autowired
    private CustomerDao customerDao;
    
    @RequestMapping("/customerList")
    public String showCustomerInputPage(ModelMap modelMap) {
    	List<Customer> customers = new ArrayList<Customer>(); //empty arrayList to prevent error 500s
    	modelMap.put("customers",customers);
    	return "customerList";
    }
    
    @RequestMapping("/customerListSearch")
    public String showCustomerSearchPage(@RequestParam String searchBy, @RequestParam String sortBy, ModelMap modelMap) {
    	List<Customer> customers = customerDao.searchAndSortCustomers(searchBy, sortBy);
    	modelMap.put("customers",customers);
    	return "customerList";
    }

}