package com.techelevator;

import com.techelevator.dao.FilmDao;
import com.techelevator.dao.model.Film;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * FilmSearchController
 */
@Controller
public class FilmSearchController {

    @Autowired
    FilmDao filmDao;

    @RequestMapping("/filmList")
    public String showFilmSearchForm(ModelMap modelMap) {
    	List<Film> films = new ArrayList<Film>();  //empty array to prevent error 500s
    	modelMap.put("films",films);
        return "filmList";
    }

    @RequestMapping("/filmListSearch")
    public String searchFilms(@RequestParam (defaultValue="0") int minLength, @RequestParam (defaultValue="0") int maxLength,
    		@RequestParam String genre, ModelMap modelMap) {
    	List<Film> films = filmDao.getFilmsBetween(genre,minLength,maxLength);
    	modelMap.put("films",films);
        return "filmList";
    }

}