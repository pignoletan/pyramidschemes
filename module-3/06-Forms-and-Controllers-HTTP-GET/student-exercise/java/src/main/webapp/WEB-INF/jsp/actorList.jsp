<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Actors Search"/>

<%@include file="common/header.jspf"%>

<c:url var="formAction" value="/actorList" />

<form method="GET" action="${formAction}">
	<div>
		<label for="actorLastName">Actor's Last Name:</label> 
		<input type="text" name="actorLastName" id="actorLastName" />
	</div>
	<input type="submit" value="Search for actor by name" />
</form>

<p>Name</p>

<c:forEach var="actor" items="${actors}">

	<p>${actor.firstName} ${actor.lastName}</p>

</c:forEach>

<%@include file="common/footer.jspf"%>