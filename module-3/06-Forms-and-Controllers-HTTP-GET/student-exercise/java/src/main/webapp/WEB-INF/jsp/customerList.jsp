<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Customers List"/>

<%@include file="common/header.jspf"%>

<c:url var="formAction" value="/customerListSearch" />
<form method="GET" action="${formAction}">
	<div>
		<label for="searchBy">Enter a partial name: </label> 
		<input type="text" name="searchBy" id="searchBy" />
	</div>
	<div>
		<label for="sortBy">Select Status:</label> 
		<select name="sortBy"	id="sortBy">
			<option value="true">Active</option>
			<option value="false">Inactive</option>
		</select>
	</div>
	
	<input type="submit" value="Search for customers" />
</form>

<table>
<tr>
<th>Name</th>
<th>Email</th>
<th>Active</th>
</tr>

<c:forEach var="customer" items="${customers}">

	<tr>
	<td>${customer.firstName} ${customer.lastName}</td>
	<td>${customer.email}</td>
	<c:choose>
		<c:when test="${customer.active}">
			<td>Yes</td>
		</c:when>
		<c:otherwise>
			<td>No</td>
		</c:otherwise>
	</c:choose>
	</tr>

</c:forEach>

</table>

<%@include file="common/footer.jspf"%>