<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Favorite Color</title>
</head>
<body>
	<c:url var="formAction" value="/favoriteFood" />
	<form method="POST" action="${formAction}">

		<div>
			<label for="color">What is your favorite color?</label>
			<input type="text" name="color" id="color" />
		</div>
		
		<input type="submit" value="I'm satisfied with my answer" />
	
	</form>
</body>
</html>