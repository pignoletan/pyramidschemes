<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Favorite Season</title>
</head>
<body>
	<c:url var="formAction" value="/allAnswers" />
	<form method="POST" action="${formAction}">

		<div>
			<label for="season">What is your favorite season?</label>
			<select name="season"	id="season">
			<option value="Summer">Summer</option>
			<option value="Winter">Winter</option>
			<option value="Fall">Fall</option>
			<option value="Spring">Spring</option>
		</select>
		</div>
		
		<input type="submit" value="But actually it's autumn" />
	
	</form>
</body>
</html></html>