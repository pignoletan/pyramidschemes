<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>Favorite color: ${answers.color}</p>
	<p>Favorite food: ${answers.food}</p>
	<p>Favorite season: ${answers.season}</p>
</body>
</html>