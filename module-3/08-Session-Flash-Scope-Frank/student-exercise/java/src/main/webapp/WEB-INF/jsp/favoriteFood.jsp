<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Favorite Food</title>
</head>
<body>
	<c:url var="formAction" value="/favoriteSeason" />
	<form method="POST" action="${formAction}">

		<div>
			<label for="food">What is your favorite food?</label>
			<input type="text" name="food" id="food" />
		</div>
		
		<input type="submit" value="I know it's disgusting but it's my favorite anyways" />
	
	</form>
</body>
</html>