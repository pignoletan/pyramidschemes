package com.techelevator;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.FavoriteAnswers;

@Controller 
public class HelloController {

	@RequestMapping("/greeting")
	public String displayGreeting() {
		
		return "greeting";
	}
	
	@RequestMapping("/")
	public String displayFavoriteColor() {
		
		return "favoriteColor";
	}
	
	@RequestMapping(path ="favoriteFood",method=RequestMethod.POST)
	public String recordFavoriteColor(@RequestParam String color, HttpSession session) {
		FavoriteAnswers answers = new FavoriteAnswers();
		answers.setColor(color);
		session.setAttribute("answers",answers);
		return "redirect:/favoriteFood";
	}
	
	@RequestMapping(path="favoriteFood",method=RequestMethod.GET)
	public String displayFavoriteFood() {
		return "/favoriteFood";
	}
	
	@RequestMapping(path ="favoriteSeason",method=RequestMethod.POST)
	public String recordFavoriteFood(@RequestParam String food, HttpSession session) {
		FavoriteAnswers answers = (FavoriteAnswers)session.getAttribute("answers");
		answers.setFood(food);
		session.setAttribute("answers",answers);
		return "redirect:/favoriteSeason";
	}
	
	@RequestMapping(path="favoriteSeason",method=RequestMethod.GET)
	public String displayFavoriteSeason() {
		return "/favoriteSeason";
	}
	
	@RequestMapping(path ="allAnswers",method=RequestMethod.POST)
	public String recordFavoriteSeason(@RequestParam String season, HttpSession session) {
		FavoriteAnswers answers = (FavoriteAnswers)session.getAttribute("answers");
		answers.setSeason(season);
		session.setAttribute("answers",answers);
		return "redirect:/allAnswers";
	}
	
	@RequestMapping(path="allAnswers",method=RequestMethod.GET)
	public String displayAllAnswers() {
		return "/allAnswers";
	}
}
