<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Exercise 2 - Fibonacci 25</title>
		<style>
			li {
				list-style-type: none;
			}
		</style>
	</head>
	<body>
		<h1>Exercise 2 - Fibonacci 25</h1>
		<c:set var="previousNum" value="1" />
		<c:set var="curNum" value="1" />
		<c:set var="temp" value="0" />
		<ul>
			<%--
				Add a list item (i.e. <li>) for each of the first 25 numbers in the Fibonacci sequence
				
				See exercise2-fibonacci.png for example output
			 --%>
			 
			 
			 
			 <c:forEach begin="1" end="25" var="entry">
			 	<li>${curNum}</li>
			 	<c:set var="temp" value="${curNum}" />
			 	<c:set var="curNum" value="${curNum + previousNum}" />
			 	<c:set var="previousNum" value="${temp}" />
			 </c:forEach>
		</ul>
	</body>
</html>