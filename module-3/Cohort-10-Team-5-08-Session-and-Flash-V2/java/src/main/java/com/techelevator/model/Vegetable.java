package com.techelevator.model;

public class Vegetable {

	private String type;
	private double price;
	private int quantity;
	
	public Vegetable() {};
	
	public Vegetable(String type, int quantity, double itemPrice) {
		this.type = type;
		this.quantity = quantity;
		this.price = itemPrice;
	}

	/**
	 * @return the name
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	


	


	/**
	 * @return the itemPrice
	 */
	public double getPrice() {
		return price;
	}


	/**
	 * @param itemPrice the itemPrice to set
	 */
	public void setPrice(double itemPrice) {
		this.price = itemPrice;
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	

}
