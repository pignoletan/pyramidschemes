package com.techelevator.validation.controller;


import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techelevator.validation.model.SignUp;

@Controller
public class UserController {

	// GET: /
	// Display homePage view with greeting and invitation to sign up for mailing list 
	
	@RequestMapping(path="/", method=RequestMethod.GET)
	public String getMainScreen() {
		return "homePage";
	}
	
	// GET: /mailingList
	// Display empty mailingList view 
	
	@RequestMapping(path="/mailingList", method=RequestMethod.GET)
	public String showRegisterForm(Model modelHolder) {         // Define ModelMap called modelHolder (refer to comments at end of this file if you are confused about Model vs ModelMap
		if( ! modelHolder.containsAttribute("SignUp")) {        // If the modelHolder ModelMap does not have a "SignUp" entry
			modelHolder.addAttribute("SignUp", new SignUp());   //    add one
		}
		return "mailingList";                                   // return this View name
	}
	
	
	// POST: /mailingList
	// 
	//     Validate the model and redirect to confirmation (if successful) 
	//     or return the mailingList view (if validation fails)
	//
	//****************************************************************************************************************** 
	// Details on new items used in this controller
	//******************************************************************************************************************
	// 
	// @VALID
	//
	//     Request the controller to perform Spring validation on the input object it is associated with (the one it's on)
	//
	// @ModelAttribute("name")   POJO-class-name   POJO-object
	//
	//     Bind a ModelMap to a POJO when used as a parameter in a controller.
	//
	//     The "name" specified is the name one would use in a View to reference the data in the ModelMap.  
	//     This is typically done in the modelAttribute= reference on the form for the data in the View.
	//
	//     Example:  @Valid @ModelAttribute("SignUp") SignUp registerFormValues
	//
	//               Validate the data in the "SignUp" entry in the ModelMap - @Valid @ModelAttribute("SignUp")
	//                   using the validation annotations in the SignUp POJO - SignUp 
	//                   and define a method parameter for the POJO          - registerFormValues
	//
	//               To access/reference the POJO data in a View one would specify - modelAttribute="SignUp"
	//
	// BindingResult 
	// 
	//      Holds the result of a validation and binding and contains errors that may have occurred. 
	//      The BindingResult must come right after the model object that is validated 
	//          or else Spring fails to validate the object and throws an exception.
	//
	// MODEL_KEY_PREFIX
	// 
	//      A constant defined for BindingResult to be used when constructing the key for an entry in a Map.  
	//      The name of the Model for the data in the BindingResult object must be appended to the MODEL-KEY_PREFIX
	//          when used in a data validation process.
	//
	// FlashMap
	//
	//      A FlashMap provides a way for one request to store attributes intended for use in another. 
	//      Most commonly needed when redirecting from one URL to another.
    //      A  FlashMap is saved before the redirect and is made available after the redirect and removed immediately.
	//
	//      Unlike a SessionMap, the FlashMap is only available to the next Http request
	// 
	// RedirectAttributes
	//
	//      Allows access to the FlashMap created when redirection to a URL occurs 
	//  
	
	@RequestMapping(path="/mailingList", method=RequestMethod.POST)
	public String submitRegisterForm(
			@Valid @ModelAttribute("SignUp") SignUp registerFormValues, // Validate the data in the "SignUp" ModelMap entry using the SignUp POJO
			BindingResult result,                                       // Object to hold the result of validation
			RedirectAttributes flashData                                // Obtain access to the FlashMap with the name "flashData"
	){
		if(result.hasErrors()) {                                        // If there are validation errors
			flashData.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "SignUp", result);  // Add the data in BindingResult object to the FlashMap
			                                                                                 //     and give it a unique ID
			flashData.addFlashAttribute("SignUp", registerFormValues);                       // Store the data from the form object in the FlashMap 
			return "redirect:/mailingList";
		}
	// if no validation errors, add the entry "message" with the message given to the FlashMap
		    flashData.addFlashAttribute("message", "You have successfully registered.");
		
		return "redirect:/confirmation";                                // Go to the confirmation URL
	}
	
	
	// GET: /confirmation
	// Return the confirmation view
	@RequestMapping(path="/confirmation", method=RequestMethod.GET)
	public String showConfirmationView() {
		return "confirmation";
	}
	
	/**********************************************************************************************************************
	* Model:    An interface that requires four addAttribute and one mergeAttribute method and one containsAttribute method
	* ModelMap: implements Map interface.
	*	
	* Model is to ModelMap as List is to ArrayList.  i.e. The List interface may be used to instantiate an ArrayList
	*                                                     List<String> aList = new ArrayList<String>();
	*
	***********************************************************************************************************************/
	
}
