<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Main Page</title>
</head>
<body>
<ul>
<li><a href="shoppingCart">View Cart</a></li>
</ul>
<h2>Garden Vegetable Seeds</h2>
<ul>
<li><a href="bellPeppers">Bell Peppers</a></li>
<li><a href="carrots">Carrots</a></li>
<li><a href="cucumbers">Cucumbers</a></li>
<li><a href="heirloomTomatoes">Heirloom Tomatoes</a></li>
<li><a href="radishes">Radishes</a></li>
</ul>
</body>
</html>