<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Hello Spring MVC</title>
		<c:url value="/css/squirrelparty.css" var="cssHref" />
		<link rel="stylesheet" href="${cssHref}">
	</head>
	<body>
		<h1>Hello, Fellow Squirrel Party Enthusiast!</h1>
		<c:url var="picturePath" value="/etc/forDummies.png"/>
		<img src="${picturePath}" alt="Craig's Glorious Book"/>
		
		<c:url var="reviewInputPath" value="reviewInput"/>
		<p><a href="${reviewInputPath}">Write Your Own Review!</a></p>
		
		<c:forEach var="review" items="${reviews}">
		
			<div class="review">
			
			<p><a class="bold">${review.title} </a><a>(${review.username})</a></p>
			
			<p>${review.dateSubmitted}</p>
			
			<c:url var="starPath" value="/etc/star.png"/>
			<c:forEach begin="1" end="${review.rating}">
				<img src="${starPath}" alt="A shiny star!"/>
			</c:forEach>
			
			<p>${review.text}</p>
			
			</div>
		
		</c:forEach>
	</body>
	
</html>