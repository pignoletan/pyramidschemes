<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<c:url var="formAction" value="/greeting" />
	<form method="POST" action="${formAction}">

		<div>
			<label for="username">You are: </label> <input type="text"
				name="username" id="username" />
		</div>

		<div>
		<label for="rating">Your rating: </label> 
		<select name="rating"	id="rating">
			<option value="1">1 Star</option>
			<option value="2">2 Stars</option>
			<option value="3">3 Stars</option>
			<option value="4">4 Stars</option>
			<option value="5">5 Stars</option>
		</select>
		</div>
		
		<div>
			<label for="title">Name your review: </label> <input type="text"
				name="title" id="title" />
		</div>

		<div>
			<label for="text">Say something, say anything:</label> <input type="text"
				name="text" id="text" />
		</div>

		<input type="submit" value="Tell us how you really feel!" />

	</form>

</body>
</html>