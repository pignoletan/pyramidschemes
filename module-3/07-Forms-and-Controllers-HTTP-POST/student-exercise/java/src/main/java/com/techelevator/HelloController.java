package com.techelevator;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;

@Controller
public class HelloController {

	@Autowired 
	private ReviewDao reviewDao;
	
	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String displayGreeting(HttpSession session, ModelMap modelMap) {
		List<Review> allReviews = reviewDao.getAllReviews();
		modelMap.put("reviews",allReviews);
		return "greeting";
	}
	
	@RequestMapping(path = "/greeting", method = RequestMethod.POST)
	public String makeNewReview(HttpSession session, ModelMap modelMap, @ModelAttribute Review aReview) {
		aReview.setDateSubmitted(LocalDateTime.now());
		reviewDao.save(aReview);
		List<Review> allReviews = reviewDao.getAllReviews();
		modelMap.put("reviews",allReviews);
		return "redirect:/";
	}
	
	@RequestMapping("/reviewInput")
	public String displayReviewInputPage() {
		
		return "reviewInput";
	}
}
