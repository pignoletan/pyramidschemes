package com.techelevator;

public class Employee {
	private int employeeId = 0;
	private String firstName = "";
	private String lastName = "";
	private String department = "";
	private double annualSalary = 0.0;
	
	//Ctors go here
	
	public Employee() {
		employeeId = 0;
		firstName = "";
		lastName = "";
		department = "";
		annualSalary = 0.0;
	}
	public Employee(int employeeId,String firstName,String lastName,double salary) {
		this.employeeId = employeeId;
		this.firstName = firstName;
		this.lastName = lastName;
		annualSalary = salary;
	}
	
	
	//Getters go here
	
	public int getEmployeeId() {
		return employeeId;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getDepartment() {
		return department;
	}
	public double getAnnualSalary() {
		return annualSalary;
	}
	public String getFullName() {
		return lastName + ", " + firstName;
	}
	
	//Setters go here
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
	//Additional methods
	
	public void raiseSalary(double percent) {
		percent /= 100;
		annualSalary *= 1 + percent;
	}
	
}
