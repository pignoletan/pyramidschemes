package com.techelevator;

public class Airplane {
	private String planeNumber = "";
	private int bookedFirstClassSeats = 0;
	private int totalFirstClassSeats = 0;
	private int bookedCoachSeats = 0;
	private int totalCoachSeats = 0;
	
	//Ctors go here
	
	public Airplane() {
		planeNumber = "";
		bookedFirstClassSeats = 0;
		totalFirstClassSeats = 0;
		bookedCoachSeats = 0;
		totalCoachSeats = 0;
	}
	
	public Airplane(String planeNumber, int totalFirstClassSeats, int totalCoachSeats) {
		this.planeNumber = planeNumber;
		this.totalFirstClassSeats = totalFirstClassSeats;
		this.totalCoachSeats = totalCoachSeats;
		bookedFirstClassSeats = 0;
		bookedCoachSeats = 0;
	}
	
	//Getters go here
	
	public String getPlaneNumber() {
		return planeNumber;
	}
	public int getBookedFirstClassSeats() {
		return bookedFirstClassSeats;
	}
	public int getTotalFirstClassSeats() {
		return totalFirstClassSeats;
	}
	public int getBookedCoachSeats() {
		return bookedCoachSeats;
	}
	public int getTotalCoachSeats() {
		return totalCoachSeats;
	}
	public int getAvailableFirstClassSeats() {
		return totalFirstClassSeats - bookedFirstClassSeats;
	}
	public int getAvailableCoachSeats() {
		return totalCoachSeats - bookedCoachSeats;
	}
	
	//Additional methods
	
	public boolean reserveSeats(boolean forFirstClass, int totalNumberOfSeats) {
		if (!forFirstClass) {
			if (totalNumberOfSeats > getAvailableCoachSeats()) {
				return false;
			}
			bookedCoachSeats += totalNumberOfSeats;
			return true;
		}
		if (totalNumberOfSeats > getAvailableFirstClassSeats()) {
			return false;
		}
		bookedFirstClassSeats += totalNumberOfSeats;
		return true;
	}
	
}
