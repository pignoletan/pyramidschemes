package com.techelevator;

public class FruitTree {
	private String typeOfFruit = "";
	private int piecesOfFruitLeft = 0;
	
	//Constructors go here
	
	public FruitTree() {
		typeOfFruit = "";
		piecesOfFruitLeft = 0;
	}
	
	public FruitTree(String typeOfFruit, int startingPiecesOfFruit) {
		this.typeOfFruit = typeOfFruit;
		piecesOfFruitLeft = startingPiecesOfFruit;
	}
	
	//Getters go here
	
	public String getTypeOfFruit() {
		return typeOfFruit;
	}
	public int getPiecesOfFruitLeft() {
		return piecesOfFruitLeft;
	}
	
	//Additional methods
	
	public boolean pickFruit(int numberOfPiecesToRemove) {
		if (piecesOfFruitLeft == 0) {
			return false;
		} else {
			piecesOfFruitLeft -= numberOfPiecesToRemove;
			return true;
		}
	}
	
}
