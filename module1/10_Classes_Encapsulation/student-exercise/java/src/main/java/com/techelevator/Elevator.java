package com.techelevator;

public class Elevator {
	private int currentFloor = 1;
	private int numberOfFloors = 0;
	private boolean doorOpen = false;
	
	//Constructors go here
	
	public Elevator() {
		currentFloor = 1;
		numberOfFloors = 1;
		doorOpen = false;
	}
	public Elevator(int totalNumberOfFloors) {
		currentFloor = 1;
		numberOfFloors = totalNumberOfFloors;
		doorOpen = false;
	}
	
	//Getters go here
	
	public int getCurrentFloor() {
		return currentFloor;
	}
	public int getNumberOfFloors() {
		return numberOfFloors;
	}
	public boolean isDoorOpen() {
		return doorOpen;
	}
	
	//Additional methods go here
	
	public void openDoor() {
		doorOpen = true;
	}
	public void closeDoor() {
		doorOpen = false;
	}
	public void goUp(int desiredFloor) {
		if (!doorOpen&&desiredFloor > currentFloor) {
			currentFloor = desiredFloor;
			if (currentFloor > numberOfFloors) {
				currentFloor = numberOfFloors;
			}
		}
	}
	public void goDown(int desiredFloor) {
		if (!doorOpen&&desiredFloor < currentFloor) {
			currentFloor = desiredFloor;
			if (currentFloor < 1) {
				currentFloor = 1;
			}
		}
	}
}
