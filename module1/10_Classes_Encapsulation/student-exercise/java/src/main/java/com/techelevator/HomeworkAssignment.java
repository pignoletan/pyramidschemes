package com.techelevator;

public class HomeworkAssignment {
	
	private int totalMarks = 0;
	private int possibleMarks = 0;
	private String submitterName = "";
	
	
	//Constructors go here
	public HomeworkAssignment() {
		totalMarks = 0;
		possibleMarks = 0;
		submitterName = "";
	}
	
	public HomeworkAssignment(int possibleMarks) {
		totalMarks = 0;
		this.possibleMarks = possibleMarks;
		submitterName = "";
	}
	

	//Getters go here
	
	public int getTotalMarks() {
		return totalMarks;
	}



	public int getPossibleMarks() {
		return possibleMarks;
	}



	public String getSubmitterName() {
		return submitterName;
	}

	public String getLetterGrade() {
		int percent = (100 * totalMarks)/possibleMarks;
		if (percent < 60) {
			return "F";
		}
		if (percent < 70) {
			return "D";
		}
		if (percent < 80) {
			return "C";
		}
		if (percent < 90) {
			return "B";
		}
		return "A";
	}

	//Setters go here

	public void setTotalMarks(int totalMarks) {
		this.totalMarks = totalMarks;
	}



	public void setSubmitterName(String submitterName) {
		this.submitterName = submitterName;
	}

}
