package com.techelevator;

public class IntroToClassesCard {
	
	public static void main(String[] args) {
        
		System.out.println("cardShape is... " + PlayingCard.getCardShape());
        /*
         *  This program will demonstrate several concepts presented in the Java cohort
		 *  in the topic Introduction to Classes 		
        */
	//  data-type   name  = create a reference to object
		PlayingCard aCard = new PlayingCard();  // instantiate a PlayingCard object
		                                        // initialize using the default constructor

	// to use an object (have an object perform a behavior): object.method()  
		
		aCard.displayCard();
		
		PlayingCard anotherCard = new PlayingCard(1, "Spades");  // using the 3-arg ctor
		anotherCard.displayCard();
		
		
		System.out.println("Value in anotherCard is: " + anotherCard.getCardValue() + " and is a " + anotherCard.getCardSuit());
		System.out.println("      Value in aCard is: " + aCard.getCardValue()       + " and is a " + aCard.getCardSuit());

		anotherCard.setCardValue(13);
		anotherCard.displayCard();
		System.out.println("Value in anotherCard is: " + anotherCard.getCardValue() + " and is a " + anotherCard.getCardSuit());
		
		PlayingCard card1 = new PlayingCard(1,"Spades");
		PlayingCard card2 = new PlayingCard(1,"Spades");
		
		//But are they equal???
		System.out.println(card1.equals(card2));
		System.out.println(card1.equals(1));
		
		PlayingCard newCard = new PlayingCard();
		newCard.clone(card1);
		card1.displayCard();
		PlayingCard.setCardShape("Pyramid");
		card1.displayCard();
		newCard.displayCard();
		
		System.out.println("card1 is: " + card1);
		System.out.println("Put em up, Sarah!");
	}
}
		 