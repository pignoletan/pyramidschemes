package com.techelevator;

public class PlayingCard {
//******************************************************************************
// Class member data - attributes of an[sic] class object
//******************************************************************************
	private String cardSuit     = "";  // private means only members of the class
	private int    cardValue    = 0;   //     can access it
	private String cardColor    = "";  // only member functions (methods) 
	static private String cardShape    = "Rectangle";
	static private String cardMaterial = "Papyrus";
	static private String backColor    = "Neon Silver";
	
//******************************************************************************	
// Class methods - behavior of a class object
//******************************************************************************

// ----- Constructors -----------------------------------------------------------

// Create constructors for the class
// a constructor: (1) same name as the class; (2) returns nothing (not even void)

// Default ctor - initialize object to default values - Create a default card
	public PlayingCard() {   
		cardSuit     = "Joker";
		cardValue    = 0;
		determineColor();
	}
// 3-arg ctor initialize to args passed and default everything else	
	public PlayingCard(int aValue, String cardSuit) {
		this.cardSuit     = cardSuit;    // use the parameter passed as aSuit
		cardValue    = aValue;   // use the parameter passed as aValue
		determineColor();   // use the private members-only method to set the color
	}
	
// ----- Getter Methods --------------------------------------------------------
	
public String getCardSuit() {
		return cardSuit;
	}
	public int getCardValue() {
		return cardValue;
	}
	public String getCardColor() {
		return cardColor;
	}
	public static String getCardShape() {
		return cardShape;
	}
	public static String getCardMaterial() {
		return cardMaterial;
	}
	public static String getBackColor() {
		return backColor;
	}

// ----- Setter Methods -----------------------------------------------------------
	/**
	 * @param cardSuit the cardSuit to set
	 */
	public void setCardSuit(String aSuit) {
		cardSuit = aSuit;  // this. is not required because the parameter name is different than the data member name
	}
	/**
	 * @param cardValue the cardValue to set
	 */
	public void setCardValue(int cardValue) {
		this.cardValue = cardValue;  // this. - reference the data in the object used to invoke the method
	}                                //       - required if the parameter name matches the data member name
	
	private void determineColor() { //we gonna set the card color based on da suit
		if (cardSuit.equals("Spades")||cardSuit.equals("Clubs")||cardSuit.equals("Joker")) {
			cardColor = "Black";
		} else {
			cardColor = "Red";
		}
	}
	public static void setCardShape(String cardShape) {
		PlayingCard.cardShape = cardShape;
	}
	
	

// ----- Additional Methods -----------------------------------------------------------

	// Display the attributes of a PlayingCard
//  access    return    method
//  modified  type      name      (parameters)
	public    void    displayCard() {
		System.out.println("     Suit: " + cardSuit);
		System.out.println("    Value: " + cardValue);
		System.out.println("    Color: " + cardColor);
		System.out.println("    Shape: " + cardShape);
		System.out.println("   Medium: " + cardMaterial);
		System.out.println("BackColor: " + backColor);
		System.out.println("---------------------------------------------------");
	}
	
	public boolean equals(PlayingCard otherCard) {
		if (this.cardValue == otherCard.cardValue
			&&this.cardSuit.equals(otherCard.cardSuit)) {
			return true;
		}
		return false;
	}
	public boolean equals(int aValue) {
		if (this.cardValue == aValue) {
			return true;
		}
		return false;
	}
	
	public void clone(PlayingCard otherCard) {
		cardSuit     = otherCard.cardSuit;
		cardValue    = otherCard.cardValue;
		determineColor();
	}
	
	@Override //We want Eclipse to verify this override is valid
	public String toString() {
		return "PlayingCard [cardSuit=" + cardSuit + ", cardValue=" + cardValue + ", cardColor=" + cardColor
				+ ", cardShape=" + cardShape + ", cardMaterial=" + cardMaterial + ", backColor=" + backColor + "]";
	}
	
}
