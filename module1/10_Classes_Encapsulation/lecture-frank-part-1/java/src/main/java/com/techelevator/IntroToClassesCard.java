package com.techelevator;

public class IntroToClassesCard {
	
	public static void main(String[] args) {
        
        /*
         *  This program will demonstrate several concepts presented in the Java cohort
		 *  in the topic Introduction to Classes 		
        */
	//  data-type   name  = create a reference to object
		PlayingCard aCard = new PlayingCard();  // instantiate a PlayingCard object
		                                        // initialize using the default constructor

	// to use an object (have an object perform a behavior): object.method()  
		
		aCard.displayCard();
		
		PlayingCard anotherCard = new PlayingCard(1, "Spades");  // using the 2-arg ctor
		anotherCard.displayCard();
		
		
		System.out.println("Value in anotherCard is: " + anotherCard.getCardValue() + " and is a " + anotherCard.getCardSuit());
		System.out.println("      Value in aCard is: " + aCard.getCardValue()       + " and is a " + aCard.getCardSuit());

		anotherCard.setCardValue(13);
		anotherCard.displayCard();
		System.out.println("Value in anotherCard is: " + anotherCard.getCardValue() + " and is a " + anotherCard.getCardSuit());
		
		PlayingCard card1 = new PlayingCard(1, "Spades");  // using the 2-arg ctor
		PlayingCard card2 = new PlayingCard(1, "Spades");  // using the 2-arg ctor
		
		// Is card1 equal to card2?
		
		if (card1.equals(card2) ) {
			System.out.println("They are equal");
		}
			else {
				System.out.println("They are not equal");
			}
		
	   if (card1.equals(1) ) {
   		   System.out.println("They are equal");
	      }
	  	 else {
			System.out.println("They are not equal");
		}
	   
	   PlayingCard newCard = new PlayingCard();  // Make a default card
	   newCard.clone(card1);                     // Copy card1 into newCard;
	   
	   card1.displayCard();
	   newCard.displayCard();
	   
	   
	}
		
	}

		 

