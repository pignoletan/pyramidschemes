package com.techelevator;

public class PlayingCard {
//******************************************************************************
// Class member data - attributes of an class object
//******************************************************************************

	// static data - only one regardless of the number of objects (even if there are no objects)
	
	static private String cardShape    = "Rectangle"; 
	static private String cardMaterial = "Paper";
	static private String backColor    = "Blue";

	// instance data - one copy for every instance of the object
	
	       private String cardSuit     = ""; // private means only members of the class
	       private int cardValue       = 0;  // can access it
	       private String cardColor    = ""; // only member functions (methods) can access
	
//******************************************************************************	
// Class methods - behavior of a class object
//******************************************************************************

// ----- Constructors -----------------------------------------------------------

// Create constructors for the class
// a constructor: (1) same name as the class; (2) returns nothing (not even void)

// Default ctor - initialize object to default values - Create a default card
	public PlayingCard() {
		cardSuit = "Joker";
		cardValue = 0;
		determineColor();   // use the private method to set the color
		
	}

// 2-arg ctor initialize to args passed and default everything else	
	public PlayingCard(int aValue, String cardSuit) {
		this.cardSuit = cardSuit;  // use the parameter passed as aSuit
		cardValue = aValue;        // use the parameter passed as aValue
		determineColor();          // use the private method to set the color
	}

// ----- Getter Methods --------------------------------------------------------

	public String getCardSuit() {
		return cardSuit;
	}

	public int getCardValue() {
		return cardValue;
	}

	public String getCardColor() {
		return cardColor;
	}

	public String getCardShape() {
		return cardShape;
	}

	public String getCardMaterial() {
		return cardMaterial;
	}

	public String getBackColor() {
		return backColor;
	}

// ----- Setter Methods -----------------------------------------------------------
	/**
	 * @param cardSuit the cardSuit to set
	 */
	public void setCardSuit(String aSuit) {
		cardSuit = aSuit; // this. is not required because the parameter name is different than the data
							// member name
	}

	/**
	 * @param cardValue the cardValue to set
	 */
	public void setCardValue(int cardValue) {
		this.cardValue = cardValue; // this. - reference the data in the object used to invoke the method
	} // - required if the parameter name matches the data member name
 
	
	/**
	 * @param cardShape the cardShape to set
	 */
	public static void setCardShape(String cardShape) {
		PlayingCard.cardShape = cardShape;
	}

	private void determineColor() { // set the cardColor based on the cardSuit
		if (cardSuit.equals("Spades") 
		 || cardSuit.equals("Clubs")
		 || cardSuit.equals("Joker")) {
			cardColor = "Black";
		} else {
			cardColor = "Red";
		}
	}

// ----- Additional Methods -----------------------------------------------------------

// clone - make a copy of a PlayingCard
	public void clone(PlayingCard otherCard) {
		cardSuit = otherCard.cardSuit;
		cardValue = otherCard.cardValue;
		determineColor();   // use the private method to set the color
	}
	
	
	
// overrides - methods to replace existing methods in a parent or allow variations of a method

// Override - same name as a parent method
// Overload - same names as existing method with different parameters

public boolean equals(PlayingCard otherCard) {
	if ( cardValue == otherCard.cardValue
      && cardSuit.equals(otherCard.cardSuit)){
		 return true;
	}
	else {
		return false;
	}
}



// overload of the equals - same method different arguments
public boolean equals(int aValue) {
	if ( cardValue == aValue){
		 return true;
	}
	else {
		return false;
	}
}
public boolean equalValue(PlayingCard otherCard) {
	if ( cardValue == otherCard.cardValue){
		 return true;
	}
	else {
		return false;
	}
}
	
	
	
/* (non-Javadoc)
 * @see java.lang.Object#toString()
 */
@Override    // We want Eclipse to verify this is a proper override of parent method
public String toString() {
	return "PlayingCard [cardSuit=" + cardSuit + ", cardValue=" + cardValue + ", cardColor=" + cardColor
			+ ", cardShape=" + cardShape + ", cardMaterial=" + cardMaterial + ", backColor=" + backColor + "]";
}

// Display the attributes of a PlayingCard
//  access    return    method
//  modified  type      name      (parameters)
	public void displayCard() {
		System.out.println("     Suit: " + cardSuit);
		System.out.println("    Value: " + cardValue);
		System.out.println("    Color: " + cardColor);
		System.out.println("    Shape: " + cardShape);
		System.out.println("   Medium: " + cardMaterial);
		System.out.println("BackColor: " + backColor);
		System.out.println("---------------------------------------------------");
	}

}
