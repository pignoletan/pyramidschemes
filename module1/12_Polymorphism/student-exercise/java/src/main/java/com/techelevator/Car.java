package com.techelevator;

public class Car implements Vehicle {
	private boolean hasTrailer = false;
	
	public Car(boolean hasTrailer) {
		this.hasTrailer = hasTrailer;
	}
	
	public double calculateToll(int distance) {
		double toll = distance * 0.020;
		if (hasTrailer) {
			toll += 1.00;
		}
		return toll;
	}

	public boolean isHasTrailer() {
		return hasTrailer;
	}
	
	public String getVehicleType() {
		String type = "Car";
		if (hasTrailer) {
			type = type + " (with trailer)";
		} else {
			type = type + "\t\t";
		}
		return type;
	}
}
