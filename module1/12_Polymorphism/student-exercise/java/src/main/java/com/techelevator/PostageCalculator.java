package com.techelevator;

import java.util.ArrayList;
import java.util.Scanner;

public class PostageCalculator {

	public static void main(String[] args) {
		System.out.print("Please enter the weight of the package: ");
		Scanner userInput = new Scanner(System.in);
		String aLine = userInput.nextLine();
		double weight = Double.parseDouble(aLine);
		boolean proceed = false;
		while (!proceed) {
			System.out.print("(P)ounds or (O)unces? ");
			aLine = userInput.nextLine();
			if (aLine.equals("O")) {
				proceed = true;
			} else if (aLine.equals("P")) {
				weight *= 16;
				proceed = true;
			} else {
				System.out.println("Error: Invalid input");
			}
		}
		System.out.print("What distance will it be travelling? ");
		aLine = userInput.nextLine();
		int distance = Integer.parseInt(aLine);
		
		ArrayList<DeliveryDriver> allDeliveryMethods = new ArrayList<DeliveryDriver>();
		PostServiceFirstClass firstClass = new PostServiceFirstClass();
		PostServiceSecondClass secondClass = new PostServiceSecondClass();
		PostServiceThirdClass thirdClass = new PostServiceThirdClass();
		FexEd copyrightInfringement = new FexEd();
		SPU fourDays = new SPU("4-day Ground");
		SPU twoDays = new SPU("2-day Business");
		SPU oneDay = new SPU("Next Day");
		allDeliveryMethods.add(firstClass);
		allDeliveryMethods.add(secondClass);
		allDeliveryMethods.add(thirdClass);
		allDeliveryMethods.add(copyrightInfringement);
		allDeliveryMethods.add(fourDays);
		allDeliveryMethods.add(twoDays);
		allDeliveryMethods.add(oneDay);
		System.out.println("Delivery Method\t\t\tCost");
		for (DeliveryDriver type : allDeliveryMethods) {
			System.out.print(type.getDeliveryType() + "\t");
			System.out.printf("%.2f",type.calculateRate(distance, weight));
			System.out.println();
		}
	}

}
