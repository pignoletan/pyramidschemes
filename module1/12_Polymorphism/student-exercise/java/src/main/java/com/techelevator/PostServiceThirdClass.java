package com.techelevator;

public class PostServiceThirdClass extends PostalService {
	private static final int ZEROTOTWOOUNCES = 200;
	private static final int THREETOEIGHTOUNCES = 220;
	private static final int NINETOFIFTEENOUNCES = 240;
	private static final int ONETOTHREEPOUNDS = 1500;
	private static final int FOURTOEIGHTPOUNDS = 1600;
	private static final int MORETHANNINEPOUNDS = 1700;
	
	public PostServiceThirdClass() {
		super(ZEROTOTWOOUNCES,THREETOEIGHTOUNCES,NINETOFIFTEENOUNCES,ONETOTHREEPOUNDS,FOURTOEIGHTPOUNDS,MORETHANNINEPOUNDS);
	}
	
	public String getDeliveryType() {
		return "Postal Service (Third Class)";
	}
}
