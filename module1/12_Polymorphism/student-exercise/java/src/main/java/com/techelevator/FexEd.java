package com.techelevator;

public class FexEd implements DeliveryDriver {

	
	public double calculateRate(int distance,double weight) {
		double rate = 20.00;
		if (distance > 500) {
			rate += 5.0;
		}
		if (weight > 48) {
			rate += 3.0;
		}
		return rate;
	}
	
	public String getDeliveryType() {
		return "FexEd\t\t\t";
	}
}
