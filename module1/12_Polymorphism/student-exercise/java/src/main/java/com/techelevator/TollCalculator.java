package com.techelevator;

import java.util.ArrayList;
import java.util.Random;

public class TollCalculator {

	public static void main(String[] args) {
		ArrayList<Vehicle> allVehicles = new ArrayList<Vehicle>();
		Car corolla = new Car(false);
		allVehicles.add(corolla);
		Car trailerCar = new Car(true);
		allVehicles.add(trailerCar);
		Truck bigCar = new Truck(4);
		allVehicles.add(bigCar);
		Truck manyAxles = new Truck(8);
		allVehicles.add(manyAxles);
		Tank militaryCar = new Tank();
		allVehicles.add(militaryCar);
		int totalMilesTravelled = 0;
		double totalToll = 0.0;
		System.out.println("Vehicle type\t\tMiles Travelled\t\tToll fare");
		for (Vehicle vehicle : allVehicles) {
			Random rand = new Random();
			int milesTravelled = rand.nextInt(230);
			milesTravelled += 10;
			totalMilesTravelled += milesTravelled;
			double toll = vehicle.calculateToll(milesTravelled);
			totalToll += toll;
			System.out.printf(vehicle.getVehicleType() + "\t" + milesTravelled + "\t\t\t%.2f",toll);
			System.out.println();
		}
		System.out.println("Total miles travelled: " + totalMilesTravelled);
		System.out.printf("Total toll revenue: %.2f",totalToll);
	}

}
