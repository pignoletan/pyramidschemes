package com.techelevator;

public class Truck implements Vehicle {
	int numberOfAxles = 4;
	
	public Truck(int numberOfAxles) {
		this.numberOfAxles = numberOfAxles;
	}
	
	public double calculateToll(int distance) {
		int tollRate = 40;
		if (numberOfAxles > 5) {
			tollRate = 45;
		}
		if (numberOfAxles > 7) {
			tollRate = 48;
		}
		return (((double)distance) * tollRate)/100;
	}
	public String getVehicleType() {
		return "Truck (" + numberOfAxles + " axles)\t";
	}
}
