package com.techelevator;

public abstract class PostalService implements DeliveryDriver {
	private int zeroToTwoOunces;
	private int threeToEightOunces;
	private int nineToFifteenOunces;
	private int oneToThreePounds;
	private int fourToEightPounds;
	private int moreThanNinePounds;
	
	public PostalService(int zeroToTwoOunces,int threeToEightOunces,int nineToFifteenOunces,int oneToThreePounds,
			int fourToEightPounds,int moreThanNinePounds) {
		this.zeroToTwoOunces = zeroToTwoOunces;
		this.threeToEightOunces = threeToEightOunces;
		this.nineToFifteenOunces = nineToFifteenOunces;
		this.oneToThreePounds = oneToThreePounds;
		this.fourToEightPounds = fourToEightPounds;
		this.moreThanNinePounds = moreThanNinePounds;
	}
	
	public double calculateRate(int distance,double weight) {
		int rate = zeroToTwoOunces;
		if (weight > 2) {
			rate = threeToEightOunces;
		}
		if (weight > 8) {
			rate = nineToFifteenOunces;
		}
		if (weight > 15) {
			rate = oneToThreePounds;
		}
		if (weight > 59) {
			rate = fourToEightPounds;
		}
		if (weight > 143) {
			rate = moreThanNinePounds;
		}
		return (((double)distance) * rate)/100000;
	}
	
	public abstract String getDeliveryType();
}
