package com.techelevator;

public class SPU implements DeliveryDriver {

	private String rateType = "4-day Ground";
	
	public SPU(String rateType) {
		this.rateType = rateType;
	}
	
	public double calculateRate(int distance,double weight) {
		int rate = 50;
		if (rateType.equals("2-day Business")) {
			rate = 500;
		}
		if (rateType.equals("Next Day")) {
			rate = 750;
		}
		weight /= 16;
		return (((double)rate) * weight * distance)/10000.0;
	}
	
	public String getDeliveryType() {
		String result = "SPU (" + rateType + ")\t";
		if (rateType.equals("Next Day")) {
			result = result + "\t";
		}
		return result;
	}
}
