package com.techelevator;

public class PostServiceSecondClass extends PostalService {
	private static final int ZEROTOTWOOUNCES = 350;
	private static final int THREETOEIGHTOUNCES = 400;
	private static final int NINETOFIFTEENOUNCES = 470;
	private static final int ONETOTHREEPOUNDS = 195;
	private static final int FOURTOEIGHTPOUNDS = 450;
	private static final int MORETHANNINEPOUNDS = 500;
	
	public PostServiceSecondClass() {
		super(ZEROTOTWOOUNCES,THREETOEIGHTOUNCES,NINETOFIFTEENOUNCES,ONETOTHREEPOUNDS,FOURTOEIGHTPOUNDS,MORETHANNINEPOUNDS);
	}
	
	public String getDeliveryType() {
		return "Postal Service (Second Class)";
	}
}
