package com.techelevator;

public class PostServiceFirstClass extends PostalService {
	private static final int ZEROTOTWOOUNCES = 3500;
	private static final int THREETOEIGHTOUNCES = 4000;
	private static final int NINETOFIFTEENOUNCES = 4700;
	private static final int ONETOTHREEPOUNDS = 1950;
	private static final int FOURTOEIGHTPOUNDS = 4500;
	private static final int MORETHANNINEPOUNDS = 5000;
	
	public PostServiceFirstClass() {
		super(ZEROTOTWOOUNCES,THREETOEIGHTOUNCES,NINETOFIFTEENOUNCES,ONETOTHREEPOUNDS,FOURTOEIGHTPOUNDS,MORETHANNINEPOUNDS);
	}
	
	public String getDeliveryType() {
		return "Postal Service (First Class)";
	}
}
