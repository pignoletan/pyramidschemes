package com.techelevator.farm;

public class Chicken extends FarmAnimal {
	
	public Chicken() {
		super("Chicken", "cluck!");
	}
	
	public void layEgg() {
		System.out.println("Chicken laid an egg!");
	}

	public void MakeSound() {
		System.out.println("a Chicken goes " + getSound()); //get method required because the super class rudely hordes the data
	}
	
	public void DefendYourself() {
		System.out.println("I'm a useless chicken - I must flee from your magnificience!");
	}
}