package com.techelevator.farm;

public class Cow extends FarmAnimal {

	public Cow() {
		super("Cow", "moo!");
	}
	
	public void MakeSound() {
		System.out.println("a Cow goes " + getSound()); //get method required because the super class rudely hordes the data
	}
	public void DefendYourself() {
		System.out.println("You messed with the wrong cow!");
	}
}