package com.techelevator.farm;

public interface Singable {
	//If one wants to be singable, one must implement these methods, lest they suffer the price of their arrogance
	
	//List the method signatures for all methods the interfaces uses
	public void MakeSound();
	public void DefendYourself();
}
