package com.techelevator;

import java.util.Scanner;

public class LinearConvert {

	public static void main(String[] args) {
		System.out.print("Please enter the length: ");
		Scanner myKeyboard = new Scanner(System.in);
		String aLine = myKeyboard.nextLine();
		int inputDist = Integer.parseInt(aLine);
		System.out.print("Is the measurement in (m)eter, or (f)eet? ");
		boolean proceed = false;
		while (!proceed) {
			aLine = myKeyboard.nextLine();
			if (aLine.equals("m")||aLine.contentEquals("f")) {
				proceed = true;
			} else {
				System.out.println("Error: Input either m or f");
			}
		}
		int newDist;
		if (aLine.contentEquals("m")) {
			newDist = (int)(inputDist * 3.2808399);
			System.out.println(inputDist + " meters is " + newDist + " feet.");
		} else {
			newDist = (int)(inputDist * 0.3048);
			System.out.println(inputDist + " feet is " + newDist + " meters.");
		}
	}

}
