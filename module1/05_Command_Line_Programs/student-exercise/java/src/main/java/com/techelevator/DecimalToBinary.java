package com.techelevator;

import java.util.Scanner;

public class DecimalToBinary {

	public static void main(String[] args) {
		System.out.print("Please enter in a series of decimal values (separated by spaces): ");
		Scanner myKeyboard = new Scanner(System.in);
		String aLine = myKeyboard.nextLine();
		String[] decimalNumbers = aLine.split(" ");
		for (int i = 0; i < decimalNumbers.length; i += 1) {
			boolean[] binary = new boolean[100];
			int number = Integer.parseInt(decimalNumbers[i]);
			int biggestDigit = -1;
			while (number > 0) {
				int j = 0;
				while (Math.pow(2,j) <= number) {
					j++;
				}
				binary[j] = true;
				if (j > biggestDigit) {biggestDigit = j;}
				number -= Math.pow(2,j-1);
			}
			String result = "1"; //the first digit of the display should always be 1
			for (int j = biggestDigit-1; j > 0; j--) {
				if (binary[j] == true) {
					result = result + "1";
				} else {
					result = result + "0";
				}
			}
			System.out.println(decimalNumbers[i] + " in binary is " + result);
		}
	}

}