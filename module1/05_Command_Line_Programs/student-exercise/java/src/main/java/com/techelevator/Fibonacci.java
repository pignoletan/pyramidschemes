package com.techelevator;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		System.out.print("Please enter the Fibonacci number: ");
		Scanner myKeyboard = new Scanner(System.in);
		String aLine = myKeyboard.nextLine();
		int goal = Integer.parseInt(aLine);
		int a = 0;
		int b = 1;
		while (a <= goal) {
			System.out.print(a);
			int temp = a;
			a += b;
			b = temp;
			if (a <= goal) {
				System.out.print(", ");
			}
		}
		
	}

}
