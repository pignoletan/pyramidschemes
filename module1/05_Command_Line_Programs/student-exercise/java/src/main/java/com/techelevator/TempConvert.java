package com.techelevator;

import java.util.Scanner;

public class TempConvert {

	public static void main(String[] args) {
		System.out.print("Please enter the temperature: ");
		Scanner myKeyboard = new Scanner(System.in);
		String aLine = myKeyboard.nextLine();
		int inputTemp = Integer.parseInt(aLine);
		System.out.print("Is the temperature in (C)elcius, or (F)arenheit? ");
		boolean proceed = false;
		while (!proceed) {
			aLine = myKeyboard.nextLine();
			if (aLine.equals("C")||aLine.contentEquals("F")) {
				proceed = true;
			} else {
				System.out.println("Error: Input either F or C");
			}
		}
		int newTemp;
		if (aLine.contentEquals("C")) {
			newTemp = (int)(inputTemp * 1.8) + 32;
			System.out.println(inputTemp + "C is " + newTemp + "F.");
		} else {
			newTemp = (int)((inputTemp - 32)/1.8);
			System.out.println(inputTemp + "F is " + newTemp + "C.");
		}
		
	}

}
