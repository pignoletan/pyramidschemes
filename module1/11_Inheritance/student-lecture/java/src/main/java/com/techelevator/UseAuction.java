package com.techelevator;

public class UseAuction {

	public static void main(String[] args) {
		Auction anAuction = new Auction("A.J. Coffe Cup");
		BuyoutAuction aBuyoutAuction = new BuyoutAuction("George R.R. Martin",10000);
		
		System.out.println("Item is: " + aBuyoutAuction.getItemForSale());
		System.out.println("Item is: " + anAuction.getItemForSale());
	}

}
