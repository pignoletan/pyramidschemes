package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSameFirstLast {

	SameFirstLast exercise = new SameFirstLast();
	@Test
	public void sameFirstLastTest() {
		int[] test1 = {};
		assertFalse("Does not properly test for array length=0",exercise.isItTheSame(test1));
		int[] test2 = {7};
		assertTrue("Returns false for array length=1",exercise.isItTheSame(test2));
		int[] test3 = {1,1,10,1,10,1,10,1,10,10};
		assertFalse("Returns true for array with different ends.",exercise.isItTheSame(test3));
		test3[9] = 1;
		assertTrue("Returns false for array with same ends.",exercise.isItTheSame(test3));
	}

}
