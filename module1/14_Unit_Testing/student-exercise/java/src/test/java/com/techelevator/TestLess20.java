package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestLess20 {

	Less20 exercise = new Less20();
	@Test
	public void less20Test() {
		assertTrue("18 returns false.",exercise.isLessThanMultipleOf20(18));
		assertTrue("39 returns false.",exercise.isLessThanMultipleOf20(39));
		assertTrue("238 returns false.",exercise.isLessThanMultipleOf20(238));
		assertFalse("7 returns true.",exercise.isLessThanMultipleOf20(7));
		assertFalse("200 returns true.",exercise.isLessThanMultipleOf20(200));
	}

}
