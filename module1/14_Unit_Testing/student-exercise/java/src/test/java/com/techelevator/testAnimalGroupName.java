package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class testAnimalGroupName {

	AnimalGroupName animalCode = new AnimalGroupName();	//instantiate the code to be tested
	
	@Test
	public void testGetHerd() {
		
		assertEquals("Wrong group name returned","Herd",animalCode.getHerd("Elephant"));
		assertEquals("Input is case sensitive","Herd",animalCode.getHerd("eLEPHANT"));
		assertEquals("Input is case sensitive","Herd",animalCode.getHerd("elephant"));
		assertEquals("Input is case sensitive","Herd",animalCode.getHerd("ELEPHANT"));
		assertEquals("Wrong group name returned","Crash",animalCode.getHerd("Rhino"));
		assertEquals("Wrong group name returned","Tower",animalCode.getHerd("Giraffe"));
		assertEquals("Wrong group name returned","Pride",animalCode.getHerd("Lion"));
		assertEquals("Wrong group name returned","Murder",animalCode.getHerd("Crow"));
		assertEquals("Wrong group name returned","Kit",animalCode.getHerd("Pigeon"));
		assertEquals("Wrong group name returned","Pat",animalCode.getHerd("Flamingo"));
		assertEquals("Wrong group name returned","Herd",animalCode.getHerd("Deer"));
		assertEquals("Wrong group name returned","Pack",animalCode.getHerd("Dog"));
		assertEquals("Wrong group name returned","Float",animalCode.getHerd("Crocodile"));
		assertEquals("Unknown not returned","unknown",animalCode.getHerd("Spadookadook"));
	}

}
