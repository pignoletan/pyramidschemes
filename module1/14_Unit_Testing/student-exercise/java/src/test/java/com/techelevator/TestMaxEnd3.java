package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestMaxEnd3 {

	MaxEnd3 exercise = new MaxEnd3();
	@Test
	public void maxEnd3Test() {
		int[] tester1 = {3,3,3};
		assertArrayEquals("Messes with arrays with equals elements.",tester1,exercise.makeArray(tester1));
		int[] tester2 = {1,3,2};
		int[] tester3 = {2,2,2};
		assertArrayEquals("Setting array elements to middle elements.",tester3,exercise.makeArray(tester2));
		tester2[2] = 3;
		assertArrayEquals("Not setting array to last element.",tester1,exercise.makeArray(tester2));
		tester2[2] = 2;
		tester2[0] = 3;
		assertArrayEquals("Not setting array to first element.",tester1,exercise.makeArray(tester2));
	}

}
