package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFrontTimes {

	FrontTimes exercise = new FrontTimes();
	@Test
	public void frontTimesTest() {
		String tester = "Chocolate";
		boolean passedTest = exercise.generateString(tester, 2).equals("ChoCho");
		assertTrue("String isn't properly copying.",passedTest);
		passedTest = exercise.generateString(tester, 3).equals("ChoChoCho");
		assertTrue("String copies improper number of time.",passedTest);
		tester = "Ch";
		passedTest = exercise.generateString(tester,4).equals("ChChChCh");
		assertTrue("Cannot copy of string less than three characters long.",passedTest);
	}

}
