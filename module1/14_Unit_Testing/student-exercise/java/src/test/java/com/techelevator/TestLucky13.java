package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestLucky13 {

	Lucky13 exercise = new Lucky13();
	@Test
	public void lucky13Test() {
		int[] testArray = {0,2,4,5,6};
		assertTrue("Returned false for no 1s or 3s.",exercise.getLucky(testArray));
		testArray[0] = 1;
		assertFalse("Cannot properly detect 1s.",exercise.getLucky(testArray));
		testArray[0] = 0;
		testArray[3] = 1;
		assertFalse("Cannot properly detect 1s.",exercise.getLucky(testArray));
		testArray[3] = 3;
		assertFalse("Cannot properly detect 3s.",exercise.getLucky(testArray));
	}

}
