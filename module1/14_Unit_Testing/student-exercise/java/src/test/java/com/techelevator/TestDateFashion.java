package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestDateFashion {

	DateFashion exercise = new DateFashion();
	@Test
	public void dateFashionTest() {
		assertEquals("Low style does not prevent table getting.",0,exercise.getATable(2, 10));
		assertEquals("Low style does not prevent table getting.",0,exercise.getATable(10, 2));
		assertEquals("High style does not guarantee a table",2,exercise.getATable(3,8));
		assertEquals("High style does not guarantee a table",2,exercise.getATable(8,3));
		assertEquals("High style does not guarantee a table",2,exercise.getATable(8,8));
		assertEquals("Middle styles do not return maybe.",1,exercise.getATable(3,7));
		assertEquals("Middle styles do not return maybe.",1,exercise.getATable(7,7));
		assertEquals("Middle styles do not return maybe.",1,exercise.getATable(3,3));
		assertEquals("Middle styles do not return maybe.",1,exercise.getATable(7,3));
	}

}
