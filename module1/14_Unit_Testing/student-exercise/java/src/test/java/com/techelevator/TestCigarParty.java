package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCigarParty {

	CigarParty exercises = new CigarParty();
	@Test
	public void test() {
		assertFalse("Party is a success at 39 cigars.",exercises.haveParty(39,false));
		assertFalse("Weekend influences lower bound.",exercises.haveParty(39, true));
		assertTrue("40 is not a success.",exercises.haveParty(40, true));
		assertTrue("Weekend influences lower bound.",exercises.haveParty(40, false));
		assertTrue("60 is not a success.",exercises.haveParty(60, true));
		assertTrue("Weekend necessary for success at 60",exercises.haveParty(60, false));
		assertTrue("Weekend not successful at 61.",exercises.haveParty(61, true));
		assertFalse("Weekend not successful at 61.",exercises.haveParty(61,false));
	}

}
