package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestNonStart {

	NonStart exercise = new NonStart();
	@Test
	public void nonStartTest() {
		String test1 = "Hello";
		String test2 = "There";
		assertEquals("Not properly removing the first letter.","ellohere",exercise.getPartialString(test1,test2));
		test1 = "H";
		assertEquals("Can't work with strings of length 1.","here",exercise.getPartialString(test1, test2));
		test2 = "T";
		assertEquals("Can't work with strings of length 1.","",exercise.getPartialString(test1, test2));
		test1 = "LLeeroy";
		assertEquals("Can't work with strings of length 1.","Leeroy",exercise.getPartialString(test1, test2));
	}

}
