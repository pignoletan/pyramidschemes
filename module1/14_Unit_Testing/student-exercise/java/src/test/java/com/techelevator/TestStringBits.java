package com.techelevator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestStringBits {

	StringBits exercise = new StringBits();
	@Test
	public void test() {
		String test = "";
		assertEquals("Cannot account for empty string.","",exercise.getBits(test));
		test = "A";
		assertEquals("Cannot account for string length=1.","A",exercise.getBits(test));
		test = "Sphinx of black quartz, judge my vow";
		assertEquals("Does not properly remove every other character.","Shn fbakqat,jdem o",exercise.getBits(test));
	}

}
