package com.techelevator;

public class UsePlayingCards {

	public static void main(String[] args) {
		// We used the base class to declare the object and the class we wanted to instantiate and initialize it
		PlayingCard aUSACard  = new AmericanPlayingCard(1, "HEARTS");
		
		PlayingCard aCard  = new PlayingCard(1, "Zoot", "BLACK"); //Oh noes we got an ERROR!!!!
		System.out.println("aCard is : \n" + aCard);
		
		
		System.out.println("aUSACard is : \n" + aUSACard);  // the toString() for class will run
		
		
		AmericanPlayingCard aUSACard2 = new AmericanPlayingCard(14, "SPADES");  // invalid value
		System.out.println("aUSACard2 is : \n" + aUSACard2);
		
		AmericanPlayingCard aUSACard3 = new AmericanPlayingCard(-1, "SPADES");  // invalid value
		System.out.println("aUSACard3 is : \n" + aUSACard3);
		
		PlayingCard anItalianCard1 = new ItalianPlayingCard(13, "SWORDS");
		System.out.println("anItalianCard1 is : \n" + anItalianCard1);
		
		ItalianPlayingCard anItalianCard2 = new ItalianPlayingCard(9, "COINS");
		System.out.println("anItalianCard2 is : \n" + anItalianCard2);
		
		SwissPlayingCard aSwissCard1 = new SwissPlayingCard(13, "ROSES");
		System.out.println("aSwissCard1 is : \n" + aSwissCard1);
		
		PlayingCard aSwissCard2 = new SwissPlayingCard(9, "SHIELDS");
		System.out.println("aSwissCard2 is : \n" + aSwissCard2);
		
		SwissPlayingCard aSwissCard3 = new SwissPlayingCard(10, "ACORNS");
		System.out.println("anSwissCard3 is : \n" + aSwissCard3);
		
		
		System.out.println("--------- Polymorphic Example -----------");
		
		PlayingCard[] cardArray = new PlayingCard[14];
		// Base class array containing derived class objects
		cardArray[0] = new AmericanPlayingCard(1, "HEARTS");
		cardArray[1] = new ItalianPlayingCard(13, "SWORDS");
		cardArray[2] = new SwissPlayingCard(10, "ACORNS");
		cardArray[3] = new AmericanPlayingCard(13, "SPADES");

		for (int i = 0; i < cardArray.length; i++) {
			System.out.println(cardArray[i]);
		}
		
	}

}
