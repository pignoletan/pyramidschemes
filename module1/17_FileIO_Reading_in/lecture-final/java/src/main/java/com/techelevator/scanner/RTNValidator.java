package com.techelevator.scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RTNValidator {
	
	private static final int[] CHECKSUM_WEIGHTS = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1 };

	public static void main(String[] args) throws FileNotFoundException {

		printApplicationBanner();
		
        File inputFile = getInputFileFromUser();         // Get a File object containing an existing file 
//      -or-		
//		File inputFile = new File("rtn.txt");            // Define a file Object for the rtn.txt filename
		
		try(Scanner fileScanner = new Scanner(inputFile)) {  // Define a Scanner object for file
			while(fileScanner.hasNextLine()) {           // Loop as long as the file has a nextLine
				String line = fileScanner.nextLine();    //     Get a line from the Scanner/File
				String rtn = line.substring(0, 9);       //     Get the first 9-chars from String
				System.out.print("RTN : " + rtn);        //     Display the 9-chars
				if(checksumIsValid(rtn) == false) {      //     Pass the 9-chars to a method
					System.out.println(" - Invalid");    //     display "Invalid" if it is
				}                                        //
					else {                               //        -or
						System.out.println(" - Valid");  //     Display "Valid" if it is NOT invalid
				}
			}
		}
	}

	private static void printApplicationBanner() {
		System.out.println("******************");
		System.out.println("RTN Validator 9000");
		System.out.println("******************");
		System.out.println();
	}

	@SuppressWarnings("resource")
	private static File getInputFileFromUser() {
		Scanner userInput = new Scanner(System.in);                // Define a Scanner object for keyboard
		System.out.print("Please enter path to input file >>> ");  // Prompt the user for a file name/path
		String path = userInput.nextLine();                        // Read a line from keyboard
		System.out.println("Path entered: "+ path + "\n");         // Display what the user entered
		File inputFile = new File(path);                           // Define a File object with the user input
		if(inputFile.exists() == false) {                          // Checks for the existence of a file
			System.out.println(path+" does not exist");            // If the file doesn't exist - issue message
			System.exit(1);                                        //                             end  the program
		} else if(inputFile.isFile() == false) {                   // Is what the user entered a file?
			System.out.println(path+" is not a file");             //                  if not - issue a message
			System.exit(1);                                        //                           end the program
		}
		return inputFile;                                          // return the File object created in this method
	}

	private static boolean checksumIsValid(String routingNumber) {
		int checksum = 0;
		for(int i = 0; i < 9; i++) {
			int digit = Integer.parseInt(routingNumber.substring(i, i+1));
			checksum += digit * CHECKSUM_WEIGHTS[i];
		}
		return checksum % 10 == 0;
	}
}
