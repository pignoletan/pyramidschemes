package com.techelevator.scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class RTNValidator {
	
	private static final int[] CHECKSUM_WEIGHTS = new int[] { 3, 7, 1, 3, 7, 1, 3, 7, 1 };

	public static void main(String[] args) throws FileNotFoundException {

		printApplicationBanner();
		
		File inputFile = getInputFileFromUser();              //Get a file if everything is okay
		try(Scanner fileScanner = new Scanner(inputFile)) {  //Define a scanner object for da file just like you did with the fucking keyboard
			while(fileScanner.hasNextLine()) {              //Loop as long as the file has a next line
				String line = fileScanner.nextLine();      //Give us a line from da file/scanner
				String rtn = line.substring(0, 9);        //Converts da string if you need to, only gets the first 9 chars
				System.out.print("RTN : " + rtn);        //Displays da 9 chars
				if(checksumIsValid(rtn) == false) {     //We're using a methods!!!!!!!!!! passes the 9 chars
					System.out.println(" - Invalid");  //Display invalid if it is
				}
					else {
						System.out.println(" - Valid");  //Display valid if it is
				}
			}
		}
	}

	private static void printApplicationBanner() {
		System.out.println("******************");
		System.out.println("RTN Validator 9000");
		System.out.println("******************");
		System.out.println();
	}

	@SuppressWarnings("resource")
	private static File getInputFileFromUser() {
		Scanner userInput = new Scanner(System.in);						//Define a scanner object for the keybeard
		System.out.print("Please enter path to input file >>> ");	   //Prompts the user to give us a file... OR ELSE!!!!
		String path = userInput.nextLine();							  //User input is the keybeard
		System.out.println("Path entered: "+ path + "\n");           //Confirms the file the user inputted
		File inputFile = new File(path);                            //Define a new file object based on the input
		if(inputFile.exists() == false) {                          //Checks for the existence of a file
			System.out.println(path+" does not exist");           //Oh noes you don't have a file!!
			System.exit(1); // Ends the program                  //Abort abort!!
		} else if(inputFile.isFile() == false) {			    //But what if the file does exist and isn't a file
			System.out.println(path+" is not a file");         //Baby what is you doing???
			System.exit(1); // Ends the program               //Abort abort!!
		}
		return inputFile;                                   //Well you did everything right here's a file
	}

	private static boolean checksumIsValid(String routingNumber) {
		int checksum = 0;
		for(int i = 0; i < 9; i++) {
			int digit = Integer.parseInt(routingNumber.substring(i, i+1));
			checksum += digit * CHECKSUM_WEIGHTS[i];
		}
		return checksum % 10 == 0;
	}
}
