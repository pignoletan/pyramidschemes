package com.techelevator;

import java.util.ArrayList;

public class QuizQuestion {
	private ArrayList<String> answers = new ArrayList<String>();
	private String question = "";
	private int correctAnswer = 1;
	
	public String getAnswer(int index) {
		return answers.get(index);
	}

	public String getQuestion() {
		return question;
	}
	
	public int getNumberOfAnswers() {
		return answers.size();
	}
	
	public boolean isCorrectAnswer(int answer) {
		return answer == correctAnswer;
	}

	public QuizQuestion(String question) {
		String[] possibleAnswers = question.split("\\|");
		this.question = possibleAnswers[0];
		for (int i = 1; i < possibleAnswers.length; i++) {
			String lastChar = possibleAnswers[i].substring(possibleAnswers[i].length()-1);
			if (lastChar.contentEquals("*")) {
				correctAnswer = i;
				String toAdd = possibleAnswers[i].substring(0,possibleAnswers[i].length()-1);
				answers.add(toAdd);
			} else {
				answers.add(possibleAnswers[i]);
			}
		}
	}
	public void printQuiz() {
		System.out.println(question);
		for (int i = 0; i < answers.size(); i++) {
			System.out.println((i+1) + ") " + answers.get(i));
		}
	}
}
