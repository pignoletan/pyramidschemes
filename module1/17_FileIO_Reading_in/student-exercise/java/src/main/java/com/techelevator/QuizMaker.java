package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class QuizMaker {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner userInput = new Scanner(System.in);
		File inputFile = getInputFileFromUser();
		int correctAnswers = 0;
		int totalAnswers = 0;
		try(Scanner fileScanner = new Scanner(inputFile)) { 
			while(fileScanner.hasNextLine()) {
				totalAnswers++;
				String line = fileScanner.nextLine();
				QuizQuestion quiz = new QuizQuestion(line);
				quiz.printQuiz();
				boolean proceed = false;
				String userAnswer = "";
				int numOfAnswer = 0;
				while (proceed == false) {
					System.out.print("What is your answer? ");
					userAnswer = userInput.nextLine();
					try {
						numOfAnswer = Integer.parseInt(userAnswer);
						if (numOfAnswer > 0&&numOfAnswer <= quiz.getNumberOfAnswers()) {
							proceed = true;
						} else {
							System.out.println("Error: Your answer number must be between 1 and " + quiz.getNumberOfAnswers());
						}
					}
					catch (NumberFormatException exception) {
						System.out.println("Error: Please type a whole number");
					}
				}
				if (quiz.isCorrectAnswer(numOfAnswer)) {
					System.out.println("Good job!! Here's a cookie!");
					correctAnswers++;
				} else {
					System.out.println("How awful!! Your answer is wrong, so no brownie for you!!");
				}
				System.out.println();
			}
		}
		System.out.println("You got " + correctAnswers + " questions correct out of " + totalAnswers + " questions.");
	}
	
	private static File getInputFileFromUser() {
		System.out.print("Please input the file path for the quiz: ");
		Scanner userInput = new Scanner(System.in);
		String filePath = userInput.nextLine();
		File inputFile = new File(filePath);
		if(inputFile.exists() == false) {                          
			System.out.println(filePath+" does not exist");           
			System.exit(1); // Ends the program                  
		} else if(inputFile.isFile() == false) {			    
			System.out.println(filePath+" is not a file");         
			System.exit(1); // Ends the program               
		}
		return inputFile;
	}

}
