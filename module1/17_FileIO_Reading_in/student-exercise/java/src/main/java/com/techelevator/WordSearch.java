package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordSearch {

	public static void main(String[] args) throws FileNotFoundException {
		File fileToSearch = getInputFileFromUser();
		Scanner userInput = new Scanner(System.in);	
		System.out.print("Please enter the word to search for: ");
		String wordToSearchFor = userInput.nextLine();
		boolean proceed = false;
		String caseSensitive = "";
		while (proceed == false) {
			System.out.print("Should the search be case-sensitive? (Y/N): ");
			caseSensitive = userInput.nextLine();
			caseSensitive = caseSensitive.toLowerCase();
			if (caseSensitive.contentEquals("y")
				||caseSensitive.equals("n")) {
				proceed = true;
				if (caseSensitive.equals("n")) {
					wordToSearchFor = wordToSearchFor.toLowerCase();
				}
			} else {
				System.out.println("Error: Invalid input");
			}
		}
		int lineCount = 0;
		try(Scanner fileScanner = new Scanner(fileToSearch)) {
			while (fileScanner.hasNextLine()) {
				lineCount++;
				String line = fileScanner.nextLine();
				String rightCase = line; //If case insensitive save line before converting to lower case
				if (caseSensitive.equals("n")) {
					rightCase = line.toLowerCase();
				}
				if (rightCase.contains(wordToSearchFor)) {
					System.out.println(lineCount + ") " + line);
				}
			}
		}
	}
	
	private static File getInputFileFromUser() {
		System.out.print("Please input the file path for the document to search: ");
		Scanner userInput = new Scanner(System.in);
		String filePath = userInput.nextLine();
		File inputFile = new File(filePath);
		if(inputFile.exists() == false) {                          
			System.out.println(filePath+" does not exist");           
			System.exit(1); // Ends the program                  
		} else if(inputFile.isFile() == false) {			    
			System.out.println(filePath+" is not a file");         
			System.exit(1); // Ends the program               
		}
		return inputFile;
	}

}
