package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileSplitter {

	public static void main(String[] args) throws IOException {
		boolean proceed = false;
		Scanner userInput = new Scanner(System.in);
		File userFile = null;
		do {
			System.out.print("Enter the path of the file: ");
			String filePath = userInput.nextLine();
			userFile = new File(filePath);
			if (!userFile.exists()) {
				System.out.println("Error: File does not exist.");
			} else if (!userFile.isFile()) {
				System.out.println("Error: Specified path does not lead to a file.");
			} else {
				proceed = true;
			}
		} while (!proceed);
		proceed = false;
		int numOfLines = 1;
		while (!proceed) {
			try {
				System.out.print("What is the maximum number of lines that should be in a file? ");
				String convertToInt = userInput.nextLine();
				numOfLines = Integer.parseInt(convertToInt);
				if (numOfLines < 1) {
					System.out.println("Error: Please input a positive number.");
				} else {
					proceed = true;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error: Please insert an integer.");
			}
		}
		
		Scanner reader = new Scanner(userFile);
		int iterations = 0;
		while (reader.hasNextLine()) {
			iterations++;
			String newFileName = "splitFile-" + iterations + ".txt";
			File newFile = new File(newFileName);
			newFile.createNewFile();
			PrintWriter writer = new PrintWriter(newFile);
			for (int i = 0; i < numOfLines&&reader.hasNextLine(); i++) {
				String currentLine = reader.nextLine();
				writer.println(currentLine);
			}
			writer.flush();
			writer.close();
		}
	}

}
