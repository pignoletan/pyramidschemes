package com.techelevator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FizzWriter {

	public static void main(String[] args) throws IOException {
		File newFile = new File("FizzBuzz.txt");
		newFile.createNewFile();
		PrintWriter writer = new PrintWriter(newFile);
		for (int i = 1; i <= 300; i++) {
			String whatToPrint = fizzBuzz(i);
			writer.println(whatToPrint);
		}
		writer.flush();
		writer.close();
	}

	public static String fizzBuzz(int num) {
		String intString = Integer.toString(num);
		boolean containsThree = intString.contains("3");
		boolean containsFive = intString.contains("5");
		boolean divisibleByThree = num % 3 == 0;
		boolean divisibleByFive = num % 5 == 0;
		if ((divisibleByThree||containsThree)&&(divisibleByFive||containsFive)) {
			return "FizzBuzz";
		}
		if (divisibleByThree||containsThree) {
			return "Fizz";
		}
		if (divisibleByFive||containsFive) {
			return "Buzz";
		}
		return Integer.toString(num);
	}
	
}
