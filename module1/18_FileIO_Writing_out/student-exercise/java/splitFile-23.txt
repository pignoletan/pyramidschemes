waited.

When the procession came opposite to Alice, they all stopped and looked
at her, and the Queen said severely, "Who is this?" She said it to the
Knave of Hearts, who only bowed and smiled in reply.

"Idiot!" said the Queen, tossing her head impatiently; and turning to
Alice, she went on, "What's your name, child?"

"My name is Alice, so please your Majesty," said Alice very politely;
but she added, to herself, "Why, they're only a pack of cards, after
all. I needn't be afraid of them!"

"And who are _these_?" said the Queen, pointing to the three gardeners
who were lying round the rose-tree; for, you see, as they were lying on
their faces, and the pattern on their backs was the same as the rest of
the pack, she could not tell whether they were gardeners, or soldiers,
or courtiers, or three of her own children.

"How should _I_ know?" said Alice, surprised at her own courage. "It's
no business of _mine_."

The Queen turned crimson with fury, and, after glaring at her for a
moment like a wild beast, screamed "Off with her head! Off----"

"Nonsense!" said Alice, very loudly and decidedly, and the Queen was
silent.

The King laid his hand upon her arm, and timidly said "Consider my dear:
she is only a child!"

The Queen turned angrily away from him, and said to the Knave "Turn them
over!"

The Knave did so, very carefully, with one foot.

"Get up!" said the Queen, in a shrill, loud voice, and the three
gardeners instantly jumped up, and began bowing to the King, the Queen,
the royal children, and everybody else.

"Leave off that!" screamed the Queen. "You make me giddy." And then,
turning to the rose-tree, she went on, "What _have_ you been doing
here?"

"May it please your Majesty," said Two, in a very humble tone, going
down on one knee as he spoke, "we were trying----"

[Illustration: _The Queen turned angrily away from him and said to the
Knave, "Turn them over"_]

"_I_ see!" said the Queen, who had meanwhile been examining the roses.
"Off with their heads!" and the procession moved on, three of the
soldiers remaining behind to execute the unfortunate gardeners, who ran
to Alice for protection.

"You shan't be beheaded!" said Alice, and she put them into a large
flower-pot that stood near. The three soldiers wandered about for a
minute or two, looking for them, and then quietly marched off after the
others.

"Are their heads off?" shouted the Queen.

"Their heads are gone, if it please your Majesty!" the soldiers shouted
in reply.

"That's right!" shouted the Queen. "Can you play croquet?"

The soldiers were silent, and looked at Alice, as the question was
evidently meant for her.

"Yes!" shouted Alice.

"Come on, then!" roared the Queen, and Alice joined the procession,
wondering very much what would happen next.

"It's--it's a very fine day!" said a timid voice at her side. She was
walking by the White Rabbit, who was peeping anxiously into her face.

"Very," said Alice: "----where's the Duchess?"

"Hush! Hush!" said the Rabbit in a low hurried tone. He looked anxiously
over his shoulder as he spoke, and then raised himself upon tiptoe, put
his mouth close to her ear, and whispered "She's under sentence of
execution."

"What for?" said Alice.

"Did you say 'What a pity!'?" the Rabbit asked.

"No, I didn't," said Alice: "I don't think it's at all a pity. I said
'What for?'"

"She boxed the Queen's ears--" the Rabbit began. Alice gave a little
scream of laughter. "Oh, hush!" the Rabbit whispered in a frightened
tone. "The Queen will hear you! You see she came rather late, and the
Queen said----"

"Get to your places!" shouted the Queen in a voice of thunder, and
people began running about in all directions, tumbling up against each
other; however, they got settled down in a minute or two, and the game
