package com.techelevator.farm;

public class Chicken extends FarmAnimal {
	
	public Chicken() {
		super("Chicken", "cluck!", false);
	}
	public void  MakeSound() {
		System.out.println("a Chicken goes " + getSound()); // get method is required because the super class has the data for sound and it's private	
	}
	@Override
	public void DefendYourself() {
		System.out.println("The other day, I was called a little turkey.\nBut I'm a chicken. You got it?\nYou beef jerky.");
	}
	
	public void layEgg() {
		System.out.println("Chicken laid an egg!");
	}

}