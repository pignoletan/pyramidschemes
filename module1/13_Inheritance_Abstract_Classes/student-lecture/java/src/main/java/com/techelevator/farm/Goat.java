package com.techelevator.farm;

public class Goat extends FarmAnimal {
	
	public Goat() {
		super("Goat","quel son fait une chevre",false);
	}
	
	public void setSound(String newSound) {
		sound = newSound;
	}

	public void  MakeSound() {
		System.out.println("a Goat goes " + getSound()); // get method is required because the super class has the data for sound and it's private	
	}
	
	public void DefendYourself() {
		System.out.println("I'm gonna double-hoøf kick you!!");
	}
	
	@Override
	public boolean isSleeping() {
		return true;
	}
}
