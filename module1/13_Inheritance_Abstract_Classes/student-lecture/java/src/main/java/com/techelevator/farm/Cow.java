package com.techelevator.farm;

public class Cow extends FarmAnimal  {

	public Cow() {
		super("Cow", "moo!", true);
	}
	public void  MakeSound() {
		System.out.println("a Cow goes " + getSound()); // get method is required because the super class has the data for sound and it's private
	}
}