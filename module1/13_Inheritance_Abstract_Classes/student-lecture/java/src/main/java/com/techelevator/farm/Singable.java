package com.techelevator.farm;

public interface Singable {  // To be included the Old MacDonald song

// If one wants to be Singable one MUST implement/code these methods
//
// List the method signatures for all methods for the interface

	public void MakeSound();
	public void DefendYourself();
	public boolean isSleeping();
}
