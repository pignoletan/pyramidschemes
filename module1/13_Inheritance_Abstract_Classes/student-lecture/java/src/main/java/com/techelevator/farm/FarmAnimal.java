package com.techelevator.farm;

public abstract class FarmAnimal implements Singable {  // A FarmAnimal promises to include all methods required by Singable interface
	private String name;
	protected String sound;
	private boolean sleeping;

	public FarmAnimal(String name, String sound,boolean sleeping) {
		this.name = name;
		this.sound = sound;
		this.sleeping = sleeping;
	}
	@Override
	public abstract void MakeSound();
	
	@Override
	public void DefendYourself() {
		System.out.println("I'm gonna bite you!!");
	}
	
	public String getName( ) {
		return name;
	}
	public String getSound( ) {
		if (!isSleeping()) {
			return sound;
		}
		return "Zzzzz...";
	}
	@Override
	public boolean isSleeping() {
		return sleeping;
	}
	
}