package com.techelevator.person;

public class Person {

	private String firstName = "";
	private String lastName = "";
	private int age = 0;
	
	//getters go here
	
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public int getAge() {
		return age;
	}
	
	//setters go here
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
	
	public String getFullName() {
		return lastName + ", " + firstName;
	}
	
	public boolean isAdult() {
		if (age >= 18) {
			return true;
		}
		return false;
	}
	
}
