package com.techelevator.company;

public class Company {

	private String name = "";
	private int numberOfEmployees = 0;
	private double revenue = 0.0;
	private double expenses = 0.0;

	//Getters go here
	
	public String getName() {
		return name;
	}

	public int getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public double getRevenue() {
		return revenue;
	}

	public double getExpenses() {
		return expenses;
	}
	
	//Setters go here

	public void setName(String name) {
		this.name = name;
	}

	public void setNumberOfEmployees(int numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	public void setExpenses(double expenses) {
		this.expenses = expenses;
	}
	
	public String getCompanySize() {
		if (numberOfEmployees <= 50) {
			return "small";
		}
		if (numberOfEmployees <= 250) {
			return "medium";
		}
		return "large";
	}
	
	public double getProfit() {
		return revenue - expenses;
	}
	
}
