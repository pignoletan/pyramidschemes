package com.techelevator;

public class PlayingCard {
//******************************************************************************
// Class member data
//******************************************************************************
	private String cardSuit     = "";  // private means only members of the class
	private int    cardValue    = 0;   //     can access it
	private String cardColor    = "";  // only member functions (methods) can
	private String cardShape    = "";  //     can see and change the data
	private String cardMaterial = "";
	private String backColor    = "";
	
//******************************************************************************	
// Class methods - behavior of a class object
//******************************************************************************
	
// Create constructors for the class
// a constructor: (1) same name as the class; (2) returns nothing (not even void)

// Default ctor - initialize object to default values - Create a default card
	public PlayingCard() {   
		cardSuit     = "Joker";
		cardValue    = 0;
		cardColor    = "Black";
		cardShape    = "Rectangular";
		cardMaterial = "Paper";
		backColor    = "Blue";
	}
// 3-arg ctor initialize to args passed and default everything else	
	public PlayingCard(int aValue, String cardSuit, String aColor) {
		this.cardSuit     = cardSuit;    // use the parameter passed as aSuit
		cardValue    = aValue;   // use the parameter passed as aValue
		cardColor    = aColor;   // use the parameter passed as aColor
		cardShape    = "Rectangular";  // use the default value
		cardMaterial = "Paper";        // use the default value
		backColor    = "Blue";	       // use the default value
	}
	

	
	
public String getCardSuit() {
		return cardSuit;
	}
	public int getCardValue() {
		return cardValue;
	}
	public String getCardColor() {
		return cardColor;
	}
	public String getCardShape() {
		return cardShape;
	}
	public String getCardMaterial() {
		return cardMaterial;
	}
	public String getBackColor() {
		return backColor;
	}
	/**
	 * @param cardSuit the cardSuit to set
	 */
	public void setCardSuit(String aSuit) {
		cardSuit = aSuit;  // this. is not required because the parameter name is different than the data member name
	}
	/**
	 * @param cardValue the cardValue to set
	 */
	public void setCardValue(int cardValue) {
		this.cardValue = cardValue;  // this. - reference the data in the object used to invoke the method
	}                                //       - required if the paramter name matches the data member name
	/**
	 * @param cardColor the cardColor to set
	 */
	public void setCardColor(String cardColor) {
		this.cardColor = cardColor;
	}
	// Display the attributes of a PlayingCard
//  access    return    method
//  modified  type      name      (parameters)
	public    void    displayCard() {
		System.out.println("     Suit: " + cardSuit);
		System.out.println("    Value: " + cardValue);
		System.out.println("    Color: " + cardColor);
		System.out.println("    Shape: " + cardShape);
		System.out.println("   Medium: " + cardMaterial);
		System.out.println("BackColor: " + backColor);
		System.out.println("---------------------------------------------------");
	}
	
}
