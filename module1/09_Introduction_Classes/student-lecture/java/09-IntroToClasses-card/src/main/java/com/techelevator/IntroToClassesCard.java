package com.techelevator;

public class IntroToClassesCard {
	
	public static void main(String[] args) {
        
        /*
         *  This program will demonstrate several concepts presented in the Java cohort
		 *  in the topic Introduction to Classes 		
        */
		PlayingCard aCard = new PlayingCard();  //instantiate a playing card object
		PlayingCard anotherCard = new PlayingCard("Spades",1,"Black");
		
		//Now we have the object perform a behavior via object.method();
		aCard.displayCard();
		System.out.println();
		anotherCard.displayCard();
		System.out.println("\nValue in anotherCard is: " + anotherCard.getCardValue() + " and is a " + anotherCard.getCardSuit());
		System.out.println("Value in aCard is: " + aCard.getCardValue() + " and is a " + aCard.getCardSuit());
		anotherCard.setCardValue(13);
		System.out.println();
		anotherCard.displayCard();
	}
}
		 

