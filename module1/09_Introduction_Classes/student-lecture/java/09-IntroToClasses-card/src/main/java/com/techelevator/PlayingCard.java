package com.techelevator;

public class PlayingCard {
	//Classes have member data, very exclusive, very posh
	private String cardSuit = ""; //when it's just a card I don't know the suit
	private int cardValue = 0;
	private String cardColor = "";
	private String cardShape = "";
	private String cardMaterial = "";
	private String backColor = "";
	
	//******************************************************************
	//Class methods be yonder
	//******************************************************************
	
	//Here is our default ctor
	public PlayingCard() {
		cardSuit = "Joker"; //Frank says its a suit
		cardValue = 0; //Joker, this line is unnecessary but makes it more readable
		cardColor = "Black";
		cardShape = "Rectangular";
		cardMaterial = "Paper";
		backColor = "Blue";
	}
	
	//Here is our not-default three argument ctor
	public PlayingCard(String aSuit,int aValue,String aColor) {
		cardValue = aValue;
		cardSuit = aSuit;
		cardColor = aColor;
		cardShape = "Rectangular";
		cardMaterial = "Paper";
		backColor = "Blue";
	}
	
	//Display the attributes of a playing card
	public void displayCard() {
		System.out.println("Suit: " + cardSuit);
		System.out.println("Value: " + cardValue);
		System.out.println("Color: " + cardColor);
		System.out.println("Shape: " + cardShape);
		System.out.println("Medium: " + cardMaterial);
		System.out.println("Back Color: " + backColor);
	}
	
	//Getter methods go here
	
	public int getCardValue() {
		return cardValue;
	}
	
	public String getCardSuit() {
		return cardSuit;
	}
	
	public String getCardColor() {
		return cardColor;
	}

	public String getCardShape() {
		return cardShape;
	}

	public String getCardMaterial() {
		return cardMaterial;
	}

	public String getBackColor() {
		return backColor;
	}

	//Setter methods go here
	
	public void setCardValue(int newValue) {
		this.cardValue = newValue;
	}

	public void setCardSuit(String cardSuit) {
		this.cardSuit = cardSuit;
	}

	public void setCardColor(String cardColor) {
		this.cardColor = cardColor;
	}

}
