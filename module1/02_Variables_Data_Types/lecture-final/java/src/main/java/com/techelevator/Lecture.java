package com.techelevator;

public class Lecture {

	public static void main(String[] args) {
		
		/*
		1. Create a variable to hold an int and call it numberOfExercises.
			Then set it to 26.
		*/
        int numberOfExercises = 26;
		System.out.println("#1 :" + numberOfExercises);  // concatenate values to display using +

		/*
		2. Create a variable to hold a double and call it half.
			Set it to 0.5.
		*/
		double half = 0.5;
		System.out.println("half=" + half);

		/*
		3. Create a variable to hold a String and call it name.
			Set it to "TechElevator".
		*/
        String name = "TechElevator";  // String values are enclosed in double quotes
		System.out.println(name);

		/*
		4. Create a variable called seasonsOfFirefly and set it to 1.
		*/
        int seasonsOfFirefly = 1;
		System.out.println(seasonsOfFirefly);

		/*
		5. Create a variable called myFavoriteLanguage and set it to "Java".
		*/
        String myFavoriteLanguage = "Java";
		System.out.println(name + " Ping Pong Champs are " + myFavoriteLanguage);

		/*
		6. Create a variable called pi and set it to 3.1416.
		*/
        double pi  = 3.1416;
        float  pie = 3.1416F;  
        float  py  = (float) 3.1416;
		System.out.println(pi);

		/*
		7. Create and set a variable that holds your name.
		*/

		/*
		8. Create and set a variable that holds the number of buttons on your mouse.
		*/

		/*
		9. Create and set a variable that holds the percentage of battery left on
		your phone.
		*/

		/*
		10. Create an int variable that holds the difference between 121 and 27.
		*/
        int diff = 121-27;
        int dif  = 27 - 121;
		/*
		11. Create a double that holds the addition of 12.3 and 32.1.
		*/
        double sum = 12.3 + 32.1;
		/*
		12. Create a String that holds your full name.
		*/
        String myName = "Your Name";
		/*
		13. Create a String that holds the word "Hello, " concatenated onto your
		name from above.
		*/
        String greeting = "Hello " + myName;  // Define a variable and assign a value
		/*
		14. Add a " Esquire" onto the end of your full name and save it back to
		the same variable.
		*/
        greeting = greeting + " Esquire";  // replace value in an existing variable
        greeting += " Esquire";            // replace value in an existing variable
		/*
		15. Now do the same as exercise 14, but use the += operator.
		*/
        greeting += " Esquire";            // replace value in an existing variable
        System.out.println(greeting);
        /*
		16. Create a variable to hold "Saw" and add a 2 onto the end of it.
		*/
        String movie = "Saw";
        System.out.println(movie + 2);
		/*
		17. Add a 4 onto the end of the variable from exercise 16.
		*/
        System.out.println(movie + 2 + 4);  // + means concatenate if any operand is a String
		/*
		18. What is 4.4 divided by 2.2?
		*/
        System.out.println("4.4 / 2.2 = " + 4.4 / 2.2);
		/*
		19. What is 5.4 divided by 2?
		*/
        System.out.println("5.4 / 2 = " + 5.4 / 2);
		/*
		20. What is 5 divided by 2?
		*/
        System.out.println("5 / 2 = " + 5 / 2);
		/*
		21. What is 5.0 divided by 2?
		*/
        System.out.println("5.0 / 2 = " + 5.0 / 2);
		/*
		22. What is 66.6 divided by 100? Is the answer you get right or wrong?
		*/
        System.out.println("66.6 / 100 is " + 66.6 / 100);
		/*
		23. If I divide 5 by 2, what's my remainder?
		*/
        System.out.println("Quotient  of 5 / 2 = " + 5 / 2);
        System.out.println("Remainder of 5 / 2 = " + 5 % 2);  // % - modulo/modulus give remainder
		/*
		24. What is 1,000,000,000 * 3?
		*/
        System.out.println("Remainder of 1,000,000,000 * 3 = " + 1000000000 * 3L); 
		/*
		25. Create a variable that holds a boolean called doneWithExercises and
		set it to false.
		*/
        boolean doneWithExercises = false; 
        System.out.println("doneWithExercises = " + doneWithExercises);
		/*
		26. Now set doneWithExercise to true.
		*/doneWithExercises = true; 
        System.out.println("doneWithExercises = " + doneWithExercises);
		/*
		 26. Now set up a variable that can't be changed (a constant).
		 */
        final int num = 12;  // final makes this a constant - cannot be changed
	}

}
