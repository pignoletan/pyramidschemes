package com.techelevator;

public class Exercises {

	public static void main(String[] args) {

        /*
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch?
        */

		// ### EXAMPLE:
		int initialNumberOfBirds = 4;
		int birdsThatFlewAway = 1;
		int remainingNumberOfBirds = initialNumberOfBirds - birdsThatFlewAway;
		System.out.println("#1 " + remainingNumberOfBirds);
        /*
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests?
        */

		// ### EXAMPLE:
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int numberOfExtraBirds = numberOfBirds - numberOfNests;
		System.out.println("#2 " + numberOfExtraBirds);
        /*
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods?
        */
		
		// not an example. Everything after this is not an example
		int initialNumberOfRaccoons = 3;
		int numberOfEatingRaccoons = 2;
		int remainingNumberOfRaccoons = initialNumberOfRaccoons - numberOfEatingRaccoons;
		System.out.println("#3 " + remainingNumberOfRaccoons);
        /*
        4. There are 5 flowers and 3 bees. How many less bees than flowers?
        */
		int numberOfFlowers = 5;
		int numberOfBees = 3;
		int numberOfExtraFlowers = numberOfFlowers - numberOfBees;
		System.out.println("#4 " + numberOfExtraFlowers);
        /*
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now?
        */
		int numberOfLonelyPigeons = 1;
		int numberOfExtraPigeons = 1;
		int totalPigeons = numberOfLonelyPigeons + numberOfExtraPigeons;
		System.out.println("#5 " + totalPigeons);
        /*
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now?
        */
		int numberOfInitialOwls = 3;
		int numberOfNewOwls = 2;
		int totalOwlsOnFence = numberOfInitialOwls + numberOfNewOwls;
		System.out.println("#6 " + totalOwlsOnFence);
        /*
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home?
        */
		int numberOfWorkingBeavers = 2;
		int numberOfSwimmingBeavers = 1;
		int numberOfRemainingBeavers = numberOfWorkingBeavers - numberOfSwimmingBeavers;
		System.out.println("#7 " + numberOfRemainingBeavers);
        /*
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all?
        */
		int initialNumberOfToucans = 2;
		int numberOfNewToucans = 1;
		int totalToucans = initialNumberOfToucans + numberOfNewToucans;
		System.out.println("#8 " + totalToucans);
        /*
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts?
        */
		int numberOfSquirrels = 4;
		int numberOfNuts = 2;
		int numberOfHungrySquirrels = numberOfSquirrels - numberOfNuts;
		System.out.println("#9 " + numberOfHungrySquirrels);
        /*
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find?
        */
		final double valueOfQuarters = 0.25;
		final double valueOfDimes = 0.10;
		final double valueOfNickels = 0.05;
		int numberOfQuarters = 1;
		int numberOfDimes = 1;
		int numberOfNickels = 2;
		double hiltsMoney = numberOfQuarters * valueOfQuarters;
		hiltsMoney += numberOfDimes * valueOfDimes;
		hiltsMoney += numberOfNickels * valueOfNickels;
		System.out.println("#10 " + hiltsMoney);
        /*
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all?
        */
		int briersMuffins = 18;
		int macadamsMuffins = 20;
		int mcflannerysMuffins = 17;
		int totalMuffins = briersMuffins + macadamsMuffins + mcflannerysMuffins;
		System.out.println("#11 " + totalMuffins);
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
		double priceOfYoyo = 0.24;
		double priceOfWhistle = 0.14;
		double totalSpent = priceOfYoyo + priceOfWhistle;
		System.out.println("#12 " + totalSpent);
        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
		int largeMarshmallowsUsed = 8;
		int smallMarshmallowsUsed = 10;
		int totalMarshmallowsUsed = largeMarshmallowsUsed + smallMarshmallowsUsed;
		System.out.println("#13 " + totalMarshmallowsUsed);
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
		int snowAtHiltsHouse = 29;
		int snowAtBrecknockSchool = 17;
		int differenceInInches = snowAtHiltsHouse - snowAtBrecknockSchool;
		System.out.println("#14 " + differenceInInches);
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
		int startingDollars = 10;
		int priceOfTruck = 3;
		int priceOfPencil = 2;
		int endingDollars = startingDollars - priceOfTruck - priceOfPencil;
		System.out.println("#15 " + endingDollars);
        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
		int joshsMarbles = 16;
		int numberOfLostMarbles = 7;
		joshsMarbles -= numberOfLostMarbles;
		System.out.println("#16 " + joshsMarbles);
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
		int currentSeashells = 19;
		int neededSeashells = 25;
		int missingSeashells = neededSeashells - currentSeashells;
		System.out.println("#17 " + missingSeashells);
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
		int totalBalloons = 17;
		int redBalloons = 8;
		int greenBalloons = totalBalloons - redBalloons;
		System.out.println("#18 " + greenBalloons);
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
		int numberOfBooksOnShelf = 38;
		int booksPutOnShelf = 10;
		numberOfBooksOnShelf += booksPutOnShelf;
		System.out.println("#19 " + numberOfBooksOnShelf);
        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
		int legsOnOneBee = 6;
		int beesWithLegs = 8;
		int totalLegs = legsOnOneBee * beesWithLegs;
		System.out.println("#20 " + totalLegs);
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
		double priceOfIceCreamCone = 0.99;
		int numberOfConesPurchased = 2;
		double purchasePrice = priceOfIceCreamCone * numberOfConesPurchased;
		System.out.println("#21 " + purchasePrice);
        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
		int currentNumberOfRocks = 64;
		int targetNumberOfRocks = 125;
		int missingRocks = targetNumberOfRocks - currentNumberOfRocks;
		System.out.println("#22 " + missingRocks);
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
		int hiltsMarbles = 38;
		int marblesLost = 15;
		hiltsMarbles -= marblesLost;
		System.out.println("#23 " + hiltsMarbles);
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
		int totalMiles = 78;
		int milesToGas = 32;
		int remainingMiles = totalMiles - milesToGas;
		System.out.println("#24 " + remainingMiles);
        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
		int timeOnSaturdayMorning = 90;
		int timeOnSaturdayAfternoon = 45;
		int totalTimeShoveling = timeOnSaturdayMorning + timeOnSaturdayAfternoon;
		System.out.println("#25 " + totalTimeShoveling);
        /*
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
		double priceOfHotDogs = 0.50;
		int numberOfHotDogs = 6;
		double moneySpentOnHotDogs = priceOfHotDogs * numberOfHotDogs;
		System.out.println("#26 " + moneySpentOnHotDogs);
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
		double priceOfOnePencil = 0.07;
		double moneyToSpend = 0.50;
		int pencilsToPurchase = (int)(moneyToSpend/priceOfOnePencil);
		System.out.println("#27 " + pencilsToPurchase);
        /*
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
		int totalButterflies = 33;
		int orangeButterflies = 20;
		int redButterflies = totalButterflies - orangeButterflies;
		System.out.println("#28 " + redButterflies);
        /*
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
		double moneyGiven = 1.00;
		double priceOfCandy = 0.54;
		double changeDue = moneyGiven - priceOfCandy;
		System.out.println("#29 " + changeDue);
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
		int numberOfTrees = 13;
		int treesPlanted = 12;
		numberOfTrees += treesPlanted;
		System.out.println("#30 " + numberOfTrees);
        /*
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
		final int HOURS_IN_DAY = 24;
		int daysUntilVisitToGrandma = 2;
		int hoursUntilVisitToGrandma = HOURS_IN_DAY * daysUntilVisitToGrandma;
		System.out.println("#31 " + hoursUntilVisitToGrandma);
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
		int numberOfCousins = 4;
		int gumPerCousin = 5;
		int totalGum = numberOfCousins * gumPerCousin;
		System.out.println("#32 " + totalGum);
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
		double dansMoney = 3.00;
		double candyBarPrice = 1.00;
		dansMoney -= candyBarPrice;
		System.out.println("#33 " + dansMoney);
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
		int boatsInLake = 5;
		int peoplePerBoat = 3;
		int peopleInLake = boatsInLake * peoplePerBoat;
		System.out.println("#34 " + peopleInLake);
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
		int ellensLegos = 380;
		int legosLost = 57;
		ellensLegos -= legosLost;
		System.out.println("#35 " + ellensLegos);
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
		int arthursMuffins = 35;
		int targetMuffins = 83;
		int neededMuffins = targetMuffins - arthursMuffins;
		System.out.println("#36 " + neededMuffins);
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
		int willysCrayons = 1400;
		int lucysCrayons = 290;
		int differenceInCrayons = willysCrayons - lucysCrayons;
		System.out.println("#37 " + differenceInCrayons);
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
		int stickersPerPage = 10;
		int pagesOfStickers = 22;
		int totalStickers = stickersPerPage * pagesOfStickers;
		System.out.println("#38 " + totalStickers);
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
		int cupcakesToShare = 96;
		int numberOfChildren = 8;
		int cupcakesPerChild = cupcakesToShare/numberOfChildren;
		System.out.println("#39 " + cupcakesPerChild);
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
		int gingerbreadCookies = 47;
		int cookiesPerJar = 6;
		int jarlessCookies = gingerbreadCookies % cookiesPerJar;
		System.out.println("#40 " + jarlessCookies);
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
		int preparedCroissants = 59;
		int neighbors = 8;
		int mariansCroissants = preparedCroissants % neighbors;
		System.out.println("#41 " + mariansCroissants);
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
		int cookiesPerTray = 12;
		int totalCookies = 276;
		int traysNeeded = totalCookies/cookiesPerTray;
		System.out.println("#42 " + traysNeeded);
        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
		int numberOfPretzels = 480;
		int pretzelsPerServing = 12;
		int numberOfServings = numberOfPretzels/pretzelsPerServing;
		System.out.println("#43 " + numberOfServings);
        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
		int numberOfLemonCupcakes = 53;
		int remainingLemonCupcakes = 2;
		int lemonCupcakesPerBox = 3;
		int boxesGiven = (numberOfLemonCupcakes - remainingLemonCupcakes)/lemonCupcakesPerBox;
		System.out.println("#44 " + boxesGiven);
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
		int numberOfCarrotSticks = 74;
		int numberOfPeopleToServe = 12;
		int uneatenCarrotSticks = numberOfCarrotSticks % numberOfPeopleToServe;
		System.out.println("#45 " + uneatenCarrotSticks);
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
		int numberOfTeddies = 98;
		int teddiesPerShelf = 7;
		int shelvesNeeded = numberOfTeddies/teddiesPerShelf;
		System.out.println("#46 " + shelvesNeeded);
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
		int picturesPerAlbum = 20;
		int numberOfPictures = 480;
		int albumsNeeded = numberOfPictures/picturesPerAlbum;
		System.out.println("#47 " + albumsNeeded);
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
		int numberOfCards = 94;
		int cardsPerBox = 8;
		int fullBoxes = numberOfCards/cardsPerBox;
		int cardsInUnfilledBoxes = numberOfCards % cardsPerBox;
		System.out.println("#48 " + fullBoxes + " and " + cardsInUnfilledBoxes);
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
		int numberOfBooks = 210;
		int numberOfShelves = 10;
		int booksPerShelf = numberOfBooks/numberOfShelves;
		System.out.println("#49 " + booksPerShelf);
        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
		int bakedCroissants = 17;
		int croissantEatingGuests = 7;
		int croissantsPerGuest = bakedCroissants/croissantEatingGuests;
		System.out.println("#50 " + croissantsPerGuest);
        /*
            CHALLENGE PROBLEMS
        */

        /*
        Bill and Jill are house painters. Bill can paint a 12 x 14 room in 2.15 hours, while Jill averages
        1.90 hours. How long will it take the two painter working together to paint 5 12 x 14 rooms?
        Hint: Calculate the hourly rate for each painter, combine them, and then divide the total walls in feet by the combined hourly rate of the painters.
        Challenge: How many days will it take the pair to paint 623 rooms assuming they work 8 hours a day?.
        */
		double billsHourlyRate = 1/2.15;
		double jillsHourlyRate = 1/1.9;
		int roomsToPaint = 5;
		double timeItTakes = roomsToPaint/(billsHourlyRate + jillsHourlyRate);
		System.out.println("Challenge 1a: " + timeItTakes);
		final int workingHours = 8;
		roomsToPaint = 623;
		timeItTakes = roomsToPaint/(billsHourlyRate + jillsHourlyRate);
		double daysItTakes = timeItTakes/workingHours;
		System.out.println("Challenge 1b: " + daysItTakes);
        /*
        Create and assign variables to hold your first name, last name, and middle initial. Using concatenation,
        build an additional variable to hold your full name in the order of last name, first name, middle initial. The
        last and first names should be separated by a comma followed by a space, and the middle initial must end
        with a period.
        Example: "Hopper, Grace B."
        */
		String myFirstName = "Andrew";
		String myLastName = "Pignolet";
		char myMiddleInitial = 'C';
		String myFullName = myLastName + ", " + myFirstName + " " + myMiddleInitial + ".";
		System.out.println("Challenge 2: " + myFullName);
        /*
        The distance between New York and Chicago is 800 miles, and the train has already travelled 537 miles.
        What percentage of the trip has been completed?
        Hint: The percent completed is the miles already travelled divided by the total miles.
        Challenge: Display as an integer value between 0 and 100 using casts.
        */
		int milesNeeded = 800;
		int milesTravelled = 537;
		double percentageCompleted = (double)milesTravelled/milesNeeded;
		int percentAsInteger = (int)(percentageCompleted * 100);
		System.out.println("Challenge 3: " + percentAsInteger);

	}

}
