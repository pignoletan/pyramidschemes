package com.techelevator;   // Define a context in which terms or word or are known

public class Helloworld {   // Every Java program is a class

//  A function is a program
//  access independent	return-type  name(arguments)      // Every Java app starts a main function
	public static       void         main(String[] args) { // Java code is in blocks { }  
		System.out.println("Hello, world.");  // Java statement to print a line a on the screen
	}    // end of block 

}
