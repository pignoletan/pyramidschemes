package com.techelevator.notReal;

import java.util.List;

public interface FoodDAO {

	Food getFoodById(int id);
	Food getFoodByType(String type);
	List<Food> getUserPreferences(int userId);
	List<Food> getRestaurantFoods(int restaurantId);
}
