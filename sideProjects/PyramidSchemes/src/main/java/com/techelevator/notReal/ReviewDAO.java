package com.techelevator.notReal;

import java.util.List;

public interface ReviewDAO {

	Review getReviewById(int id);
	List<Review> getReviewsByUserId(int userId);
	List<Review> getReviewsByRestaurantId(int restaurantId);
	void deleteReview(int id);
	void addReview(Review aReview);
	void updateReview(int id, Review aReview);
	double averageRestaurantRating(int restaurantId);
}
