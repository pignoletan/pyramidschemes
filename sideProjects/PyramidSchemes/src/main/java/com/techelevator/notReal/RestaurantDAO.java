package com.techelevator.notReal;

import java.util.List;

public interface RestaurantDAO {
	public Restaurant getRestaurantById(int id);
	public List<Restaurant> getRestaurantByName(String name);
	public int getUserVisits(int userId, int restaurantId);
	public List<Restaurant> getUserFavorites(int userId);
	public Restaurant getRandomishRestaurants(int userId);
}
