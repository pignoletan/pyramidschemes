package com.techelevator.notReal;

public interface UserDao {
	
	public User getUserById(int id);
	public User getUserByUsername(String username);
	public void createUser(User aUser);
	public boolean isCorrectPassword(int id, String password);
	public void updatePassword(int id, String password);
	public void deleteUser(int id);
	public void addFoodPreference(int userId, int foodId);
	public void removeFoodPreference(int userId, int foodId);
	public void updateZipCode(int userId, int zipCode);
	public void updateBudget(int userId, int budget);
	public void addFavoriteRestaurant(int userId, int restaurantId);
}
