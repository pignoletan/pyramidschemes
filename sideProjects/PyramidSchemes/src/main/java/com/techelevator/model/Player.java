package com.techelevator.model;

public class Player {
	public final PlayerEnum FACTION;
	private int monez;
	
	public Player(PlayerEnum faction) {
		FACTION = faction;
		monez = 0;
	}
	
	public int getMonez() {
		return monez;
	}
	
	public void giveMonez(int amount) {
		monez += amount;
	}
}
