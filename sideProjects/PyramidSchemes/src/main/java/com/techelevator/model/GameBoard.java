package com.techelevator.model;

import java.util.ArrayList;

import com.techelevator.PyramidSchemes.App;

public class GameBoard {
	public static final int PYRAMID_PRICE = 15;
	public final int X_SIZE; // Maximum xsize of the board
	public final int Y_SIZE; // maximum ysize of the board
	private int[][] clashValues; // positive clash is blue, negative clash is red, 0 clash is neutral
	private boolean[][] preparedMove; //stores what moves the player is planning to make so they can all be processed at once
	private ArrayList<Pyramid> pyramids = new ArrayList<Pyramid>();

	public GameBoard(int xSize, int ySize) {
		X_SIZE = xSize;
		Y_SIZE = ySize;
		clashValues = new int[X_SIZE][Y_SIZE];
		preparedMove = new boolean[X_SIZE][Y_SIZE];
		resetClash();  //sets it all to 0
		resetPreparedMoves();  //sets it all to false
	}

	public boolean isRealSpace(int xCoord, int yCoord) {
		if (xCoord >= 0 && yCoord >= 0 && xCoord < X_SIZE && yCoord < Y_SIZE) {
			return true;
		}
		return false;
	}
	
	public boolean isSpaceOccupied(int xCoord, int yCoord) {
		for (Pyramid aPyramid : pyramids) {
			if (xCoord == aPyramid.XCOORD&&yCoord == aPyramid.YCOORD) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isPreparedMove(int xCoord, int yCoord) {
		return preparedMove[xCoord][yCoord];
	}
	
	public void setPreparedMove(int xCoord, int yCoord, boolean newVal) {
		preparedMove[xCoord][yCoord] = newVal;
	}
	
	/*
	 * If the space is currently set to true, set to false, return true, and refund the player
	 * If the space is currently set to false and the player can't afford to build here, return false
	 * If the space is currently set to false and the player can afford to build here, set to true, return true,
	 * and extract funds from that player
	 */
	public boolean togglePreparedMove(int xCoord, int yCoord, Player aPlayer) {
		int price = PYRAMID_PRICE;
		if (isSpaceOccupied(xCoord,yCoord)) {
			price = (getPyramidAtSpace(xCoord,yCoord).getStories() + 1) * PYRAMID_PRICE;
		}
		if (preparedMove[xCoord][yCoord]) {
			preparedMove[xCoord][yCoord] = false;
			aPlayer.giveMonez(price);
			return true;
		}
		if (aPlayer.getMonez() < price) {
			return false;
		}
		preparedMove[xCoord][yCoord] = true;
		aPlayer.giveMonez(-price);
		return true;
	}

	// return whether or not it successfully made a pyramid
	public Pyramid makeNewPyramid(int xCoord, int yCoord, PlayerEnum faction) {
		Pyramid aPyramid = null;
		if (!isRealSpace(xCoord, yCoord)) {
			return aPyramid;
		}
		aPyramid = new Pyramid(xCoord, yCoord, faction);
		pyramids.add(aPyramid);
		return aPyramid;
	}

	public int[][] getClashValues() {
		return clashValues;
	}
	
	public int getClashAtPoint(int xCoord, int yCoord) {
		return clashValues[xCoord][yCoord];
	}

	public ArrayList<Pyramid> getPyramids() {
		return pyramids;
	}

	public boolean isSpaceFaction(int xCoord, int yCoord, PlayerEnum faction) {
		for (int i = 0; i < pyramids.size(); i++) {
			Pyramid aPyramid = pyramids.get(i);
			if (aPyramid.XCOORD == xCoord && aPyramid.YCOORD == yCoord) {
				if (aPyramid.FACTION.equals(faction)) {
					return true;
				}
				return false;
			}

		}
		if (clashValues[xCoord][yCoord] > 0 && faction.equals(PlayerEnum.BLUE)) {
			return true;
		}
		if (clashValues[xCoord][yCoord] < 0 && faction.equals(PlayerEnum.RED)) {
			return true;
		}
		return false;
	}

	public int getNumOfSpacesOwned(PlayerEnum faction) {
		int result = 0;
		for (int x = 0; x < X_SIZE; x++) {
			for (int y = 0; y < Y_SIZE; y++) {
				if (isSpaceFaction(x, y, faction)) {
					result++;
				}
			}
		}
		return result;
	}

	public void calculateClash() {
		resetClash();
		for (int i = 0; i < pyramids.size(); i++) {
			Pyramid aPyramid = pyramids.get(i);
			int stories = aPyramid.getStories();
			PlayerEnum faction = aPyramid.FACTION;
			for (int j = 1; j <= stories; j++) {
				for (int z = 0; z < 4; z++) { // run once for each cardinal direction
					int xAdj = 0;
					int yAdj = 0;
					int xInc = 0;
					int yInc = 0;
					switch (z) {
					case 0:
						xAdj = j;
						xInc = -1;
						yInc = -1;
						break;
					case 1:
						yAdj = -j;
						xInc = -1;
						yInc = 1;
						break;
					case 2:
						xAdj = -j;
						xInc = 1;
						yInc = 1;
						break;
					case 3:
						yAdj = j;
						xInc = 1;
						yInc = -1;
						break;
					}
					for (int k = 0; k < j; k++) {
						if (isRealSpace(aPyramid.XCOORD + xAdj, aPyramid.YCOORD + yAdj)) {
							if (faction.equals(PlayerEnum.BLUE)) {
								clashValues[aPyramid.XCOORD + xAdj][aPyramid.YCOORD + yAdj] += stories + 1 - j;
							} else {
								clashValues[aPyramid.XCOORD + xAdj][aPyramid.YCOORD + yAdj] -= stories + 1 - j;
							}
						}
						xAdj += xInc;
						yAdj += yInc;

					}
				}
			}
		}
	}

	public void resetClash() {
		for (int x = 0; x < X_SIZE; x++) {
			for (int y = 0; y < Y_SIZE; y++) {
				clashValues[x][y] = 0;
			}
		}
	}
	
	public void resetPreparedMoves() {
		for (int x = 0; x < X_SIZE; x++) {
			for (int y = 0; y < Y_SIZE; y++) {
				preparedMove[x][y] = false;
			}
		}
	}
	
	public void buildPreparedMoves(PlayerEnum faction) {
		for (int x = 0; x < X_SIZE; x++) {
			for (int y = 0; y < Y_SIZE; y++) {
				if (preparedMove[x][y]) {
					preparedMove[x][y] = false;
					if (!isSpaceOccupied(x,y)) {
						makeNewPyramid(x,y,faction);
					} else {
						Pyramid thisPyramid = getPyramidAtSpace(x,y);
						thisPyramid.addStories(1);
					}
				}
			}
		}
	}

	public static int getGridDistance(int x1, int y1, int x2, int y2) {
		int xDiff = Math.abs(x1 - x2);
		int yDiff = Math.abs(y1 - y2);
		return xDiff + yDiff;
	}
	
	public Pyramid getPyramidAtSpace(int xCoord, int yCoord) {
		Pyramid result = null;
		for (Pyramid aPyramid : pyramids) {
			if (xCoord == aPyramid.XCOORD&&yCoord == aPyramid.YCOORD) {
				result = aPyramid;
			}
		}
		return result;
	}
	
	/*
	 * Destroy all pyramids where the enemy player has clash on that space
	 */
	public void destroyWeakPyramids() {
		for (int i = 0; i < pyramids.size(); i++) {
			Pyramid aPyramid = pyramids.get(i);
			if ((aPyramid.FACTION == PlayerEnum.BLUE&&clashValues[aPyramid.XCOORD][aPyramid.YCOORD] < 0) ||
					(aPyramid.FACTION == PlayerEnum.RED&&clashValues[aPyramid.XCOORD][aPyramid.YCOORD] > 0)) {
				pyramids.remove(i);
				i--;
			}
		}
	}
	
	public String canBuildOnSpace(int xCoord, int yCoord, Player aPlayer) {
		if (preparedMove[xCoord][yCoord]) {
			return "";
		}
		int price = 0;
		if (isSpaceFaction(xCoord,yCoord,aPlayer.FACTION)) {
			Pyramid aPyramid = getPyramidAtSpace(xCoord,yCoord);
			if (aPyramid == null) {
				price = PYRAMID_PRICE;
			} else {
				int stories = aPyramid.getStories();
				if (stories == 5) {
					return "Pyramids can only be five stories tall! CHEATER!";
				} else {
					price = (stories + 1) * PYRAMID_PRICE;
				}
			}
		} else {
			return "You don't own that space! CHEATER!";
		}
		if (aPlayer.getMonez() < price) {
			return "You poor pauper! Thou cannst afford such prime real estate.";
		}
		return "";
	}
	
	public boolean checkVictory() {
		if (App.bluePlayer.getMonez() >= 350) {
			System.out.println("Unacceptable! Blue player wins via their superior wealth!");
			return true;
		}
		if (App.redPlayer.getMonez() >= 350) {
			System.out.println("Woohoo! Red player wins just by having all the monez!");
			return true;
		}
		boolean bluePlayerHasPyramids = false;
		boolean redPlayerHasPyramids = false;
		for (Pyramid aPyramid: pyramids) {
			if (aPyramid.FACTION == PlayerEnum.BLUE) {
				bluePlayerHasPyramids = true;
			} else {
				redPlayerHasPyramids = true;
			}
		}
		if (!bluePlayerHasPyramids) {
			System.out.println("Yeah crush 'em! The red player destroyed literally all of the blue player's pyramids!");
			return true;
		}
		if (!redPlayerHasPyramids) {
			System.out.println("What? Somehow, the blue player destroyed the red player's pyramids!");
			return true;
		}
		return false;
	}
}
