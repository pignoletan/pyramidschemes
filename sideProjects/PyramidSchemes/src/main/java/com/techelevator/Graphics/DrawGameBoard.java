package com.techelevator.Graphics;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.techelevator.PyramidSchemes.App;
import com.techelevator.model.GameBoard;
import com.techelevator.model.PlayerEnum;
import com.techelevator.model.Pyramid;

public class DrawGameBoard extends JPanel {
	private static final int BOARD_X_COORD = 100; // top left X coordinate of board
	private static final int BOARD_Y_COORD = 100; // top left Y coordinate of board
	private static final int PYRAMID_WIDTH = 32;
	private static final int PYRAMID_HEIGHT = 32;
	private static final int FONT_SIZE = 18;
	private static final int PANEL_WIDTH = 800;
	private static final int PANEL_HEIGHT = 600;
	private static final Font FONT = new Font("Verdana", Font.BOLD, FONT_SIZE);
	private static JFrame frame = new JFrame("Pyramid Schemes");
	public static final int SIZE_INC = 3; //how much space to put in between each story in the pyramid
	private int clashToDisplay = 0;
	private boolean displayClash = false;
	private String messageForUser = "";

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawClash(g);
		drawPyramids(g);
		drawMonez(g);
		drawPreparedMoves(g);
		drawInspectedClash(g);
		drawMessageForUser(g);
		if (this.getMouseListeners().length == 0) {
			handleMouseListeners();
		}
	}
	
	private void handleMouseListeners() {
		addMouseMotionListener(new MouseAdapter() {
			@Override
            public void mouseMoved(MouseEvent e) {
				Pyramid aPyramid = getPyramidAtPoint(e.getX(),e.getY());
				if (aPyramid != null) {
					clashToDisplay = getClashAtPoint(e.getX(),e.getY());
					displayClash = true;
				} else {
					displayClash = false;
				}
				updatePanel();
			}
		});
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				int xCoord = getXOnBoard(e.getX());
				int yCoord = getYOnBoard(e.getY());
				if (App.theGameBoard.isRealSpace(xCoord, yCoord)) {
					messageForUser = App.theGameBoard.canBuildOnSpace(xCoord, yCoord, App.currentPlayer);
					if (messageForUser.equals("")) {
						App.theGameBoard.togglePreparedMove(xCoord, yCoord, App.currentPlayer);
					}
				}
				updatePanel();
			}
		});
	
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println("I did it!");
				if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					App.endTurn();
				}
			}
		});
	}

	private void drawPyramids(Graphics g) {
		for (Pyramid aPyramid : App.theGameBoard.getPyramids()) {
			g.setColor(Color.blue);
			if (aPyramid.FACTION == PlayerEnum.RED) {
				g.setColor(Color.red);
			}
			int x = BOARD_X_COORD + aPyramid.XCOORD * PYRAMID_WIDTH;
			int y = BOARD_Y_COORD + aPyramid.YCOORD * PYRAMID_HEIGHT;
			g.fillRect(x, y, PYRAMID_WIDTH, PYRAMID_HEIGHT);
			g.setColor(Color.black);
			int sizeAdjust = 0;
			for (int i = 0; i < aPyramid.getStories(); i++) {
				g.drawRect(x + sizeAdjust, y + sizeAdjust, PYRAMID_WIDTH - sizeAdjust * 2,
						PYRAMID_HEIGHT - sizeAdjust * 2);
				sizeAdjust += SIZE_INC;
			}

		}
	}
	
	private void drawPreparedMoves(Graphics g) {
		g.setColor(Color.yellow);
		for (int x = 0; x < App.theGameBoard.X_SIZE; x++) {
			for (int y = 0; y < App.theGameBoard.Y_SIZE; y++) {
				if (App.theGameBoard.isPreparedMove(x, y)) {
					int sizeAdjust = 0;
					if (App.theGameBoard.isSpaceOccupied(x, y)) {
						sizeAdjust = SIZE_INC * App.theGameBoard.getPyramidAtSpace(x, y).getStories();
					}
					int xCoord = x * PYRAMID_WIDTH + sizeAdjust + BOARD_X_COORD;
					int yCoord = y * PYRAMID_HEIGHT + sizeAdjust + BOARD_Y_COORD;
					g.drawRect(xCoord, yCoord, PYRAMID_WIDTH - sizeAdjust * 2, PYRAMID_HEIGHT - sizeAdjust * 2);
				}
			}
		}
	}
	
	private void drawClash(Graphics g) {
		g.setFont(FONT);
		int xAdj = PYRAMID_WIDTH/2 - FONT_SIZE/4;
		int yAdj = FONT_SIZE/2 + PYRAMID_HEIGHT/2;
		int[][] clashValues = App.theGameBoard.getClashValues();
		for (int x = 0; x < App.theGameBoard.X_SIZE; x++) {
			for (int y = 0; y < App.theGameBoard.Y_SIZE; y++) {
				g.setColor(Color.black);
				int clash = clashValues[x][y];
				if (clash > 0) {
					g.setColor(Color.blue);
				}
				if (clash < 0) {
					g.setColor(Color.red);
				}
				int xCoord = BOARD_X_COORD + x * PYRAMID_WIDTH + xAdj;
				int yCoord = BOARD_Y_COORD + y * PYRAMID_HEIGHT + yAdj;
				g.drawString(Integer.toString(Math.abs(clash)), xCoord, yCoord);
			}
		}
	}

	@Override
	public Dimension getPreferredSize() {
		// so that our GUI is big enough
		return new Dimension(PANEL_WIDTH, PANEL_HEIGHT);
	}

	// create the GUI explicitly on the Swing event thread
	public void createAndShowGui() {
		DrawGameBoard mainPanel = new DrawGameBoard();

		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		frame.setFocusable(true);
		
	}
	
	public void drawMonez(Graphics g) {
		g.setFont(FONT);
		int blueMoney = App.bluePlayer.getMonez();
		int redMoney = App.redPlayer.getMonez();
		g.setColor(Color.blue);
		g.drawString(Integer.toString(blueMoney) + " (" + Integer.toString((int)Math.floor(blueMoney/15)) + ")",
				PANEL_WIDTH/2, 20);
		g.setColor(Color.red);
		g.drawString(Integer.toString(redMoney) + " (" + Integer.toString((int)Math.floor(redMoney/15)) + ")",
				PANEL_WIDTH/2, 40 + FONT_SIZE);
	}
	
	/*
	 * Draws the clash of whatever pyramid the user is mousing over
	 */
	public void drawInspectedClash(Graphics g) {
		if (displayClash) {
			g.setFont(FONT);
			g.setColor(Color.black);
			if (clashToDisplay > 0) {
				g.setColor(Color.blue);
			}
			if (clashToDisplay < 0) {
				g.setColor(Color.red);
			}
			g.drawString(Integer.toString(Math.abs(clashToDisplay)), 4 + FONT_SIZE, 4 + FONT_SIZE);
		}
	}
	
	public void drawMessageForUser(Graphics g) {
		g.setColor(Color.black);
		g.setFont(FONT);
		g.drawString(messageForUser,PANEL_WIDTH/8,68 + FONT_SIZE);
	}
	
	public void updatePanel() {
		frame.revalidate();
		frame.repaint();
	}
	
	public Pyramid getPyramidAtPoint(int xCoord, int yCoord) {
		Pyramid result = null;
		xCoord = getXOnBoard(xCoord);
		yCoord = getYOnBoard(yCoord);
		if (App.theGameBoard.isRealSpace(xCoord, yCoord)) {
			result = App.theGameBoard.getPyramidAtSpace(xCoord, yCoord);
		}
		return result;
	}
	
	public int getClashAtPoint(int xCoord, int yCoord) {
		xCoord = getXOnBoard(xCoord);
		yCoord = getYOnBoard(yCoord);
		if (App.theGameBoard.isRealSpace(xCoord, yCoord)) {
			return App.theGameBoard.getClashAtPoint(xCoord, yCoord);
		}
		return 0;
	}

	public int getXOnBoard(int xMouseCoord) {
		xMouseCoord -= BOARD_X_COORD;
		return (int) Math.floor(xMouseCoord/PYRAMID_HEIGHT);
	}
	
	public int getYOnBoard(int yMouseCoord) {
		yMouseCoord -= BOARD_Y_COORD;
		return (int) Math.floor(yMouseCoord/PYRAMID_WIDTH);
	}
	
}
