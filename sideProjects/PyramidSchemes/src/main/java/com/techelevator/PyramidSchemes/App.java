package com.techelevator.PyramidSchemes;

import java.util.Scanner;

import com.techelevator.Graphics.DrawGameBoard;
import com.techelevator.model.GameBoard;
import com.techelevator.model.Player;
import com.techelevator.model.PlayerEnum;
import com.techelevator.model.Pyramid;

/**
 * Hello world!
 *
 */
public class App 
{
	public static GameBoard theGameBoard = new GameBoard(10,12);
	public static DrawGameBoard drawGameBoard = new DrawGameBoard();
	public static Player bluePlayer = new Player(PlayerEnum.BLUE);
	public static Player redPlayer = new Player(PlayerEnum.RED);
	public static Player currentPlayer = bluePlayer;
    public static void main( String[] args )
    {
    	bluePlayer.giveMonez(15);
    	redPlayer.giveMonez(30);
    	Scanner scanner = new Scanner(System.in);
    	initializeGameBoard();
        theGameBoard.calculateClash();
        drawGameBoard.createAndShowGui();
        boolean gameInProgress = true;
        while (gameInProgress)
        {
        	boolean correctInput = false;
        	int xCoord = 0;
    		int yCoord = 0;
        	while (!correctInput) {
        		System.out.println("Enter a pyramid coordinate like: X,Y:\nOR type \"f\"");
        		String userInput = scanner.nextLine();
        		if (userInput.equals("f")) {
        			currentPlayer = endTurn();
        			correctInput = true;
        			continue;
        		}
        		String[] coordinates = userInput.split(",");
        		if (coordinates.length != 2) {
        			System.out.println("Please put in two numbers separated by a comma");
        			continue;
        		}
        		try {
        			xCoord = Integer.parseInt(coordinates[0]);
        			yCoord = Integer.parseInt(coordinates[1]);
        		} catch (NumberFormatException e) {
        			System.out.println("Please input whole numbers only");
        			continue;
        		}
        		xCoord--;
        		yCoord--;
        		if (!theGameBoard.isRealSpace(xCoord, yCoord)) {
        			System.out.println("Please pick a space that is on the board");
        			continue;
        		}
        		if (!theGameBoard.isSpaceFaction(xCoord, yCoord, currentPlayer.FACTION)) {
        			System.out.println("You don't own that space! CHEATER!!!");
        			continue;
        		}
        		Pyramid aPyramid = theGameBoard.getPyramidAtSpace(xCoord, yCoord);
            	if (aPyramid != null) {
            		if (aPyramid.getStories() > 4) {
            			System.out.println("Pyramids can only have five stories you cheat!");
            			continue;
            		}
            		if (!theGameBoard.togglePreparedMove(xCoord, yCoord,currentPlayer)) {
            			System.out.println("You poor pauper! Thou canst afford such prime real estate!");
            			continue;
            		}
            	}
            	else {
            		if (!theGameBoard.togglePreparedMove(xCoord, yCoord,currentPlayer)) {
            			System.out.println("You poor pauper! Thou canst afford such prime real estate!");
            			continue;
            		}
            	}
            	System.out.println("Yeah, you can do that!");
        		correctInput = true;
        	}
        	drawGameBoard.updatePanel();
        	gameInProgress = !theGameBoard.checkVictory();
        }
    }
    
    public static Player endTurn() {
    	theGameBoard.buildPreparedMoves(currentPlayer.FACTION);
    	Player result = new Player(PlayerEnum.BLUE);
		if (currentPlayer == bluePlayer) {
    		System.out.println("Go red player, go!");
    		result =  redPlayer;
    	} else {
    		result = bluePlayer;
    		System.out.println("Not the blue player again!!");
    	}
    	theGameBoard.calculateClash();
    	theGameBoard.destroyWeakPyramids();
    	theGameBoard.calculateClash();
    	currentPlayer.giveMonez(theGameBoard.getNumOfSpacesOwned(currentPlayer.FACTION));
    	return result;
    }
    
    private static void initializeGameBoard() {
        Pyramid aPyramid = theGameBoard.makeNewPyramid(2, 4, PlayerEnum.BLUE);
        aPyramid.addStories(1);
        aPyramid = theGameBoard.makeNewPyramid(2, 7, PlayerEnum.RED);
        aPyramid.addStories(1);
        aPyramid = theGameBoard.makeNewPyramid(4, 8, PlayerEnum.BLUE);
        aPyramid.addStories(1);
        aPyramid = theGameBoard.makeNewPyramid(4, 3, PlayerEnum.RED);
        aPyramid.addStories(1);
        aPyramid = theGameBoard.makeNewPyramid(6, 4, PlayerEnum.BLUE);
        aPyramid.addStories(1);
        aPyramid = theGameBoard.makeNewPyramid(6, 7, PlayerEnum.RED);
        aPyramid.addStories(1);
        aPyramid = theGameBoard.makeNewPyramid(7, 9, PlayerEnum.BLUE);
        aPyramid.addStories(1);
        aPyramid = theGameBoard.makeNewPyramid(7, 2, PlayerEnum.RED);
        aPyramid.addStories(1);
    }
}
