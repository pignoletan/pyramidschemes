-- Write queries to return the following:
-- The following changes are applied to the "dvdstore" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.

begin transaction;

insert into actor(first_name,last_name) values('HAMPTOM','AVENUE');
insert into actor(first_name,last_name) values('LISA','BYWAY');

select *
from actor
order by last_name;

commit;

-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in
-- ancient Greece", to the film table. The movie was released in 2008 in English.
-- Since its an epic, the run length is 3hrs and 18mins. There are no special
-- features, the film speaks for itself, and doesn't need any gimmicks.

begin transaction;

insert into film(title,description,release_year,language_id,length)
values ('EUCLIDIAN PI','The epic story of Euclid as a pizza delivery boy in ancient Greece',
2008,(select language_id from language where name = 'English'),198);

select *
from film
where title = 'EUCLIDIAN PI';

commit;

-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.

begin transaction;

insert into film_actor(film_id,actor_id)
values((select film_id from film where title = 'EUCLIDIAN PI')
, (select actor_id from actor where first_name = 'LISA' and last_name = 'BYWAY'));

insert into film_actor(film_id,actor_id)
values((select film_id from film where title = 'EUCLIDIAN PI')
, (select actor_id from actor where first_name = 'HAMPTOM' and last_name = 'AVENUE'));

select actor
from actor, film_actor, film
where actor.actor_id = film_actor.actor_id
and film_actor.film_id = film.film_id
and film.title = 'EUCLIDIAN PI';

commit;

-- 4. Add Mathmagical to the category table.

begin transaction;

insert into category(name) values('Mathmagical');

select *
from category
where name = 'Mathmagical';

commit;

-- 5. Assign the Mathmagical category to the following films, "Euclidean PI",
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"

begin transaction;

insert into film_category(film_id,category_id)
values((select film_id from film where title = 'EUCLIDIAN PI')
, (select category_id from category where name = 'Mathmagical'));
        
update film_category
set category_id = (select category_id
        from category
        where name = 'Mathmagical')
where film_id = (select film_id
        from film
        where title = 'EGG IGBY');


update film_category
set category_id = (select category_id
        from category
        where name = 'Mathmagical')
where film_id = (select film_id
        from film
        where title = 'KARATE MOON');


update film_category
set category_id = (select category_id
        from category
        where name = 'Mathmagical')
where film_id = (select film_id
        from film
        where title = 'RANDOM GO');
        
update film_category
set category_id = (select category_id
        from category
        where name = 'Mathmagical')
where film_id = (select film_id
        from film
        where title = 'YOUNG LANGUAGE')
;

select *
from film
where film_id in (select film_id
        from film_category
        where category_id in (select category_id
                from category
                where name = 'Mathmagical'));

commit;

-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films
-- accordingly.
-- (5 rows affected)

begin transaction;

update film
set rating = 'G'
where film_id in (select film_id from film_category
        where category_id in (select category_id from category where name = 'Mathmagical'));
        
select title, rating
from film
where film_id in (select film_id from film_category
        where category_id in (select category_id from category where name = 'Mathmagical'));

commit;

-- 7. Add a copy of "Euclidean PI" to all the stores.

begin transaction;

insert into inventory(store_id,film_id)
values (1,(select film_id from film where title = 'EUCLIDIAN PI'));

insert into inventory(store_id,film_id)
values (2,(select film_id from film where title = 'EUCLIDIAN PI'));

select *
from inventory
where film_id in (select film_id from film where title = 'EUCLIDIAN PI');

commit;

-- 8. The Feds have stepped in and have impounded all copies of the pirated film,
-- "Euclidean PI". The film has been seized from all stores, and needs to be
-- deleted from the film table. Delete "Euclidean PI" from the film table.
-- (Did it succeed? Why?)
-- No, violates foreign key constraint because Euclidian Pi is in inventory and film_category

begin transaction;

delete from film
where title = 'EUCLIDIAN PI';

rollback;

-- 9. Delete Mathmagical from the category table.
-- (Did it succeed? Why?)
-- No, violates foreign key constraint because Mathmagical is on film_category table

begin transaction;

delete from category
where name = 'Mathmagical';

rollback;

-- 10. Delete all links to Mathmagical in the film_category tale.
-- (Did it succeed? Why?)
-- Yes, category_id is a foreign key and can therefore have no entries on film_category

begin transaction;

delete from film_category
where category_id in (select category_id from category where name = 'Mathmagical');

select *
from film_category
where category_id in (select category_id from category where name = 'Mathmagical');

commit;

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI".
-- (Did either deletes succeed? Why?)
-- Deleting mathmagical succeeded because it no longer has an entry on the film_category table
-- Still cannot delete Euclidian Pi because it has an entry on the film_actor table

begin transaction;

delete from category
where name = 'Mathmagical';

commit;

begin transaction;

delete from film
where title = 'EUCLIDIAN PI';

rollback;

-- 12. Check database metadata to determine all constraints of the film id, and
-- describe any remaining adjustments needed before the film "Euclidean PI" can
-- be removed from the film table.

--Need to remove all references to actors in Euclidian Pi
--Need to remove all references to Euclidian Pi in the inventory table