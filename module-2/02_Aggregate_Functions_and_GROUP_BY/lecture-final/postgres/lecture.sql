--------------------------------------------------------------------------------------------------------
-- Ordering, Grouping Basic Functions Lecture Code
--------------------------------------------------------------------------------------------------------
--
-- ORDER BY -  Sequence of Rows in Result
--
--    ORDER BY          -- Ascending Sequence (low-high)
--    ORDER BY ASC      -- Ascending Sequence (low-high)
--    ORDER BY DESC     -- Descending Sequence (high-low)

-- Show Populations of all countries in acscending order
  select population, name
    from country
order by population   -- ASC is default
;
  select population, name
    from country
order by 1            -- column # in the SELECT may be used in place of a column name in the ORDER BY
;

  select population / surfacearea, name
    from country 
   where population > 0 
order by 1            -- column # in the SELECT 
;

  select population / surfacearea as andrej, name
    from country 
   where population > 0 
order by andrej        -- as-name in the SELECT for order by
;
-- Show Populations of all countries in descending order
  select population, name
    from country
order by population  desc

-- Show  the names of countries and continents in ascending order
  select  name, continent
    from country
order by continent  -- ASC is default
;
  select  name, continent
    from country
order by continent desc, name
;
--------------------------------------------------------------------------------------------------------
-- Limiting the number of rows in the result
--
-- LIMIT n   - Limit the number of rows in the result - always goes at the end of the SELECT
--
--

  select  name, continent
    from country
order by continent desc, name
limit 10   -- limit result to 10 rows
;
-- Show the name and average life expectancy of the countries with the 10 highest life expectancies.
  select name, lifeexpectancy
    from country
   where lifeexpectancy is not null
order by lifeexpectancy desc
   limit 10 
;
--------------------------------------------------------------------------------------------------------
-- Concatenating values 
--
-- the concat operator (||) may be used to concatenate character (string) values in a result
--

-- Show the name & state in the format: "city-name, state"
-- of all cities in California, Oregon, or Washington.
-- sorted by state then city
  select name || ', ' || district
    from city
   where district in ('California', 'Oregon', 'Washington')
order by district, name
;

--------------------------------------------------------------------------------------------------------
-- Aggregate functions - produce one row in result for each group specified
--
-- The group used by the aggregate functions is determined by the GROUP BY clause
-- if no GROUP BY clause is specified, the group is the set of rows in the result
--
--     AVG(column-expression)   - arithmentic average for group of non-NULL values in expression 
--     SUM(column-expression)   - arithmentic sum for group of a non-NULL values in expression 
--     MIN(column-expression)   - lowest value for group of non-NULL values in expression 
--     MAX(column-expression)   - highest value for group of non-NULL values in expression 
--     COUNT(*)                 - number of rows in the group
--     COUNT(column-expression) - number of rows for the group of non-NULL values in expression 
--
--
-- AVG(), SUM() may only bes used with numeric data types
-- MIN(), MAX() may be used with numeric, character, date, time datatypes
--
-- COUNT() is applied to rows (not columns)
--
--
-- Show average life expectancy in the world
select avg(lifeexpectancy)   -- one row in the result for each group 
  from country
  ;                          -- no group by - the group is the entire result set
  
-- Show average life expectancy for each continent in the world
select continent, avg(lifeexpectancy)   -- one row in the result for each group 
  from country
group by continent           -- how we want the avgage to be calculated
  ;                          --  on line in result for each continent
  
-- Show the total population in Ohio and Michigan
select district, sum(population)
  from city
 where district = 'Ohio'
    or district = 'Michigan'
group by district
;
-- Show the surface area of the smallest country in the world
-- Stay tuned for how to include the name...
select min(surfacearea)
  from country
;
-- Show The 10 largest countries (by surface area) in the world
  select name, surfacearea
    from country
order by surfacearea desc  
   limit 10 
;
-- Show the number of countries who declared independence in 1991
select count(*)
  from country
 where indepyear = 1991
;
--------------------------------------------------------------------------------------------------------
-- GROUP BY  - Specify the group to which the aggregate functions apply
--
--      GROUP BY column-expression
--
-- When using a GROUP BY the SELECT is limited ot aggregate functions or columns in the GROUP BY
--
--

-- Show the number of countries where each language is spoken, order show them from most countries to least
select   language, count(*) as Nathan
  from   countrylanguage
group by language
order by Nathan desc
;
-- Show the average life expectancy of each continent ordered from highest to lowest
select continent, avg(lifeexpectancy)
  from country
  group by continent
  having avg(lifeexpectancy) is not null  -- filter groups for the result 
order by 2 desc
;
-- Show the average life expectancy of each continent where lifeepectancy is 70 or more ordered from highest to lowest
select continent, avg(lifeexpectancy)
  from country
  group by continent
  having avg(lifeexpectancy) >= 70  -- filter groups for the result 
order by 2 desc
;


-- Exclude Antarctica from consideration for average life expectancy
select continent, avg(lifeexpectancy)
  from country
  group by continent
  having avg(lifeexpectancy) is not null  -- filter groups for the result 
order by 2 desc
;


-- What is the sum of the population of cities in each state in the USA ordered by state name
select   district, sum(population)
  from   city
 where   countrycode = 'USA'
group by district 
order by district   -- order by 1 is OK too 
;

-- What is the average population of cities in each state in the USA ordered by state name
select   district, avg(population)
  from   city
 where   countrycode = 'USA'
group by district 
order by district   -- order by 1 is OK too 
;

--------------------------------------------------------------------------------------------------------
-- SUBQUERIES - Using the result from one query (inner query) in another query (outer query)
--
-- Frequently used in a WHERE clause with an IN predicate:
--
--       WHERE column-name IN (SELECT column-name FROM some-table WHERE some-predicate)
--
-- Any WHERE predicate may be used to connect the subquery in a WHERE clause, but you must
-- be sure a single value is returned from the subquery. 
--
-- Subqueries may also be used in a SELECT as a column-specification or a FROM as a table
-- (These are advanced concepts we will discuss later, if there is time)
--
-- Show the cities under the same given government leader

select name, countrycode
  from city
 where countrycode in (select code -- get a list countrycodes with same head of state
                         from country outerTable              -- give a nickname to the table in the outer select
                        where headofstate in (select headofstate              -- list of headofstate
                                                from country                  -- country table
                                               where code != outerTable.code)) -- for all countries but the one from outer select    
;
-- Show countries with the same independece year
select name, indepyear
  from country  c1
 where indepyear in (select indepyear     -- list of indepyear that are not the one in the country outer query
                       from country
                      where code != c1.code) 
 order by indepyear
 ;

-- Show the cities cities whose country has not yet declared independence yet


--------------------------------------------------------------------------------------------------------
--
-- Additional samples
--
-- You may alias column and table names to provide more descriptive names
--
SELECT name AS CityName 
  FROM city AS cities

-- Ordering allows columns to be displayed in ascending order, or descending order (Look at Arlington)
SELECT name
     , population 
  FROM city 
 WHERE countryCode='USA' 
 ORDER BY name ASC, population DESC
;
-- Limiting results allows rows to be returned in 'limited' clusters where LIMIT says how many, 
-- and an optional OFFSET specifies number of rows to skip
SELECT name
     , population 
  FROM city 
  LIMIT 10 OFFSET 10
;