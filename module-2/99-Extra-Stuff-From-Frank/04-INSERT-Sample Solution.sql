begin transaction;
--
-- Be sure we have not already done the updates
-- This delete is only here for testing - it should be removed when we wanrt to do this for real
--
--
delete from film_category where category_id in (select distinct category_id from category where name = 'Mathemagical');
--
--  Find the current film_id for the film we want and current category_id for the category we want
--  and use them to insert the row
--
insert into film_category
(film_id, category_id)
select (select film_id from film where film.title = 'EUCLIDIAN PI')          
      ,(select distinct category_id from category where name = 'Mathemagical')
;
insert into film_category
(film_id, category_id)
select (select film_id from film where film.title = 'EGG IGBY')          
      ,(select distinct category_id from category where name = 'Mathemagical')
;
insert into film_category
(film_id, category_id)
select (select film_id from film where film.title = 'KARATE MOON')          
      ,(select distinct category_id from category where name = 'Mathemagical')
;
insert into film_category
(film_id, category_id)
select (select film_id from film where film.title = 'RANDOM GO')          
      ,(select distinct category_id from category where name = 'Mathemagical')
;
insert into film_category
(film_id, category_id)
select (select film_id from film where film.title = 'YOUNG LANGUAGE')          
      ,(select distinct category_id from category where name = 'Mathemagical')
;
--
-- Verify we have inserted the proper rows
-- This select is only here for testing - it should be removed when we wanrt to do this for real
--

select film_id, title from film where film.title in ( 'EUCLIDIAN PI', 'EGG IGBY', 'KARATE MOON', 'RANDOM GO', 'YOUNG LANGUAGE');
select distinct name category_id from category where name = 'Mathemagical';

select film_id, category_id
from film_category
where category_id = (select distinct category_id from category where name = 'Mathemagical')
;
--
-- rollback only for testing - change to commit when we are sure INSERTs are being done correctly
--
rollback;