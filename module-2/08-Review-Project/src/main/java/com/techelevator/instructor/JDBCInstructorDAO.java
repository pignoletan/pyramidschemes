package com.techelevator.instructor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

public class JDBCInstructorDAO implements InstructorDAO {
//  declare a jdbcTemplate object to use when interface with the Spring JDBC Framework

	private JdbcTemplate jdbcTemplate;
	
// Define a ctor for the JDBC/DAO that takes a data source as a parameter
	public JDBCInstructorDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);  // instantiate and initialize jdbcTemplate object.
	}
	@Override
	// Insert an Instructor object into the Instructor table
	// return true if update was successful, false, if not
	public boolean save(Instructor anInstructor) {
		
		String sqlInsertInstructor = "Insert into instructors " 
				                    +"(instructorid, firstname, middlename, lastname, homelocation) " 
				                    +" Values(?, ?, ?, ?, ?)";
		try {
		int resultValue = jdbcTemplate.update(sqlInsertInstructor,anInstructor.getFirstName().substring(0,3)  // create primary key using new data
	                                                             +anInstructor.getLastName().substring(0,3)
		                                                         ,anInstructor.getFirstName()
		                                                         ,anInstructor.getMiddleName()
		                                                         ,anInstructor.getLastName()
		                                                         ,anInstructor.getHomeLocation());	
		return resultValue == 1 ? true : false;   // if update() result was 0 - update worked, if not, it didn't
		}
		catch (DataAccessException exceptionInfo) {
			System.out.println("Problem performing save to database");
			System.out.println("System reported: " + exceptionInfo.getMessage());
			exceptionInfo.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean save(String aFirstName, String aLastName, String aLocation) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean delete(Instructor anInstructor) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean delete(String anInstructorId) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean delete(String aFirstName, String aLocation) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean update(Instructor anInstructor) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Instructor searchByInstructorId(String anInstructorId) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<Instructor> searchByLocation(String aLocation) {
		// define result object
		List<Instructor> allInstructors = new ArrayList<Instructor>();  
		
		// define a String to hold SQL statement to be run
		String sqlSearchLocation = "select *"           + 
				                   "  from instructors" + 
				                   " where homelocation = ?";
		
		// define SqlRowSet object to hold the result from the SQL and run the SQL supplying values for any ?'s
		SqlRowSet rowsReturned = jdbcTemplate.queryForRowSet(sqlSearchLocation, aLocation);
		
		// loop through the result adding each row to result list
		while(rowsReturned.next()) {  // loop for each row in the result
		Instructor anInstructor = mapRowToInstructor(rowsReturned);	 // Create an Instructor to add to the list and
			                                                         // Populate the Instructor with data from result
		allInstructors.add(anInstructor);                            // Add instructor to list
		}	
		return allInstructors;
	}
	@Override
	public List<Instructor> getAllInstructors() {
		// TODO Auto-generated method stub
		return null;
	}
	
//---------------------------------------------------------------------
// Helper Methods	
//---------------------------------------------------------------------	
	
	private Instructor mapRowToInstructor(SqlRowSet rowsReturned) {  // This method requires all columns in SQL result
		// define return object
		Instructor pigeon = new Instructor(); 
		// Use our return object set methods to populate with the data from result
		pigeon.setInstructorId(rowsReturned.getString("instructorid"));
		pigeon.setFirstName(rowsReturned.getString("firstname"));
	    pigeon.setMiddleName(rowsReturned.getString("middlename"));
		pigeon.setLastName(rowsReturned.getString("lastname"));
		pigeon.setHomeLocation(rowsReturned.getString("homelocation"));
		// return the object with data from the SQL
		return pigeon;
	}



	

}










