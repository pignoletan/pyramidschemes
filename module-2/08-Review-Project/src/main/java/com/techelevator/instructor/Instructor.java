package com.techelevator.instructor;

public class Instructor {
// member data variables

private	String instructorId; 
private	String firstName;
private	String middleName;
private	String lastName;
private	String homeLocation;

public String getInstructorId() {
	return instructorId;
}
public void setInstructorId(String instructorId) {
	this.instructorId = instructorId;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getMiddleName() {
	return middleName;
}
public void setMiddleName(String middleName) {
	this.middleName = middleName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getHomeLocation() {
	return homeLocation;
}
public void setHomeLocation(String homeLocation) {
	this.homeLocation = homeLocation;
}

}
