package com.techelevator.instructor;

import java.util.List;

/**
 * @author frankfella
 *
 */

public interface InstructorDAO {
	
// Typically have save method to insert and object in the database and C.R.U.D.	
// Usually include search methods and return all rows method
	
	/**
	 * save() - insert a row into the instructor table
	 * 
	 * @param anInstructor - Instructor object containing the instructor information 
	 *                       to be added to the instructor table
	 * @return - true, if operation was a success ; false, if not
	 */

	public boolean save(Instructor anInstructor);   
	
	/**
	 * save() - insert a row into the instructor table
	 * 
	 * @param aFirstName - First name of instructor to be added
	 * @param aLastName  - Last name of instructor to be added
	 * @param aLocation  - Home location of instructor to be added
	 * 
	 * @return - true, if operation was a success ; false, if not
	 */
	
	public boolean save(String aFirstName, String aLastName, String aLocation); 
	
	/**
	 * delete() - remove an instructor from the instructor table
	 * 
	 * @param anInstructor - instructor object containing at least the InstructorID of the instructor to be removed
	 *
	 * @return - true, if operation was a success ; false, if not
	 */
	public boolean delete(Instructor anInstructor);                 // Delete by object or primary key

	/**
	 * delete() - remove an instructor from the instructor table
	 * 
	 * @param instructorID - instructorID of the instructor to be removed
	 *
	 * @return - true, if operation was a success ; false, if not
	 */
	public boolean delete(String anInstructorId);                   // Delete by object or primary key

	/**
	 * update() - update instructor information using information in an Instructor object
	 * 
	 * Note:  If any value if the supplied Instructor object is null, that data should not be updated
	 * 
	 * @param anInstructor
	 * 	 
	 * @return - true, if operation was a success ; false, if not
	 */
	public boolean update(Instructor anInstructor);                  // Update Instructor using an Instructor object
	
	/**
	 * Search for an instructor given the InstructorID
	 * 
	 * @param anInstructorId
	 * @return Instructor object containing the data from the table
	 *          or null if instructor is not found in the table
	 */
	public Instructor        searchByInstructorId(String anInstructorId);  // Find a specific instructor

	/**
	 * Search for an instructors at a given Location
	 * 
	 * @param aLocation - Location to be used in the search
	 * @return ArrayList of instructor objects containing the data for all instructors at the requested location
	 *          or null if  no instructors are not found in the table for the specified location
	 */
	public List<Instructor>  searchByLocation(String aLocation);           // Find all instructors in a location
	
	/**
	 * Return all instructors
	 * 
	 * @param none 
	 * @return ArrayList of instructor objects containing the data for all instructors in the table
	 *          or null if no instructors are is in the table
	 */
	public List<Instructor>  getAllInstructors();                          // Return all instructors
	
	
}
