package com.techelevator;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.instructor.Instructor;
import com.techelevator.instructor.InstructorDAO;
import com.techelevator.instructor.JDBCInstructorDAO;

public class ReviewProjectApp {

	public static void main(String[] args) {
		BasicDataSource cohortDataSource = new BasicDataSource();
		cohortDataSource.setUrl("jdbc:postgresql://localhost:5432/TachEscalator");
		cohortDataSource.setUsername("postgres");
		cohortDataSource.setPassword("postgres1");
		
		InstructorDAO instructorTable = new JDBCInstructorDAO(cohortDataSource);
		
		instructorTable.delete("AndJav");
		
		Instructor newInstructor = new Instructor();
		newInstructor.setFirstName("Andrej");
		newInstructor.setMiddleName("Pidgeon");
		newInstructor.setLastName("JavaPro");
		newInstructor.setHomeLocation("CLE");

		boolean wasSaved = instructorTable.save(newInstructor);
		if (wasSaved) {
			System.out.println("Save was successful");
		}
		else {
			System.out.println("Save was NOT successful");  
		}
		
		List<Instructor> allAtLocation = new ArrayList<Instructor>();
		allAtLocation = instructorTable.searchByLocation("CLE");
		
		System.out.printf("\n%6s %15s %15s %15s %10s\n","ID","First Name", "Middle Name", "Last Name", "Location");
		
		for (Instructor anInstructor : allAtLocation ) {
			System.out.printf("%6s %15s %15s %15s %10s\n"
					          ,anInstructor.getInstructorId()
					          ,anInstructor.getFirstName()
					          ,anInstructor.getMiddleName()
					          ,anInstructor.getLastName()
					          ,anInstructor.getHomeLocation());
			
		}
		
	}

}
