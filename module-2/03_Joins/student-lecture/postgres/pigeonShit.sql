-- list the names of the actors who are in Chicago North, then Something Duck

select first_name, last_name
from film, actor, film_actor
where film.film_id = 141 and film_actor.film_id = film.film_id and film_actor.actor_id = actor.actor_id;

-- Here's the more modern syntax

select first_name, last_name
from film inner join film_actor on film.film_id = film_actor.film_id inner join actor on film_actor.actor_id = actor.actor_id
where film.film_id = 141;

-- but this is clearly more work. It was a waste of time to make this syntax

-- now instead of manually inputting 141, let's have SQL do all the hard word

select first_name, last_name
from film, actor, film_actor
where film.film_id = (select film_id from film where title = 'SOMETHING DUCK')
and film_actor.film_id = film.film_id and film_actor.actor_id = actor.actor_id;