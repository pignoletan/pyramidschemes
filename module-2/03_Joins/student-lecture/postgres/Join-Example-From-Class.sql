--
-- list the names of the actors who are in Chicago North
--
-- Classic syntax for a join
--
--
select first_name, last_name        -- columns to put in result
  from film  f                      -- Tables needed on from
     , actor a                      -- Assign correlation names (alias, nick name) to table for easier coding
     , film_actor fa 
 where f.film_id      = 141         -- id for the film we want  (filtering condition)
   and f.film_id      = fa.film_id  -- match between film and film_actor on film_id   (join condition)
   and fa.actor_id    = a.actor_id  -- match bewteen film_actor and actor on actor_id (join condition)
;
--
-- Modern syntax for a join
--
--
select first_name, last_name         -- columns to put in result
  from film  f                       -- first table in the join
       inner join                    -- type of join - inner join - matching rows bewteen
       film_actor fa                 -- second table in the join
    on f.film_id = fa.film_id        -- match between film and film_actor on film_id   (join condition)

       inner join                    -- type of join - inner join - matching rows bewteen
       actor a                       -- next table in the join
    on fa.actor_id = a.actor_id      -- match bewteen film_actor and actor on actor_id (join condition   
 
 where f.film_id    = 141             -- id for the film we want  (filtering condition)
;

--
-- Classic syntax for a join
--
select first_name, last_name        -- columns to put in result
  from film  f                      -- Tables needed on from
     , actor a                      -- Assign correlation names (alias, nick name) to table for easier coding
     , film_actor fa 
 where f.film_id      = (select film_id   -- use subquery to find id for the film we want  (filtering condition)
                           from film
                          where title = 'CHICAGO NORTH')        
   and f.film_id      = fa.film_id  -- match between film and film_actor on film_id   (join condition)
   and fa.actor_id    = a.actor_id  -- match bewteen film_actor and actor on actor_id (join condition)
;
--

