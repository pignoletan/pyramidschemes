-- The following queries utilize the "dvdstore" database.

-- 1. All of the films that Nick Stallone has appeared in
-- (30 rows)
select title
from film
where film_id in (select film_id
        from film_actor
        where actor_id in (select actor_id
                from actor
                where first_name = 'NICK' and last_name = 'STALLONE'));

-- 2. All of the films that Rita Reynolds has appeared in
-- (20 rows)
select title
from film
where film_id in (select film_id
        from film_actor
        where actor_id in (select actor_id
                from actor
                where first_name = 'RITA' and last_name = 'REYNOLDS'));

-- 3. All of the films that Judy Dean or River Dean have appeared in
-- (46 rows)
select title
from film
where film_id in (select film_id
        from film_actor
        where actor_id in (select actor_id
                from actor
                where (first_name = 'JUDY' or first_name = 'RIVER') and last_name = 'DEAN'));

-- 4. All of the the ‘Documentary’ films
-- (68 rows)
select title
from film
where film_id in (select film_id
        from film_category
        where category_id in (select category_id
                from category
                where name = 'Documentary'));

-- 5. All of the ‘Comedy’ films
-- (58 rows)
select title
from film
where film_id in (select film_id
        from film_category
        where category_id in (select category_id
                from category
                where name = 'Comedy'));

-- 6. All of the ‘Children’ films that are rated ‘G’
-- (10 rows)
select title
from film
where film_id in (select film_id
        from film_category
        where category_id in (select category_id
                from category
                where name = 'Children'))
and rating = 'G';

-- 7. All of the ‘Family’ films that are rated ‘G’ and are less than 2 hours in length
-- (3 rows)
select title
from film
where film_id in (select film_id
        from film_category
        where category_id in (select category_id
                from category
                where name = 'Family'))
and rating = 'G'
and length < 120;

-- 8. All of the films featuring actor Matthew Leigh that are rated ‘G’
-- (9 rows)
select title
from film
where film_id in (select film_id
        from film_actor
        where actor_id in (select actor_id
                from actor
                where first_name = 'MATTHEW' and last_name = 'LEIGH'))
and rating = 'G';

-- 9. All of the ‘Sci-Fi’ films released in 2006
-- (61 rows)
select title
from film
where film_id in (select film_id
        from film_category
        where release_year = 2006
        and category_id in (select category_id
                from category
                where name = 'Sci-Fi'));

-- 10. All of the ‘Action’ films starring Nick Stallone
-- (2 rows)
select title
from film
where film_id in (select film_id
        from film_actor
        where actor_id in (select actor_id
                from actor
                where first_name = 'NICK' and last_name = 'STALLONE'))
and film_id in (select film_id
        from film_category
        where release_year = 2006
        and category_id in (select category_id
                from category
                where name = 'Action'));

-- 11. The address of all stores, including street address, city, district, and country
-- (2 rows)
select address.address, city.city, address.district, country.country
from address, city, country
where address.city_id = city.city_id
and city.country_id = country.country_id
and address.address_id in (select address_id
        from store);

-- 12. A list of all stores by ID, the store’s street address, and the name of the store’s manager
-- (2 rows)
select store.store_id, address.address, staff.first_name, staff.last_name
from store, address, staff
where store.address_id = address.address_id
and store.store_id = staff.store_id;

-- 13. The first and last name of the top ten customers ranked by number of rentals 
-- (#1 should be “ELEANOR HUNT” with 46 rentals, #10 should have 39 rentals)
select customer.first_name || ' ' || customer.last_name as full_name, count(rental.*) as num_of_rentals
from customer, rental
where rental.customer_id = customer.customer_id
group by full_name
order by num_of_rentals desc
limit 10;

-- 14. The first and last name of the top ten customers ranked by dollars spent 
-- (#1 should be “KARL SEAL” with 221.55 spent, #10 should be “ANA BRADLEY” with 174.66 spent)
select customer.first_name || ' ' || customer.last_name as full_name, sum(payment.amount) as dollars_spent
from customer, payment
where payment.customer_id = customer.customer_id
group by full_name
order by dollars_spent desc
limit 10;

-- 15. The store ID, street address, total number of rentals, total amount of sales (i.e. payments), and average sale of each store 
-- (Store 1 has 7928 total rentals and Store 2 has 8121 total rentals)
select store.store_id, address.address, count(rental.*), sum(payment.amount), avg(payment.amount)
from store, address, rental, payment, inventory
where store.address_id = address.address_id
and store.store_id = inventory.store_id
and inventory.inventory_id = rental.inventory_id
and rental.rental_id = payment.rental_id
group by store.store_id, address.address;

-- 16. The top ten film titles by number of rentals
-- (#1 should be “BUCKET BROTHERHOOD” with 34 rentals and #10 should have 31 rentals)
select film.title, count(rental.rental_id)
from film, rental, inventory
where film.film_id = inventory.film_id
and inventory.inventory_id = rental.inventory_id
group by film.title
order by 2 desc
limit 10;

-- 17. The top five film categories by number of rentals 
-- (#1 should be “Sports” with 1179 rentals and #5 should be “Family” with 1096 rentals)
select category.name, count(rental.*)
from category, rental, film_category, film, inventory
where category.category_id = film_category.category_id
and film_category.film_id = film.film_id
and film.film_id = inventory.film_id
and inventory.inventory_id = rental.inventory_id
group by 1
order by 2 desc
limit 5;


-- 18. The top five Action film titles by number of rentals 
-- (#1 should have 30 rentals and #5 should have 28 rentals)
select film.title, count(rental.*)
from film, rental, inventory
where film.film_id in (select film_id
        from film_category
        where category_id in (select category_id
                from category
                where name = 'Action'))
and film.film_id = inventory.film_id
and inventory.inventory_id = rental.inventory_id
group by 1
order by 2 desc
limit 5;

-- 19. The top 10 actors ranked by number of rentals of films starring that actor 
-- (#1 should be “GINA DEGENERES” with 753 rentals and #10 should be “SEAN GUINESS” with 599 rentals)
select actor.first_name || ' ' || actor.last_name as full_name, count(rental.*)
from actor, rental, film_actor, inventory
where actor.actor_id = film_actor.actor_id
and film_actor.film_id = inventory.film_id
and inventory.inventory_id = rental.inventory_id
group by actor.actor_id
order by 2 desc
limit 10;

-- 20. The top 5 “Comedy” actors ranked by number of rentals of films in the “Comedy” category starring that actor 
-- (#1 should have 87 rentals and #5 should have 72 rentals)
select actor.first_name || ' ' || actor.last_name as full_name, count(rental.*)
from actor, rental, category, film_category, inventory, film_actor, film
where category.name = 'Comedy'
and category.category_id = film_category.category_id
and film_category.film_id = film.film_id
and film.film_id = inventory.film_id
and inventory.inventory_id = rental.inventory_id
and film.film_id = film_actor.film_id
and film_actor.actor_id = actor.actor_id
group by actor.actor_id
order by 2 desc
limit 5;