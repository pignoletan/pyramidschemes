begin transaction;

drop table    if exists customer_info;
drop sequence if exists seq_purchase_id;
drop table    if exists purchase_history;

create table customer_info (  -- place the column definitions inside the () for the create table
-- column-name      data-type     allow-nulls?    other-options
 customer_id         serial        not null     -- serial - database manager to assign as a unique sequential value
,firstName           varchar(30)   not null  
,lastName            varchar(40)   not null  
,streetAddress       text          not null
,city                varchar(20)   not null
,state               char(2)       not null
,postalCode          varchar(10)   not null
,phone               varchar(15)   
-- define primary key as constraint
-- constraint-name (which appears in messages)   PRIMARY KEY (name-of-prikey-column)
,CONSTRAINT pk_customer_info_customer_id   primary key (customer_id)
);

create sequence seq_purchase_id;  -- define an object to generate unique, sequential values for use with purchase id

create table purchase_history (
 purchase_id          integer                  not null default nextval('seq_purchase_id')  -- default value is coming from a sequence object
,purchase_date        timestamp with time zone not null default current_timestamp
,puchase_price        numeric(9,2)             not null
,inventory_id         integer                  not null
,customer_id          integer                  not null
,constraint pk_purchase_history_purchase_id  primary key (purchase_id)
--          constraint-name                           column-in-this-table        parent-table(primary-key-in-parent)
,constraint fk_purchase_history_customer     foreign key (customer_id) references customer_info(customer_id)
);
--
--
-- may also add foreign keys after all tables are created and loaded with initial data
--
-- alter table purchase_history add foreign key (customer_id) references customer_info(customer_id)

rollback; -- change to commit once we are sure this works
