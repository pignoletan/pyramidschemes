begin transaction;

drop table if exists student;
drop table if exists cohort;
drop table if exists instructor;
drop table if exists location;
drop table if exists course;
drop sequence if exists seq_cohort_id;

create table location (
        location_id varchar(8) not null unique,
        city varchar(30) not null,
        state char(2) not null,
        openingDate date,
        constraint pk_location_location_id primary key (location_id)
);

create table instructor (
        instructor_key_id serial not null,
        instructorId char(6) not null, --if two people have the same names this won't be unique
        firstName varchar(30) not null,
        lastName varchar(30) not null,
        location_id varchar(8) not null,
        constraint pk_instructor_instructor_key_id primary key (instructor_key_id),
        constraint fk_instructor_location_id foreign key (location_id) references location
);

create sequence seq_cohort_id;

create table cohort (
        cohort_id integer not null default nextval('seq_cohort_id'),
        course_id integer not null,
        location_id varchar(8) not null,
        instructor_key_id integer,
        maxNumOfStudents integer not null default 18,
        curNumOfStudents integer not null default 0,
        curNumOfGrads integer not null default 0,
        startDate date not null,
        gradDate date not null,
        notes text,
        constraint pk_cohort_cohort_id primary key (cohort_id),
        constraint fk_cohort_location_id foreign key (location_id) references location,
        constraint fk_cohort_instructor_key_id foreign key (instructor_key_id) references instructor
);

create table student (
        student_id integer not null unique,
        cohort_id integer, -- use to get location and course of interest
        firstName varChar(30) not null,
        middleName varchar(30) not null,
        lastName varchar(30) not null,
        streetAddress varchar(40) not null,
        city varchar(30) not null,
        state char (2) not null,
        zipCode varchar(10) not null,
        emailAddress varchar(50) not null,
        twitterID varchar(15),
        prefPronouns varchar(10) not null,
        status varchar(19) not null,
        dateAdded date not null,
        dateLastChanged timestamp with time zone not null,
        note text,
        constraint ck_status check (status in ('Enrolled','Applied','Interviewed','Rejected','Graduated',
        'Withdrawn-Personal','Withdrawn-Academic')),
        constraint pk_student_student_id primary key (student_id),
        constraint fk_student_cohort_id foreign key (cohort_id) references cohort
);

create table course (
        course_id serial not null,
        name varchar(30),
        durInWeeks integer not null,
        constraint pk_course_course_id primary key (course_id)
);

ALTER TABLE cohort ADD FOREIGN KEY (course_id) REFERENCES course(course_id);

commit;



begin transaction;

insert into course (name, durInWeeks) values ('Java',14);
insert into course (name, durInWeeks) values ('Terminators',1991);
insert into course (name, durInWeeks) values ('Dad Joke Chat Bots',200);
insert into course (name, durInWeeks) values ('Cyber security',2);
insert into course (name, durInWeeks) values ('Remedial Loops',5);
insert into course (name, durInWeeks) values ('C#',14);

insert into location (location_id,city,state,openingDate) values ('1234ABCD','Atlantis','GA','03/05/2020');
insert into location (location_id,city,state,openingDate) values ('777','Cleveland','OH','05/06/2007');
insert into location (location_id,city,state,openingDate) values ('BITEME','New York','NY','12/31/1');

insert into instructor (instructorId,firstName,lastName,location_id) values ('FraFel','Frank','Fella','777');
insert into instructor (instructorId,firstName,lastName,location_id) values ('RonMcd','Ronald','McDonald','777');
insert into instructor (instructorId,firstName,lastName,location_id) values ('PosGod','Poseidon','God','1234ABCD');
insert into instructor (instructorId,firstName,lastName,location_id) values ('FucKit','Fucima','Kitanelli','BITEME');
insert into instructor (instructorId,firstName,lastName,location_id) values ('MikHun','Mike','Hunt','BITEME');
insert into instructor (instructorId,firstName,lastName,location_id) values ('EviSev','Evil','Sevil','1234ABCD');
insert into instructor (instructorId,firstName,lastName,location_id) values ('ThaSoc','Thatdude','Socrait','777');
insert into instructor (instructorId,firstName,lastName,location_id) values ('VivIna','Vivian','Inari','BITEME');
insert into instructor (instructorId,firstName,lastName,location_id) values ('PriSco','Princess','Scorpia','BITEME');
insert into instructor (instructorId,firstName,lastName,location_id) values ('ImeVil','Imenka','Vilma','BITEME');

insert into cohort (cohort_id,course_id,location_id,instructor_key_id,startDate,gradDate)
values (0,(select course_id from course where name = 'Java'),'BITEME',(select instructor_key_id from instructor where instructorid = 'MikHun'),
'01/01/1000',date('1/1/1000') + interval '98 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Dad Joke Chat Bots'),'777',(select instructor_key_id from instructor where instructorid = 'FraFel'),
'7/1/2019',date('7/1/2019') + interval '1400 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Remedial Loops'),'777',(select instructor_key_id from instructor where instructorid = 'RonMcd'),
current_date,current_date + interval '70 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Cyber security'),'1234ABCD',(select instructor_key_id from instructor where instructorid = 'PosGod'),
'01/01/1000',date('1/1/1000') + interval '14 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'C#'),'BITEME',(select instructor_key_id from instructor where instructorid = 'FucKit'),
'01/01/1000',date('1/1/1000') + interval '98 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Cyber security'),'1234ABCD',(select instructor_key_id from instructor where instructorid = 'EviSev'),
'01/01/1000',date('1/1/1000') + interval '14 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Remedial Loops'),'777',(select instructor_key_id from instructor where instructorid = 'ThaSoc'),
'01/01/1000',date('1/1/1000') + interval '70 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Java'),'BITEME',(select instructor_key_id from instructor where instructorid = 'VivIna'),
'01/01/1000',date('1/1/1000') + interval '98 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Terminators'),'BITEME',(select instructor_key_id from instructor where instructorid = 'PriSco'),
'01/01/1000',date('1/1/1000') + interval '13937 days');
insert into cohort (course_id,location_id,instructor_key_id,startDate,gradDate)
values ((select course_id from course where name = 'Terminators'),'BITEME',(select instructor_key_id from instructor where instructorid = 'ImeVil'),
'01/01/1000',date('1/1/1000') + interval '13937 days');

insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (1,0,'Andrew','Badass','Pignolet','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Enrolled',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (2,1,'Matthew','James','Knauff','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (3,2,'Maxwell','Taylor','Watson','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (4,3,'Mark','Designer','Rosewater','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (5,4,'Esther','Winner','Nahm','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (6,5,'Josh','PingPong','Goran','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (7,6,'John','Hisnameis','Cena','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (8,7,'Andrej','Foreigner','Thegiant','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (9,8,'Jennifer','Awesome','Lawrence','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);
insert into student(student_id,cohort_id,firstName,middleName,lastName,streetAddress,city,state,zipCode,emailAddress,prefPronouns,status,dateAdded,dateLastChanged)
values (10,9,'Sarah','Farmer','Hague','1234 Number Road','Novelty','OH','12345','hi@email.com','He Him Her','Rejected',current_date,current_date);


select * from course;
select * from location;
select * from instructor;
select * from cohort;
select * from student;

select firstName, lastName, name as course_name
from instructor, course, cohort
where instructor.instructor_key_id = cohort.instructor_key_id
and cohort.course_id = course.course_id;

select firstName, lastName, name as course_name
from student, course, cohort
where student.cohort_id = cohort.cohort_id
and cohort.course_id = course.course_id;

rollback;