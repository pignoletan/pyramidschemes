
--------------------------------------------------------------------------------------------------------
-- Basic SELECT Lecture Code
--------------------------------------------------------------------------------------------------------
-- -- indicates a comment - anything following on the line is ignored
--
-- SQL SELECT statement - retrieve values form the database (Read)
--
-- A SELECT statement is often referred to as a query
--
-- Basic syntax:
--
--      SELECT   - columns to include in the result (seperate mutiple column reqeusts with commas)
--      FROM     - table containing rows used in the query 
--      WHERE    - rows to include in the result
--      ORDER BY - sequence of rows in teh result
--
-- WHERE predicates:
--
--        =  <>  !=  >  >=  <  <= 
--        IN(list-of-values)
--        NOT IN(list-of-values)
--        BETWEEN value AND value
--        IS NULL
--        IS NOT NULL
--        LIKE    (use wildcards: % means 0 to any number of any characters
--                                _ means exactly any one character
--        ILIKE   (case insensivtive LIKE - Postgres extension)
--
-- predicates may be combined using AND and OR
--
-- use parentheses to make your multi-predicate condition clear

-- The DISTINCT clause on a SELECT removes duplicate values from the result
-- based on the all columns that follow
--
-- The DISTINCT ON(columns/expression) clause on a SELECT removes duplicate values from the result
-- based on the all columns/expression inside the parentheses that follow (Postgres extension)
------------------------------------------------------------------------------------------------------
--
-- SELECT ... FROM
-- Selecting the names for all countries
Select name      -- Name of the country
  from country   -- Get the data from the country table
;                -- Put a ; at then of each SQL statement in case it's followed by another SQL statement

-- Selecting the name and population of all countries
-- in population order
--
-- ORDER BY - sort results
--
-- Default order is ASCending - ASC - low to high
-- Specify DESC for DESCending -     high to low
--
--
-- The order of rows in the results is UNDEFINED without an ORDER BY
-- If there is no ORDER BY the order or the results is not guaranteed
-- If the order of result is important - code an ORDER BY
--
  SELECT population, name    -- Show me the population and name 
    FROM country             -- for the countries
ORDER BY population  desc       -- sort the result by this column  
;
-- Selecting all columns from the city table
select * from city;

-- SELECT ... FROM ... WHERE
-- Selecting the cities in Ohio
select name
  from city
 Where district <> 'Ohio'
;
-- Selecting countries that gained independence in the year 1776
Select indepyear    -- year of independence
      ,name         -- name of country 
  from country
 where indepyear = 1776
;
-- Selecting countries not in Asia
select name, continent
  from country
 where continent != 'Asia'
;
-- Selecting countries that do not have an independence year
-- Missing or unknown data is respresent by a NULL
--
-- NULL indicates missing or unknown - any operation with a NULL results in a null
-- Frank: "I don't know" value
--
-- CANNOT use = ! <> or any other standard WHERE predicate with null - result is null
--
-- Use IS NULL  or IS NOT NULL to check for null
--
-- Most functions ignore nulls
--
Select indepyear    -- year of independence
      ,name         -- name of country 
  from country
 where indepyear IS null  -- if indepyear is unknown
;
-- Selecting countries that do have an independence year
Select indepyear    -- year of independence
      ,name         -- name of country 
  from country
 where indepyear IS not null  -- if indepyear has a value
;

-- Selecting countries that have a population greater than 5 million
Select name, population
  from country
 Where population > 5000000
 order by population desc
;

-- SELECT ... FROM ... WHERE ... AND/OR
-- Selecting cities in Ohio and Population greater than 400,000
select name     --,district, population
  from city
 where district = 'Ohio' 
   AND population > 400000
;
-- Selecting country names on the continent North America or South America
--
--  a complete predicate must be on each side of teh AND  OR
--
--     complete predicate;   column condition value
--

select name, continent
  from country
 where continent = 'North America' or continent = 'South America'
 order by continent
;

--  The IN clause is an altenative to a series of = OR
--
--  column IN(a list-of-values)
--
select name, continent
  from country
 where continent IN ('North America', 'South America')
 order by continent
;
--
-- LIKE clause uses wildcards to search
--
-- % = any 0-n of characters
--     %value = ends-with
--     value% = start-with
--    %value% = contains
--
-- _ = exactly any one character
--
--
select name, continent
  from country
 where continent LIKE '%America'  -- continent ends with American
 order by continent
;

--
select name, continent
  from country
 where continent LIKE 'A_r%ca'  -- continent starts with 'A', any seconds charace, third char must be 'r' and end in 'ca'
;
-- SELECTING DATA w/arithmetic
-- Selecting the population, life expectancy, 
-- and population per area = (population / area)
--	note the use of the 'as' keyword
--
-- derived columns do not have names
-- use the AS phrase to give them names
-- 
-- derived means result if calculation or operation
--
--

Select population
      ,surfacearea
      ,population / surfacearea   -- arithmetic allowed as a column
 from country
 ;


Select population   as number_of_people
      ,surfacearea
      ,population / surfacearea  as Pop_per_Area
 from country
 ;
--
--
-- Show all the continent names
--
-- Distinct will only return one row for each unique value in the column list
--
-- without distinct - all rows that satisfy WHERE clause are returnd
--
Select distinct continent
  from country
;
--
--
-- Show countries that gained independence in the 20th century
--
select name, indepyear
  from country
 where indepyear between 1900 and 1999
 order by indepyear
 ;