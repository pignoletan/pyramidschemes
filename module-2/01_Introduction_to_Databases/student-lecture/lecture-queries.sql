-- SELECT ... FROM
-- Selecting the names for all countries
Select name
  from country;
-- -- indicates a comment
-- Selecting the name and population of all countries
SELECT population, name
  FROM country
  Order by population desc;

-- Selecting all columns from the city table
Select *
  from city;

-- SELECT ... FROM ... WHERE
-- Selecting the cities in Ohio
select name, district
from city
where district <> 'Ohio';

-- Selecting countries that gained independence in the year 1776
Select name, indepyear
from country
where indepyear = 1776;

-- Selecting countries not in Asia
select name, continent
from country
where continent != 'Asia';

-- Selecting countries that do not have an independence year
select name, indepyear
from country
where indepyear is null;

-- Selecting countries that do have an independence year
select name, indepyear
from country
where indepyear is not null;

-- Selecting countries that have a population greater than 5 million
select name, population
from country
where population > 5000000;


-- SELECT ... FROM ... WHERE ... AND/OR
-- Selecting cities in Ohio and Population greater than 400,000
select name district, population
from city
where district = 'Ohio' and population > 400000;

-- Selecting country names on the continent North America or South America
select name, continent
from country
where continent in ('North America', 'South America')
order by continent;



-- SELECTING DATA w/arithmetic
-- Selecting the population, life expectancy, and population per area
--	note the use of the 'as' keyword
select name, population, lifeexpectancy, population/surfacearea as Pop_per_area
from country;


-- get some continent names
select distinct continent
from country;