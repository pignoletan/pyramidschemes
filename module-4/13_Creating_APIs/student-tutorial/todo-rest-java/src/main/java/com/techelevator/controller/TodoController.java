package com.techelevator.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.techelevator.todo.dao.TodoDao;
import com.techelevator.todo.model.Todo;

@RestController
@RequestMapping("/api/todos")
public class TodoController {
	private TodoDao todoDao;

	public TodoController(TodoDao todoDao) {
		this.todoDao = todoDao;
	}
	
	@GetMapping
	public List<Todo> list() {
		return todoDao.list();
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Todo create(@RequestBody Todo todo) {
		return todoDao.create(todo);
	}
}
