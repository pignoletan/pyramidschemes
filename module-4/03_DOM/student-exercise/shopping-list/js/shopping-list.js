const pageTitle = 'My Shopping List';
const groceries = ['Poop','Munchkins','Fate','Money','Boulders','Calcium',
                  'Skill','Java','Covfefe','Ten Thousand Flying Lemurs'];

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle() {
  const mainTitle = document.getElementById('title');
  mainTitle.innerHTML = pageTitle;
}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries() {
  const list = document.getElementById('groceries');
  groceries.forEach( (grocery) => {
    const entry = document.createElement('li');
    entry.innerHTML = grocery;
    list.insertAdjacentElement('beforeend',entry);
  });
}

/**
 * This function will be called wh4en the button is clicked. You will need to get a reference
 * to every list item and add the class completed to each one
 */
function markCompleted() {
  const allLists = document.querySelectorAll('li');
  allLists.forEach( (aList) => {
    aList.setAttribute('class','completed');
  });
}

setPageTitle();

displayGroceries();

// Don't worry too much about what is going on here, we will cover this when we discuss events.
document.addEventListener('DOMContentLoaded', () => {
  // When the DOM Content has loaded attach a click listener to the button
  const button = document.querySelector('.btn');
  button.addEventListener('click', markCompleted);
});
