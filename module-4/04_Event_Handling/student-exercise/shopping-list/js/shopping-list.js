let allItemsIncomplete = true;
const pageTitle = 'My Shopping List';
const groceries = [
  { id: 1, name: 'Roses', completed: false },
  { id: 2, name: 'Dresses', completed: false },
  { id: 3, name: 'Eyeliner', completed: false },
  { id: 4, name: 'Mascara', completed: false },
  { id: 5, name: 'Raven Feed', completed: false },
  { id: 6, name: 'Goat Skulls', completed: false },
  { id: 7, name: 'Rings', completed: false },
  { id: 8, name: 'Diamonds', completed: false },
  { id: 9, name: 'Nightshade', completed: false },
  { id: 10, name: 'Tea', completed: false }
];

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle() {
  const title = document.getElementById('title');
  title.innerText = pageTitle;
}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries() {
  const ul = document.querySelector('ul');
  groceries.forEach((item) => {
    const li = document.createElement('li');
    li.innerText = item.name;
    const checkCircle = document.createElement('i');
    checkCircle.setAttribute('class', 'far fa-check-circle');
    li.appendChild(checkCircle);
    ul.appendChild(li);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  setPageTitle();
  displayGroceries();

  const listItems = document.querySelectorAll('li');
  listItems.forEach( (item) => {
      item.addEventListener('click', (event) => {
        if (!item.classList.contains('completed')) {
        item.classList.add('completed');
        item.querySelector('i').classList.add('completed');
      }
    });
  

    item.addEventListener('dblclick', (event) => {
      if (item.classList.contains('completed')) {
      item.classList.remove('completed');
      item.querySelector('i').classList.remove('completed');
    }
  });
  });

  const theButton = document.getElementById('toggleAll');
  theButton.addEventListener('click', (event) => {
    if (allItemsIncomplete) {
      allItemsIncomplete = false;
      listItems.forEach( (item) => {
        if (!item.classList.contains('completed')) {
          item.classList.add('completed');
          item.querySelector('i').classList.add('completed');
        }
      });
    } else {
      allItemsIncomplete = true;
      listItems.forEach( (item) => {
      if (item.classList.contains('completed')) {
        item.classList.remove('completed');
        item.querySelector('i').classList.remove('completed');
      }
    })
  }
})
});


