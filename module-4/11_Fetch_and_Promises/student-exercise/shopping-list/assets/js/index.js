let listItems = [];

function loadList() {
    fetch("assets/data/shopping-list.json")
    .then((response) => {
        return response.json();
      })
      .then((data) => {
        listItems = data;
        displayList();
      })
      .catch((err) => console.error(err));
}

function displayList() {
    const listArea = document.querySelector('ul');
    listItems.forEach( (item) => {
        const tmpl = document.getElementById('shopping-list-item-template').content.cloneNode(true);
        tmpl.querySelector('li').insertAdjacentHTML('afterBegin',item.name);
        if (item.completed) {
            tmpl.querySelector('li').classList.add('completed');
            tmpl.querySelector('i').classList.add('completed');
        }
        listArea.appendChild(tmpl);
    })
}

const button = document.querySelector(".loadingButton");
button.addEventListener("click", () => {
    if (document.querySelector("ul").getElementsByTagName("li").length < 2)
    {
        loadList();
        button.disabled = true;
    }
})