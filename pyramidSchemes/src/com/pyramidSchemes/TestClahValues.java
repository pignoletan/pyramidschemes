package com.pyramidSchemes;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

class TestClahValues {  //oh whoops that name is typoed
	
	@Test
	public void testClashValues() {
		GameBoard theGameBoard = new GameBoard(9,11);
		Pyramid aPyramid = theGameBoard.makeNewPyramid(3, 3, PlayerEnum.BLUE);
		aPyramid.addStories(2);
		theGameBoard.isRealSpace(3,3);
		theGameBoard.calculateClash();
		int[][] clashValues = theGameBoard.getClashValues();
		assertEquals(clashValues[3][3],0);
		assertEquals(clashValues[2][3],3);
		assertEquals(clashValues[4][3],3);
		assertEquals(clashValues[3][2],3);
		assertEquals(clashValues[3][4],3);
		assertEquals(clashValues[3][5],2);
		assertEquals(clashValues[3][6],1);
		assertEquals(clashValues[4][4],2);
		assertEquals(clashValues[2][2],2);
		assertEquals(clashValues[2][4],2);
		assertEquals(clashValues[4][2],2);
		Pyramid newPyramid = theGameBoard.makeNewPyramid(3,7,PlayerEnum.RED);
		theGameBoard.calculateClash();
		clashValues = theGameBoard.getClashValues();
		assertEquals(0,clashValues[3][6]);
		assertEquals(-1,clashValues[3][8]);
		newPyramid.addStories(1);
		theGameBoard.calculateClash();
		clashValues = theGameBoard.getClashValues();
		assertEquals(-1,clashValues[3][6]);
		assertEquals(-1,clashValues[4][6]);
		assertEquals(-1,clashValues[2][6]);
		assertEquals(1,clashValues[3][5]);
	}

}
