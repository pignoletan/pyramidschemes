package com.pyramidSchemes;

public class Pyramid {
	public final int XCOORD;
	public final int YCOORD;
	public final PlayerEnum FACTION;  //must be either a blue or red pyramid
	private int stories;  //can vary from 1 to 5
	
	public int getStories() {
		return stories;
	}
	
	public Pyramid(int xCoord,int yCoord,PlayerEnum faction) {
		XCOORD = xCoord;
		YCOORD = yCoord;
		FACTION = faction;
		stories = 1;
	}
	
	public Pyramid(int xCoord,int yCoord,PlayerEnum faction,int stories) {
		XCOORD = xCoord;
		YCOORD = yCoord;
		FACTION = faction;
		this.stories = stories;
	}
	
	public int addStories(int amount) {
		stories += amount;
		return stories;
	}
}
