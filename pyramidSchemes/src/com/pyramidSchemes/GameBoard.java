package com.pyramidSchemes;

import java.util.ArrayList;

public class GameBoard {
	public final int X_SIZE;  //Maximum xsize of the board
	public final int Y_SIZE; //maximum ysize of the board
	private int[][] clashValues;  //positive clash is blue, negative clash is red, 0 clash is neutral
	private ArrayList<Pyramid> pyramids = new ArrayList<Pyramid>();
	
	public GameBoard(int xSize, int ySize) {
		X_SIZE = xSize;
		Y_SIZE = ySize;
		clashValues = new int[X_SIZE][Y_SIZE];
		resetClash();
	}
	
	public boolean isRealSpace(int xCoord,int yCoord) {
		if (xCoord >= 0&&yCoord >= 0&&xCoord < X_SIZE&&yCoord < Y_SIZE) {
			return true;
		}
		return false;
	}
	
	//return whether or not it successfully made a pyramid
	public Pyramid makeNewPyramid(int xCoord,int yCoord,PlayerEnum faction) {
		Pyramid aPyramid = null;
		if (!isRealSpace(xCoord,yCoord)) {
			return aPyramid;
		}
		aPyramid = new Pyramid(xCoord,yCoord,faction);
		pyramids.add(aPyramid);
		return aPyramid;
	}

	public int[][] getClashValues() {
		return clashValues;
	}

	public ArrayList<Pyramid> getPyramids() {
		return pyramids;
	}
	
	public boolean isSpaceFaction(int xCoord,int yCoord, PlayerEnum faction) {
		if (clashValues[xCoord][yCoord] > 0&&faction == PlayerEnum.BLUE) {
			return true;
		}
		if (clashValues[xCoord][yCoord] < 0&&faction == PlayerEnum.RED) {
			return true;
		}
		for (int i = 0; i < pyramids.size(); i++) {
			Pyramid aPyramid = pyramids.get(i);
			if (aPyramid.XCOORD == xCoord
					&&aPyramid.YCOORD == yCoord
					&&aPyramid.FACTION == faction) {
				return true;
			}
				
		}
		return false;
	}
	
	public int getNumOfSpacesOwned(PlayerEnum faction) {
		int result = 0;
		for (int x = 0; x < X_SIZE; x++) {
			for (int y = 0; y < Y_SIZE; y++) {
				if (isSpaceFaction(x,y,faction)) {
					result++;
				}
			}
		}
		return result;
	}
	
	public void calculateClash() {
		resetClash();
		for (int i = 0; i < pyramids.size(); i++) {
			Pyramid aPyramid = pyramids.get(i);
			int stories = aPyramid.getStories();
			PlayerEnum faction = aPyramid.FACTION;
			for (int j = 1; j <= stories; j++) {
				for (int z = 0; z < 4; z++) {  //run once for each cardinal direction
					int xAdj = 0;
					int yAdj = 0;
					int xInc = 0;
					int yInc = 0;
					switch (z) {
					case 0:
						xAdj = j;
						xInc = -1;
						yInc = -1;
						break;
					case 1:
						yAdj = -j;
						xInc = -1;
						yInc = 1;
						break;
					case 2:
						xAdj = -j;
						xInc = 1;
						yInc = 1;
						break;
					case 3:
						yAdj = j;
						xInc = 1;
						yInc = -1;
						break;
					}
					for (int k = 0; k < j; k++) {
						if (faction == PlayerEnum.BLUE) {
							clashValues[aPyramid.XCOORD + xAdj][aPyramid.YCOORD + yAdj] += stories + 1 - j;
						} else {
							clashValues[aPyramid.XCOORD + xAdj][aPyramid.YCOORD + yAdj] -= stories + 1 - j;
						}
						xAdj += xInc;
						yAdj += yInc;
						
					}
				}
			}
		}
	}
	
	public void resetClash() {
		for (int x = 0; x < X_SIZE; x++) {
			for (int y = 0; y < Y_SIZE; y++) {
				clashValues[x][y] = 0;
			}
		}
	}
	
	public static int getGridDistance(int x1, int y1, int x2, int y2) {
		int xDiff = Math.abs(x1 - x2);
		int yDiff = Math.abs(y1 - y2);
		return xDiff + yDiff;
	}
	
}
